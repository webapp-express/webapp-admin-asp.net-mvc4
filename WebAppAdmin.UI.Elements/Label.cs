﻿using System;
using WebAppAdmin.UI.Core;

namespace WebAppAdmin.UI.Elements
{
    public class Label : UIElement
    {
        private string html;

        public string For
        {
            get;
            set;
        }

        public string Html
        {
            get
            {
                if (ResourceManager != null)
                {
                    return ResourceManager.GetString(html);
                }
                return html;
            }
            set
            {
                html = value;
            }
        }

        protected override void RenderBeginTag(ViewContext viewContext)
        {
            viewContext.Content.AppendLine();
            viewContext.Content.Append("<label");
            if (!String.IsNullOrEmpty(this.Id))
            {
                viewContext.Content.Append(" id=\"" + this.Id + "\"");
            }
            if (!String.IsNullOrEmpty(this.For))
            {
                viewContext.Content.Append(" for=\"" + this.For + "\"");
            }
            if (!String.IsNullOrEmpty(this.Class))
            {
                viewContext.Content.Append(" class=\"" + this.Class + "\"");
            }
            if (this.Attributes != null)
            {
                string attrName = "";
                foreach (var attr in this.Attributes)
                {
                    attrName = attr.Name.ToLower();
                    if (attrName == "id"
                        || attrName == "for"
                        || attrName == "class")
                    {
                        continue;
                    }
                    viewContext.Content.Append(" " + attrName + "=\"" + attr.Value + "\"");
                }
            }
            viewContext.Content.Append(">");
        }

        protected override void RenderContents(ViewContext viewContext)
        {
            if (!String.IsNullOrEmpty(this.Html))
            {
                viewContext.Content.Append(this.Html);
            }
        }

        protected override void RenderEndTag(ViewContext viewContext)
        {
            viewContext.Content.Append("</label>");
        }
    }
}
