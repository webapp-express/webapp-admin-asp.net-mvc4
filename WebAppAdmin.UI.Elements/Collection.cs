﻿using System;
using System.Collections.Generic;
using WebAppAdmin.UI.Core;

namespace WebAppAdmin.UI.Elements
{
    public class Collection : Control, ItemsControl
    {
        public List<object> List
        {
            get;
            set;
        }

        public override string ValuePropertyName
        {
            get
            {
                return "List";
            }
        }

        public List<Column> Columns { get; set; }

        public List<Control> ItemControls
        {

            get
            {
                List<Control> controList = new List<Control>();
                foreach (var column in Columns)
                {
                    controList.Add(column.Control);
                }
                return controList;
            }
        }

        protected override void RenderBeginTag(ViewContext viewContext)
        {
            viewContext.Content.AppendLine();
            viewContext.Content.Append("<div");
            if (!String.IsNullOrEmpty(this.Id))
            {
                viewContext.Content.Append(" id=\"" + this.Id + "\"");
            }
            if (!String.IsNullOrEmpty(this.Class))
            {
                viewContext.Content.Append(" class=\"" + this.Class + "\"");
            }
            if (this.Attributes != null)
            {
                string attrName = "";
                foreach (var attr in this.Attributes)
                {
                    attrName = attr.Name.ToLower();
                    if (attrName == "id"
                        || attrName == "class")
                    {
                        continue;
                    }
                    viewContext.Content.Append(" " + attrName + "=\"" + attr.Value + "\"");
                }
            }
            viewContext.Content.Append(">");

            viewContext.Content.AppendLine();
            viewContext.Content.Append("<table>");
            viewContext.Content.AppendLine();
            viewContext.Content.Append("<tbody>");

            if (Columns != null)
            {
                this.Children.Clear();
                TableRow row = new TableRow();
                TableColumn tableColumn = null;
                Control control = null;
                foreach (var column in Columns)
                {
                    tableColumn = new TableColumn();
                    tableColumn.Column = column;
                    tableColumn.ResourceManager = this.ResourceManager;
                    row.Children.Add(tableColumn);

                    control = (Control)column.Control.Clone();
                    viewContext.Resources.AddRange(control.Resources);
                }
                row.Children.Add(new TableColumn());
                this.Children.Add(row);

                if (List != null
                    && ItemControls != null)
                {
                    for (int i = 0; i < List.Count; i++)
                    {
                        this.Children.Add(this.ItemTemplate(List[i], this.Name, i));
                    }
                }
            }
        }

        private TableRow ItemTemplate(object item, string propertyName, int index, bool isItemTemplate = false)
        {
            TableRow row = new TableRow();
            row.ViewModel = item;
            TableCell cell = null;
            Control control = null;
            foreach (var column in Columns)
            {
                cell = new TableCell();
                cell.Column = column;
                control = (Control)column.Control.Clone();
                if (!isItemTemplate)
                {
                    control.Id = this.Id + "-" + propertyName + "-i" + index + "-" + column.Control.Name;
                    control.Name = propertyName + "[" + index + "]." + column.Control.Name;
                }
                else
                {
                    control.Id = this.Id + "-" + propertyName + "-i<%=index%>" + "-" + column.Control.Name;
                    control.Name = this.Name + "[<%=index%>]." + column.Control.Name;
                }
                cell.Children.Add(control);
                row.Children.Add(cell);
            }
            cell = new TableCell();
            cell.Class = "item-btn-container";
            row.Children.Add(cell);
            return row;
        }

        protected override void RenderEndTag(ViewContext viewContext)
        {
            viewContext.Content.AppendLine();
            viewContext.Content.Append("</tbody>");
            viewContext.Content.AppendLine();
            viewContext.Content.Append("</table>");
            if (Columns != null)
            {
                viewContext.Content.Append("<script class='ItemTemplate' type='text/html'>");
                viewContext.Content.AppendLine();

                ViewContext viewContextItemTemplate = new ViewContext();
                TableRow row = this.ItemTemplate(null, this.Name, 0, true);
                row.Mode = this.Mode;

                row.Render(viewContextItemTemplate);
                viewContext.Content.Append(viewContextItemTemplate.Content.ToString());

                viewContext.Content.AppendLine();
                viewContext.Content.Append("</script>");
            }
            viewContext.Content.AppendLine();
            viewContext.Content.Append("<input type=\"hidden\" class=\"Index\" value=\"" + (this.List != null ? this.List.Count - 1 : -1) + "\"/>");
            viewContext.Content.AppendLine();
            viewContext.Content.Append("</div>");
        }
    }
}
