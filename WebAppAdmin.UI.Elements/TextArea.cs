﻿using System;
using WebAppAdmin.UI.Core;

namespace WebAppAdmin.UI.Elements
{
    public class TextArea : Control
    {
        public string Html
        {
            get;
            set;
        }

        public override string ValuePropertyName
        {
            get
            {
                return "Html";
            }
        }

        protected override void RenderBeginTag(ViewContext viewContext)
        {
            viewContext.Content.AppendLine();
            viewContext.Content.Append("<textarea");
            if (!String.IsNullOrEmpty(this.Id))
            {
                viewContext.Content.Append(" id=\"" + this.Id + "\"");
            }
            if (!String.IsNullOrEmpty(this.Name))
            {
                viewContext.Content.Append(" name=\"" + this.Name + "\"");
            }
            if (!String.IsNullOrEmpty(this.Class))
            {
                viewContext.Content.Append(" class=\"" + this.Class + "\"");
            }
            if (this.Attributes != null)
            {
                string attrName = "";
                foreach (var attr in this.Attributes)
                {
                    attrName = attr.Name.ToLower();
                    if (attrName == "id"
                        || attrName == "name"
                        || attrName == "class")
                    {
                        continue;
                    }
                    viewContext.Content.Append(" " + attrName + "=\"" + attr.Value + "\"");
                } 
            }
            viewContext.Content.Append(" rows=\"2\" cols=\"20\">");
        }

        protected override void RenderContents(ViewContext viewContext)
        {
            if (!String.IsNullOrEmpty(this.Html))
            {
                viewContext.Content.Append(this.Html);
            }
        }

        protected override void RenderEndTag(ViewContext viewContext)
        {
            viewContext.Content.Append("</textarea>");
        }
    }
}
