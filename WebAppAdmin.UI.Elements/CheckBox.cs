﻿using System;
using WebAppAdmin.UI.Core;

namespace WebAppAdmin.UI.Elements
{
    public class CheckBox : Control
    {
        private string viewModelPropertyValue;

        public string Value
        {
            get;
            set;
        }

        public bool Checked
        {
            get;
            set;
        }

        public string ViewModelPropertyValue
        {
            get
            {
                return viewModelPropertyValue;
            }
            set
            {
                viewModelPropertyValue = value;
                Checked = Value == viewModelPropertyValue;
            }
        }

        public override string ValuePropertyName
        {
            get
            {
                return "ViewModelPropertyValue";
            }
        }

        protected override void RenderBeginTag(ViewContext viewContext)
        {
            viewContext.Content.AppendLine();
            viewContext.Content.Append("<input type=\"checkbox\"");
            if (!String.IsNullOrEmpty(this.Id))
            {
                viewContext.Content.Append(" id=\"" + this.Id + "\"");
            }
            if (!String.IsNullOrEmpty(this.Name))
            {
                viewContext.Content.Append(" name=\"" + this.Name + "\"");
            }
            if (!String.IsNullOrEmpty(this.Value))
            {
                viewContext.Content.Append(" value=\"" + this.Value + "\"");
            }
            viewContext.Content.Append((Checked ? " checked" : ""));
            if (!String.IsNullOrEmpty(this.Class))
            {
                viewContext.Content.Append(" class=\"" + this.Class + "\"");
            }
            if (this.Attributes != null)
            {
                string attrName = "";
                foreach (var attr in this.Attributes)
                {
                    attrName = attr.Name.ToLower();
                    if (attrName == "type"
                        || attrName == "id"
                        || attrName == "name"
                        || attrName == "value"
                        || attrName == "class")
                    {
                        continue;
                    }
                    viewContext.Content.Append(" " + attrName + "=\"" + attr.Value + "\"");
                }
            }
            viewContext.Content.Append("/>");
        }
    }
}
