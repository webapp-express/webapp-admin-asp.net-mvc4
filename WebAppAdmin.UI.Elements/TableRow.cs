﻿using System;
using WebAppAdmin.UI.Core;

namespace WebAppAdmin.UI.Elements
{
    public class TableRow : UIElement
    {
        protected override void RenderBeginTag(ViewContext viewContext)
        {
            viewContext.Content.AppendLine();
            viewContext.Content.Append("<tr>");
        }

        protected override void RenderEndTag(ViewContext viewContext)
        {
            viewContext.Content.AppendLine();
            viewContext.Content.Append("</tr>");
        }
    }
}
