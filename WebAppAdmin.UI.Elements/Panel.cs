﻿using System;
using WebAppAdmin.UI.Core;

namespace WebAppAdmin.UI.Elements
{
    public class Panel : UIElement
    {
        public string Html
        {
            get;
            set;
        }

        protected override void RenderBeginTag(ViewContext viewContext)
        {
            viewContext.Content.AppendLine();
            viewContext.Content.Append("<div");
            if (!String.IsNullOrEmpty(this.Id))
            {
                viewContext.Content.Append(" id=\"" + this.Id + "\"");
            }
            if (!String.IsNullOrEmpty(this.Class))
            {
                viewContext.Content.Append(" class=\"" + this.Class + "\"");
            }
            if (this.Attributes != null)
            {
                string attrName = "";
                foreach (var attr in this.Attributes)
                {
                    attrName = attr.Name.ToLower();
                    if (attrName == "id"
                        || attrName == "class")
                    {
                        continue;
                    }
                    viewContext.Content.Append(" " + attrName + "=\"" + attr.Value + "\"");
                } 
            }
            viewContext.Content.Append(">");
           
        }

        protected override void RenderContents(ViewContext viewContext)
        {
            if (!String.IsNullOrEmpty(this.Html))
            {
                viewContext.Content.Append(this.Html);
            }
            else
            {
                base.RenderContents(viewContext);
            }
        }

        protected override void RenderEndTag(ViewContext viewContext)
        {
            viewContext.Content.AppendLine();
            viewContext.Content.Append("</div>");
        }
    }
}
