﻿using System;
using WebAppAdmin.UI.Core;

namespace WebAppAdmin.UI.Elements
{
    public class EntityBox : Control
    {
        public EntityViewModel EntityViewModel
        {
            get;
            set;
        }

        public override string ValuePropertyName
        {
            get
            {
                return "EntityViewModel";
            }
        }

        protected override void RenderBeginTag(ViewContext viewContext)
        {
            viewContext.Content.AppendLine();
            viewContext.Content.Append("<div");
            if (!String.IsNullOrEmpty(this.Id))
            {
                viewContext.Content.Append(" id=\"" + this.Id + "\"");
            }
            if (!String.IsNullOrEmpty(this.Class))
            {
                viewContext.Content.Append(" class=\"" + this.Class + "\"");
            }
            if (this.Attributes != null)
            {
                string attrName = "";
                foreach (var attr in this.Attributes)
                {
                    attrName = attr.Name.ToLower();
                    if (attrName == "id"
                        || attrName == "class")
                    {
                        continue;
                    }
                    viewContext.Content.Append(" " + attrName + "=\"" + attr.Value + "\"");
                }
            }
            viewContext.Content.Append(">");

            viewContext.Content.AppendLine();
            viewContext.Content.Append("<input type=\"hidden\" class=\"EntityName\" value=\"" + (this.EntityViewModel != null ? this.EntityViewModel.EntityName : "") + "\"/>");

            viewContext.Content.AppendLine();
            viewContext.Content.Append("<input type=\"hidden\" class=\"Key\" name=\"" + (this.Name != null ? this.Name : "") + "\" value=\"" + (this.EntityViewModel != null ? this.EntityViewModel.Key : "") + "\"/>");

            viewContext.Content.AppendLine();
            viewContext.Content.Append("<input type=\"text\" value=\"" + (this.EntityViewModel != null ? this.EntityViewModel.Text : "") + "\"/>");

            viewContext.Content.AppendLine();
            viewContext.Content.Append("</div>");
        }
    }
}
