﻿using System;
using WebAppAdmin.UI.Core;

namespace WebAppAdmin.UI.Elements
{
    public class TableColumn : UIElement
    {
        internal Column Column { get; set; }

        private string GetColumnText()
        {
            if (this.Column != null)
            {
                if (ResourceManager != null)
                {
                    return ResourceManager.GetString(this.Column.Control.Name);
                }
                return this.Column.Control.Name;
            }
            return "";
        }

        protected override void RenderBeginTag(ViewContext viewContext)
        {
            viewContext.Content.AppendLine();
            viewContext.Content.Append("<th");
            if (this.Column != null
                && Mode == UIElementMode.View
                && !this.Column.Visible)
            {
                viewContext.Content.Append(" style=\"display: none;\"");
            }
            viewContext.Content.Append(">");
            viewContext.Content.Append(this.GetColumnText());
            viewContext.Content.Append("</th>");
        }
    }
}
