﻿using System;
using System.Collections.Generic;
using WebAppAdmin.UI.Core;

namespace WebAppAdmin.UI.Elements
{
    public class DropDownList : Control
    {
        private string viewModelPropertyValue;

        public List<SelectListItem> SelectList
        {
            get;
            set;
        }

        public string ViewModelPropertyValue
        {
            get
            {
                return viewModelPropertyValue;
            }
            set
            {
                viewModelPropertyValue = value;
                if (SelectList != null)
                {
                    foreach (var item in SelectList)
                    {
                        if (item.Value == viewModelPropertyValue)
                        {
                            item.Selected = true;
                        }
                    }
                }
            }
        }

        public override string ValuePropertyName
        {
            get
            {
                return "ViewModelPropertyValue";
            }
        }

        protected override void RenderBeginTag(ViewContext viewContext)
        {
            viewContext.Content.AppendLine();
            viewContext.Content.Append("<select");
            if (!String.IsNullOrEmpty(this.Id))
            {
                viewContext.Content.Append(" id=\"" + this.Id + "\"");
            }
            if (!String.IsNullOrEmpty(this.Name))
            {
                viewContext.Content.Append(" name=\"" + this.Name + "\"");
            }
            if (!String.IsNullOrEmpty(this.Class))
            {
                viewContext.Content.Append(" class=\"" + this.Class + "\"");
            }
            if (this.Attributes != null)
            {
                string attrName = "";
                foreach (var attr in this.Attributes)
                {
                    attrName = attr.Name.ToLower();
                    if (attrName == "id"
                        || attrName == "name"
                        || attrName == "class")
                    {
                        continue;
                    }
                    viewContext.Content.Append(" " + attrName + "=\"" + attr.Value + "\"");
                }
            }
            viewContext.Content.Append(">");
            viewContext.Content.AppendLine();
        }

        protected override void RenderContents(ViewContext viewContext)
        {
            if (SelectList != null)
            {
                foreach (var item in SelectList)
                {
                    viewContext.Content.Append("<option value=\"" + item.Value + "\"" + (item.Selected ? " selected" : "") + ">" + item.Text + "</option>");
                    viewContext.Content.AppendLine();
                }
            }
        }

        protected override void RenderEndTag(ViewContext viewContext)
        {
            viewContext.Content.Append("</select>");
        }
    }
}
