﻿using System;

namespace WebAppAdmin.UI.Elements
{
    public class EntityViewModel
    {
        public string EntityName { get; set; }
        public string Key { get; set; }
        public string Text { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }
}
