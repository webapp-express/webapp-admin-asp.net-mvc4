﻿using System;
using System.Collections.Generic;
using WebAppAdmin.UI.Core;

namespace WebAppAdmin.UI.Elements
{
    public class Column
    {
        public Control Control { get; set; }
        public bool Visible { get; set; }
    }
}
