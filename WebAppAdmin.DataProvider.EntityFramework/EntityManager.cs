﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using WebAppAdmin.DataProvider.Core;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Common;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;

namespace WebAppAdmin.DataProvider.EntityFramework
{
    public class EntityManager : IEntityManager, IEntityState
    {
        private readonly DbContext dbContext;

        public EntityManager(DbContext dbContext)
        {
            if (dbContext == null)
            {
                throw new ArgumentNullException("dbContext");
            }
            this.dbContext = dbContext;
        }

        public DbContext DbContext
        {
            get { return dbContext; }
        }

        public void Create(object entity)
        {
            dbContext.Entry(entity).State = EntityState.Added;
        }

        public void Update(object entity)
        {
            dbContext.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(object entity)
        {
            dbContext.Entry(entity).State = EntityState.Deleted;
        }

        public void SaveChanges()
        {
            dbContext.SaveChanges();
        }

        public IList<T> GetList<T>(ICollection<FilterItem> filter, ICollection<SortItem> sort, int skip, int take)
        {
            if (sort == null)
            {
                throw new ArgumentNullException("sort");
            }
            EntityTypeEntityFramework entityType = (EntityTypeEntityFramework)this.EntityTypeManager.GetEntityTypes().Where(item => item.Type == typeof(T)).FirstOrDefault();
            string where = "";
            List<ObjectParameter> parameters = null;
            GetEntitySqlWhere(filter, out where, out parameters);
            string queryString = "select VALUE entity from " + GetEntitySqlFrom(entityType) + " as entity" + where + GetEntitySqlOrder(sort);
            var query = ((IObjectContextAdapter)dbContext).ObjectContext.CreateQuery<T>(queryString);
            if (parameters != null)
            {
                foreach (var item in parameters)
                {
                    query.Parameters.Add(item);
                }
            }
            var queryable = query.Skip(skip);
            if (take > 0)
            {
                queryable = queryable.Take(take);
            }
            return queryable.ToList<T>();
        }


        public T Get<T>(object keyValues)
        {
            WebAppAdmin.DataProvider.Core.EntityType entityType = this.EntityTypeManager.GetEntityTypes().Where(item => item.Type == typeof(T)).FirstOrDefault();
            object entity = null;
            if (keyValues != null)
            {
                if (keyValues is IDictionary)
                {
                    IDictionary idDict = (IDictionary)keyValues;
                    object[] idArr = new object[idDict.Count];
                    idDict.Values.CopyTo(idArr, 0);
                    entity = dbContext.Set(entityType.Type).Find(idArr);
                }
                else
                {
                    entity = dbContext.Set(entityType.Type).Find(keyValues);
                }
            }
            if (entity != null)
            {
                return (T)entity;
            }
            return default(T);
        }

        public int Count<T>(ICollection<FilterItem> filter)
        {
            EntityTypeEntityFramework entityType = (EntityTypeEntityFramework)this.EntityTypeManager.GetEntityTypes().Where(item => item.Type == typeof(T)).FirstOrDefault();
            string where = "";
            List<ObjectParameter> parameters = null;
            GetEntitySqlWhere(filter, out where, out parameters);
            string queryString = "select count(0) from " + GetEntitySqlFrom(entityType) + " as entity" + where;
            var query = ((IObjectContextAdapter)dbContext).ObjectContext.CreateQuery<DbDataRecord>(queryString);
            foreach (var item in parameters)
            {
                query.Parameters.Add(item);
            }
            return (int)query.FirstOrDefault<DbDataRecord>().GetValue(0);
        }

        public void Attach(object entity)
        {
            dbContext.Entry(entity).State = EntityState.Unchanged;
        }

        public void Detach(object entity)
        {
            dbContext.Entry(entity).State = EntityState.Detached;
        }

        private void GetEntitySqlWhere(ICollection<FilterItem> filter, out string where, out List<ObjectParameter> parameters)
        {
            where = "";
            parameters = new List<ObjectParameter>();
            if (filter != null)
            {
                int paramCount = 0;
                string paramName = "";
                object paramValue = null;
                foreach (var item in filter)
                {
                    if (item.Operator != Operator.NoSet)
                    {
                        paramCount++;
                        paramName = "param" + paramCount;
                        where = where + (where == "" ? "" : (item.OperatorLogical == OperatorsLogical.And ? " and " : " or "));

                        if (item.Value != null)
                        {
                            if (item.Operator == Operator.In)
                            {
                                where = where + " entity." + item.PropertyName + " in (@" + paramName + ")";
                            }
                            else if (item.Operator == Operator.NotIn)
                            {
                                where = where + " entity." + item.PropertyName + " not in (@" + paramName + ")";
                            }
                            else if (item.Operator == Operator.Between)
                            {
                                where = where + " entity." + item.PropertyName + " between @" + paramName + "_1" + " and @" + paramName + "_2";
                            }
                            else
                            {
                                where = where + " entity." + item.PropertyName + " " + GetSqlOperator(item.Operator) + " @" + paramName;
                            }

                            if (item.Operator == Operator.Between)
                            {
                                parameters.Add(new ObjectParameter(paramName + "_1", ((object[])item.Value)[0]));
                                parameters.Add(new ObjectParameter(paramName + "_2", ((object[])item.Value)[1]));
                            }
                            else
                            {
                                if (item.Operator == Operator.Like)
                                {
                                    paramValue = "%" + item.Value.ToString() + "%";
                                }
                                else
                                {
                                    paramValue = item.Value;
                                }
                                parameters.Add(new ObjectParameter(paramName, paramValue));
                            }
                        }
                        else
                        {
                            if (item.Operator == Operator.Equal)
                            {
                                where = where + " entity." + item.PropertyName + " is null";
                            }
                            else if (item.Operator == Operator.NotEqual)
                            {
                                where = where + " entity." + item.PropertyName + " is not null";
                            }
                        }
                    }
                }

                where = (where == "" ? "" : " where ") + where;
            }
        }

        private string GetSqlOperator(Operator oper)
        {
            switch (oper)
            {
                case Operator.Equal:
                    return "=";
                case Operator.NotEqual:
                    return "<>";
                case Operator.GreaterThan:
                    return ">";
                case Operator.GreaterThanOrEqual:
                    return ">=";
                case Operator.LessThan:
                    return "<";
                case Operator.LessThanOrEqual:
                    return "<=";
                case Operator.Like:
                    return "like";
                default:
                    return "";
            }
        }

        private string GetEntitySqlFrom(EntityTypeEntityFramework entityType)
        {
            if (!String.IsNullOrEmpty(entityType.Discriminator))
            {
                return "oftype(" + dbContext.GetType().Name + "." + entityType.EnitySetName+ ","+ entityType.Name + ")";
            }
            return dbContext.GetType().Name + "." + entityType.EnitySetName;
        }

        private string GetEntitySqlOrder(ICollection<SortItem> sort)
        {
            string order = "";
            foreach (var item in sort)
            {
                order = order + (order != "" ? "," : "") + "entity." + item.PropertyName + (item.SortDirection == SortDirection.Descending ? " desc" : " asc");
            }
            return " order by " + order;
        }

        public IEntityTypeManager EntityTypeManager
        {
            get
            {
                return new EntityTypeManager(dbContext);
            }
        }

        public void Dispose()
        {
            if (dbContext != null)
            {
                dbContext.Dispose();
            }
        }
    }
}
