﻿using System;
using System.Collections.ObjectModel;
using WebAppAdmin.DataProvider.Core;

namespace WebAppAdmin.DataProvider.EntityFramework
{
    public class EntityTypeNavigationProperty : EntityTypeProperty, IDependentProperties
    {
        public ReadOnlyCollection<EntityTypeProperty> DependentProperties
        {
            get;
            set;
        }
    }
}
