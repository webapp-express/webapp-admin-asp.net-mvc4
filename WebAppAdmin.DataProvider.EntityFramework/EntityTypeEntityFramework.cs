﻿using System;
using WebAppAdmin.DataProvider.Core;

namespace WebAppAdmin.DataProvider.EntityFramework
{
    public class EntityTypeEntityFramework : EntityType, IDiscriminator
    {
        public string EnitySetName
        {
            get;
            set;
        }

        public string Discriminator
        {
            get;
            set;
        }

        public Type BaseType
        {
            get;
            set;
        }
    }
}
