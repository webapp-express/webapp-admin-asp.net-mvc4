﻿using System;

namespace WebAppAdmin.UI.Core
{
    public class Attribute
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
