﻿using System;
using System.Collections.Generic;

namespace WebAppAdmin.UI.Core
{
    public interface ItemsControl
    {
        List<Control> ItemControls { get; }
    }
}
