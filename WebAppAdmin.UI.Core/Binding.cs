﻿using System;

namespace WebAppAdmin.UI.Core
{
    public class Binding
    {
        public Binding(string uiElementPropertyName, object source, string sourcePropertyName)
        {
            if (uiElementPropertyName == null)
            {
                throw new ArgumentNullException("uiElementPropertyName");
            }
            if (String.IsNullOrEmpty(sourcePropertyName))
            {
                throw new ArgumentNullException("sourcePropertyPath");
            }
            this.UIElementPropertyName = uiElementPropertyName;
            this.Source = source;
            this.SourcePropertyName = sourcePropertyName;
        }

        public Binding(string uiElementPropertyName, string sourcePropertyName)
            : this(uiElementPropertyName, null, sourcePropertyName)
        {

        }

        private Binding()
        { }

        public string UIElementPropertyName { get; set; }
        public Object Source { get; set; }
        public string SourcePropertyName { get; set; }
    }
}
