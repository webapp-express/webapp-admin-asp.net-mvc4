﻿using System;

namespace WebAppAdmin.UI.Core
{
    public enum UIElementMode
    {
        View,
        Edit
    }
}
