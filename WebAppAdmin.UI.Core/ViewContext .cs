﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebAppAdmin.UI.Core
{
    public class ViewContext
    {
        private StringBuilder content;
        private List<Resource> resources;

        public ViewContext()
        {
            this.content = new StringBuilder();
            this.resources = new List<Resource>();
        }

        public StringBuilder Content
        {
            get { return content; }
        }

        public List<Resource> Resources
        {
            get { return resources; }
        }
    }
}
