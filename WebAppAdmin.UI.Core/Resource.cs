﻿using System;

namespace WebAppAdmin.UI.Core
{
    public class Resource
    {
        public ResourceType Type { get; set; }
        public string Value { get; set; }
    }
}
