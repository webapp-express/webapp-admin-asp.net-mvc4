﻿using System;

namespace WebAppAdmin.UI.Core
{
    public abstract class Control : UIElement
    {
        public string Name { get; set; }
        public abstract string ValuePropertyName { get; }
    }
}
