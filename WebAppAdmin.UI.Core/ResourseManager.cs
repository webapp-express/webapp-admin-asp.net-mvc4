﻿using System;

namespace WebAppAdmin.UI.Core
{
    public abstract class ResourceManager
    {
        public abstract string GetString(string name);
        public abstract string GetString(string name, string culture);
    }
}
