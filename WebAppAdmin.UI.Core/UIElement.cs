﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Xml.Serialization;

namespace WebAppAdmin.UI.Core
{
    public abstract class UIElement : ICloneable
    {
        private ObservableCollection<UIElement> children;
        private List<Binding> dataBindings;
        private ResourceManager resourceManager;
        private UIElementMode mode;

        public string Id { get; set; }

        public List<Attribute> Attributes { get; set; }

        public string Class { get; set; }

        public List<Resource> Resources { get; set; }

        public UIElement Parent { get; set; }

        public ObservableCollection<UIElement> Children
        {
            get
            {
                if (children == null)
                {
                    children = new ObservableCollection<UIElement>();
                    children.CollectionChanged += children_CollectionChanged;
                }
                return children;
            }
        }

        private void children_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (UIElement item in e.NewItems)
                {
                    item.Parent = this;
                    item.Mode = this.Mode;
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (UIElement item in e.OldItems)
                {
                    item.Parent = null;
                }
            }
        }

        public object ViewModel { get; set; }

        public List<Binding> DataBindings
        {
            get
            {
                if (dataBindings == null)
                {
                    dataBindings = new List<Binding>();
                }
                return dataBindings;
            }
        }

        public ResourceManager ResourceManager
        {
            get
            {
                return resourceManager;
            }
            set
            {
                resourceManager = value;
                foreach (var element in Children)
                {
                    element.ResourceManager = resourceManager;
                }
            }
        }

        public UIElementMode Mode
        {
            get
            {
                return mode;
            }
            set
            {
                mode = value;
                foreach (var element in Children)
                {
                    element.Mode = mode;
                }
            }
        }

        public void Render(ViewContext viewContext)
        {
            this.Bind();

            if (this.Resources != null)
            {
                viewContext.Resources.AddRange(this.Resources);
            }

            this.RenderBeginTag(viewContext);
            this.RenderContents(viewContext);
            this.RenderEndTag(viewContext);
        }

        private void Bind()
        {
            object propVal = null;
            object bindingSource = null;
            foreach (var binding in this.DataBindings)
            {
                bindingSource = binding.Source;
                if (bindingSource == null)
                {
                    bindingSource = this.GetBindingSource(this);
                }
                if (bindingSource != null)
                {
                    if (bindingSource is IDictionary)
                    {
                        propVal = ((IDictionary)bindingSource)[binding.SourcePropertyName];
                    }
                    else
                    {
                        propVal = bindingSource.GetType().GetProperty(binding.SourcePropertyName).GetValue(bindingSource, null);
                    }
                    PropertyInfo propertyInfo = this.GetType().GetProperty(binding.UIElementPropertyName);
                    propertyInfo.SetValue(this, propVal, null);
                }
            }
        }

        private object GetBindingSource(UIElement uiElement)
        {
            if (uiElement.ViewModel != null)
            {
                return uiElement.ViewModel;
            }
            else if (uiElement.Parent != null)
            {
                return GetBindingSource(uiElement.Parent);
            }
            return null;
        }

        protected virtual void RenderBeginTag(ViewContext viewContext)
        {
        }

        protected virtual void RenderContents(ViewContext viewContext)
        {
            foreach (var element in Children)
            {
                element.Render(viewContext);
            }
        }

        protected virtual void RenderEndTag(ViewContext viewContext)
        {
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
