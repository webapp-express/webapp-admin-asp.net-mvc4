﻿using System;

namespace WebAppAdmin.DataProvider.Core
{
    public interface IDiscriminator
    {
        string Discriminator { get; set; }
        Type BaseType { get; set; }
    }
}
