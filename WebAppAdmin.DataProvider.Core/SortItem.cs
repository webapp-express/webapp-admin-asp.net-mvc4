﻿using System;

namespace WebAppAdmin.DataProvider.Core
{
    public class SortItem
    {
        public string PropertyName
        {
            get;
            set;
        }

        public SortDirection SortDirection
        {
            get;
            set;
        }
    }
}
