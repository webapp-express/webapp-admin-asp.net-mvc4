﻿using System;
using System.Collections.ObjectModel;

namespace WebAppAdmin.DataProvider.Core
{
    public interface IDependentProperties
    {
        ReadOnlyCollection<EntityTypeProperty> DependentProperties
        {
            get;
            set;
        }
    }
}
