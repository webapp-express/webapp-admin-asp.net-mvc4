﻿using System;
using System.Collections.ObjectModel;

namespace WebAppAdmin.DataProvider.Core
{
    public interface IEntityTypeManager
    {
        /// <summary>Gets all the items in the specified data model.</summary>
        /// <returns>
        /// A collection of type <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection`1" /> that contains all the items in the specified data model.
        /// </returns>
        ReadOnlyCollection<EntityType> GetEntityTypes();
    }
}
