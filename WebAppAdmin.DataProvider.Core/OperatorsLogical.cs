using System;

namespace WebAppAdmin.DataProvider.Core
{
    public enum OperatorsLogical
    {
        And,
        Or
    }
}
