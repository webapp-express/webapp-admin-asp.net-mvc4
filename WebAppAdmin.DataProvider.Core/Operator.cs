﻿using System;

namespace WebAppAdmin.DataProvider.Core
{
    public enum Operator
    {
        NoSet,
        Equal,
        NotEqual,
        GreaterThan,
        GreaterThanOrEqual,
        LessThan,
        LessThanOrEqual,
        Like,
        NotLike,
        In,
        NotIn,
        Between
    }
}
