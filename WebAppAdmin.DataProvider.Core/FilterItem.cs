using System;

namespace WebAppAdmin.DataProvider.Core
{
    public class FilterItem
    {
        private string propertyName;
        private Object value;
        private Operator oper;
        private OperatorsLogical operatorLogical;
        private bool readOnly;

        public string PropertyName
        {
            get { return propertyName; }
            set
            {
                if (!readOnly)
                {
                    propertyName = value;
                }
            }
        }

        public Object Value
        {
            get 
            { 
                return this.value;
            }
            set 
            {
                if (!readOnly)
                {
                    this.value = value;
                }
            }
        }

        public Operator Operator
        {
            get { return oper; }
            set 
            {
                if (!readOnly)
                {
                    oper = value;  
                }
            }
        }

        public OperatorsLogical OperatorLogical
        {
            get { return operatorLogical; }
            set
            {
                if (!readOnly)
                {
                    operatorLogical = value;
                }
            }
        }

        public bool ReadOnly
        {
            get { return readOnly; }
            set { readOnly = value; }
        }
    }
}
