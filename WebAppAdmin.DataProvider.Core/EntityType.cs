﻿using System;
using System.Collections.ObjectModel;

namespace WebAppAdmin.DataProvider.Core
{
    public class EntityType
    {
        public string Name
        {
            get;
            set;
        }

        public Type Type
        {
            get;
            set;
        }

        public ReadOnlyCollection<EntityTypeProperty> Properties
        {
            get;
            set;
        }
    }
}
