﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace WebAppAdmin.DataProvider.Core
{
    public interface IEntityManager : IDisposable
    {
        /// <summary>
        /// Marks the given entity as Created such that it will be created from the database when SaveChanges
        /// is called. 
        /// </summary>
        /// <param name="entity"> The entity to create. </param>
        void Create(object entity);

        /// <summary>
        /// Marks the given entity as Updated such that it will be updated from the database when SaveChanges
        /// is called. 
        /// </summary>
        /// <param name="entity"> The entity to create. </param>
        void Update(object entity);

        /// <summary>
        /// Marks the given entity as Deleted such that it will be deleted from the database when SaveChanges
        /// is called. 
        /// </summary>
        /// <param name="entity"> The entity to create. </param>
        void Delete(object entity);

        /// <summary>
        /// Saves all changes made in this context to the underlying database.
        /// </summary>
        void SaveChanges();

        /// <summary>
        /// Return an <see cref="IList"/> of the entity type.
        /// </summary>
        /// <typeparam name="T">
        /// The entity type of the returned.
        /// </typeparam>
        /// <returns>An <see cref="IList"/> of the entity type.</returns>
        IList<T> GetList<T>(ICollection<FilterItem> filter, ICollection<SortItem> sort, int skip, int take);

        /// <summary>
        /// Get an entity with the given primary key values.
        /// </summary>
        /// <param name="keyValues"> The values of the primary key for the entity to be found. </param>
        /// <typeparam name="T">
        /// The entity type of the returned.
        /// </typeparam>
        /// <returns> The entity found, or null. </returns>
        T Get<T>(object keyValues);

        /// <summary>
        ///  Returns the number of the entity type.
        /// </summary>
        /// <typeparam name="T">
        /// The entity type of the returned.
        /// </typeparam>
        /// <returns>The number of the entity type.</returns>
        int Count<T>(ICollection<FilterItem> filter);

        /// <summary>Gets the metadata used by the entity manager. </summary>
        IEntityTypeManager EntityTypeManager
        {
            get;
        }
    }
}
