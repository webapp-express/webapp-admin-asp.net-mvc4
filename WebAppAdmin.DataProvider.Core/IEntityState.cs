﻿using System;

namespace WebAppAdmin.DataProvider.Core
{
    public interface IEntityState
    {
        void Attach(object entity);

        void Detach(object entity);
    }
}
