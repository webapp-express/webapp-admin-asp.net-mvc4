﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using WebAppAdmin.DataProvider.Core;
using NHibernate;
using Hibernate = NHibernate;
using NHibernate.Metadata;
using NHibernate.Engine;
using NHibernate.Persister.Entity;

namespace WebAppAdmin.DataProvider.NHibernate
{
    public class EntityManager : IEntityManager, IEntityState
    {
        private readonly ISessionFactory sessionFactory;
        private readonly ISession session;
        private ITransaction transaction;

        public EntityManager(ISessionFactory sessionFactory)
        {
            if (sessionFactory == null)
            {
                throw new ArgumentNullException("sessionFactory");
            }
            this.sessionFactory = sessionFactory;
            this.session = sessionFactory.OpenSession();
            this.session.FlushMode = FlushMode.Commit;
        }

        public ISession Session
        {
            get { return session; }
        }

        private void BeginTransaction()
        {
            if (transaction == null)
            {
                transaction = session.BeginTransaction();
            }
        }

        public void Create(object entity)
        {
            BeginTransaction();
            session.Save(entity);
        }

        public void Update(object entity)
        {
            BeginTransaction();
            session.Update(entity);
        }

        public void Delete(object entity)
        {
            BeginTransaction();
            session.Delete(entity);
        }

        public void SaveChanges()
        {
            transaction.Commit();
            transaction = null;
        }

        public IList<T> GetList<T>(ICollection<FilterItem> filter, ICollection<SortItem> sort, int skip, int take)
        {
            if (sort == null)
            {
                throw new ArgumentNullException("sort");
            }
            WebAppAdmin.DataProvider.Core.EntityType entityType = this.EntityTypeManager.GetEntityTypes().Where(item => item.Type == typeof(T)).FirstOrDefault();
            string where = "";
            Dictionary<string, object> parameters = null;
            GetHqlWhere(filter, out where, out parameters);
            string queryString = "from " + entityType.Name + " as entity" + where + GetHqlOrder(sort);
            var query = session.CreateQuery(queryString);
            foreach (var item in parameters)
            {
                if (item.Value is IList)
                {
                    query.SetParameterList(item.Key, (IList)item.Value);
                }
                else
                {
                    query.SetParameter(item.Key, item.Value);
                }
            }
            query = query.SetFirstResult(skip);
            if (take > 0)
            {
                query = query.SetMaxResults(take);
            }
            return query.List<T>();
        }

        public T Get<T>(object keyValues)
        {
            if (keyValues is IDictionary)
            {
                IDictionary idDict = (IDictionary)keyValues;
                object entity = Activator.CreateInstance<T>();
                foreach (var key in idDict.Keys)
                {
                    entity.GetType().GetProperty(key.ToString()).SetValue(entity, idDict[key], null);
                }
                return session.Get<T>(entity);
            }
            else
            {
                return session.Get<T>(keyValues);
            }
        }

        public int Count<T>(ICollection<FilterItem> filter)
        {
            WebAppAdmin.DataProvider.Core.EntityType entityType = this.EntityTypeManager.GetEntityTypes().Where(item => item.Type == typeof(T)).FirstOrDefault();
            string where = "";
            Dictionary<string, object> parameters = null;
            GetHqlWhere(filter, out where, out parameters);
            string queryString = "select count(*) from " + entityType.Name + " as entity" + where;
            var query = session.CreateQuery(queryString);
            foreach (var item in parameters)
            {
                if (item.Value is IList)
                {
                    query.SetParameterList(item.Key, (IList)item.Value);
                }
                else
                {
                    query.SetParameter(item.Key, item.Value);
                }
            }
            return Convert.ToInt32(query.UniqueResult());
        }

        public void Attach(object entity)
        {
            ISessionImplementor sessionImplementor = (ISessionImplementor)session;
            IEntityPersister persister = sessionImplementor.GetEntityPersister(null, entity);
            object id = persister.GetIdentifier(entity, sessionImplementor.EntityMode);
            EntityKey entityKey = sessionImplementor.GenerateEntityKey(id, persister);
            if (!sessionImplementor.PersistenceContext.ContainsEntity(entityKey))
            {
                session.Lock(entity, LockMode.None);
            }
        }

        public void Detach(object entity)
        {
            session.Evict(entity);
        }

        private void GetHqlWhere(ICollection<FilterItem> filter, out string where, out Dictionary<string, object> parameters)
        {
            where = "";
            parameters = new Dictionary<string, object>();
            if (filter != null)
            {
                int paramCount = 0;
                string paramName = "";
                object paramValue = null;
                foreach (var item in filter)
                {
                    if (item.Operator != Operator.NoSet)
                    {
                        paramCount++;
                        paramName = "param" + paramCount;
                        where = where + (where == "" ? "" : (item.OperatorLogical == OperatorsLogical.And ? " and " : " or "));

                        if (item.Value != null)
                        {
                            if (item.Operator == Operator.In)
                            {
                                where = where + " entity." + item.PropertyName + " in (@" + paramName + ")";
                            }
                            else if (item.Operator == Operator.NotIn)
                            {
                                where = where + " entity." + item.PropertyName + " not in (@" + paramName + ")";
                            }
                            else if (item.Operator == Operator.Between)
                            {
                                where = where + " entity." + item.PropertyName + " between @" + paramName + "_1" + " and @" + paramName + "_2";
                            }
                            else
                            {
                                where = where + " entity." + item.PropertyName + " " + GetSqlOperator(item.Operator) + " @" + paramName;
                            }

                            if (item.Operator == Operator.Between)
                            {
                                parameters.Add(paramName + "_1", ((object[])item.Value)[0]);
                                parameters.Add(paramName + "_2", ((object[])item.Value)[1]);
                            }
                            else
                            {
                                if (item.Operator == Operator.Like)
                                {
                                    paramValue = "%" + item.Value.ToString() + "%";
                                }
                                else
                                {
                                    paramValue = item.Value;
                                }
                                parameters.Add(paramName, paramValue);
                            }
                        }
                        else
                        {
                            if (item.Operator == Operator.Equal)
                            {
                                where = where + " entity." + item.PropertyName + " is null";
                            }
                            else if (item.Operator == Operator.NotEqual)
                            {
                                where = where + " entity." + item.PropertyName + " is not null";
                            }
                        }
                    }
                }

                where = (where == "" ? "" : " where ") + where;
            }
        }

        private string GetSqlOperator(Operator oper)
        {
            switch (oper)
            {
                case Operator.Equal:
                    return "=";
                case Operator.NotEqual:
                    return "<>";
                case Operator.GreaterThan:
                    return ">";
                case Operator.GreaterThanOrEqual:
                    return ">=";
                case Operator.LessThan:
                    return "<";
                case Operator.LessThanOrEqual:
                    return "<=";
                case Operator.Like:
                    return "like";
                default:
                    return "";
            }
        }

        private string GetHqlOrder(ICollection<SortItem> sort)
        {
            string order = "";
            foreach (var item in sort)
            {
                order = order + (order != "" ? "," : "") + "entity." + item.PropertyName + (item.SortDirection == SortDirection.Descending ? " desc" : " asc");
            }
            return " order by " + order;
        }

        public IEntityTypeManager EntityTypeManager
        {
            get
            {
                return new EntityTypeManager(sessionFactory);
            }
        }

        public void Dispose()
        {
            if (session != null)
            {
                session.Dispose();
            }
        }
    }
}
