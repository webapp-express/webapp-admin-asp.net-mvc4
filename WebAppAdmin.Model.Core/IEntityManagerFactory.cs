﻿using System;
using System.Collections.Generic;
using WebAppAdmin.DataProvider.Core;

namespace WebAppAdmin.Model.Core
{
    public interface IEntityManagerFactory
    {
        IEntityManager GetInstance(string name);
        IEnumerable<string> GetNames();
    }
}
