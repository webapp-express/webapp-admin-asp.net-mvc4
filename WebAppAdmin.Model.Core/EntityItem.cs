﻿using System;

namespace WebAppAdmin.Model.Core
{
    public class EntityItem
    {
        public string EntityManagerName
        {
            get;
            set;
        }

        public string EntityName
        {
            get;
            set;
        }
    }
}
