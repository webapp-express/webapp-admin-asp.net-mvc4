﻿using System;
using System.Collections.Generic;

namespace WebAppAdmin.Model.Core
{
    public interface IEntityItemManager
    {
        IDictionary<string, IEnumerable<EntityItem>> GetEntityItems();
    }
}
