﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebAppAdmin.Areas.Admin.Resources.EntityView {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class _Edit {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal _Edit() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("WebAppAdmin.Areas.Admin.Resources.EntityView._Edit", typeof(_Edit).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add.
        /// </summary>
        public static string Add {
            get {
                return ResourceManager.GetString("Add", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add properies.
        /// </summary>
        public static string Add_properies {
            get {
                return ResourceManager.GetString("Add_properies", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clear.
        /// </summary>
        public static string Clear {
            get {
                return ResourceManager.GetString("Clear", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Close.
        /// </summary>
        public static string Close {
            get {
                return ResourceManager.GetString("Close", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Commands.
        /// </summary>
        public static string Commands {
            get {
                return ResourceManager.GetString("Commands", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Down.
        /// </summary>
        public static string Down {
            get {
                return ResourceManager.GetString("Down", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to EditView.
        /// </summary>
        public static string EditView {
            get {
                return ResourceManager.GetString("EditView", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ListView.
        /// </summary>
        public static string ListView {
            get {
                return ResourceManager.GetString("ListView", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Properties.
        /// </summary>
        public static string Properties {
            get {
                return ResourceManager.GetString("Properties", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Property editor.
        /// </summary>
        public static string Property_editor {
            get {
                return ResourceManager.GetString("Property_editor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reload.
        /// </summary>
        public static string Reload {
            get {
                return ResourceManager.GetString("Reload", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Remove.
        /// </summary>
        public static string Remove {
            get {
                return ResourceManager.GetString("Remove", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save.
        /// </summary>
        public static string Save {
            get {
                return ResourceManager.GetString("Save", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sort by.
        /// </summary>
        public static string Sort_by {
            get {
                return ResourceManager.GetString("Sort_by", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Up.
        /// </summary>
        public static string Up {
            get {
                return ResourceManager.GetString("Up", resourceCulture);
            }
        }
    }
}
