﻿using System;
using System.Collections.Generic;

namespace WebAppAdmin.Areas.Admin.Models
{
    public class ListItem
    {
        public string Key { get; set; }
        public Dictionary<string, object> Properties { get; set; }
    }
}