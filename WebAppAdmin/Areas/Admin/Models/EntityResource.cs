﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebAppAdmin.Areas.Admin.Models
{
    public class EntityResource
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Value { get; set; }
    }
}