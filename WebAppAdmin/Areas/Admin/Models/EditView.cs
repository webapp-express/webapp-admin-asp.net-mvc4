﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using WebAppAdmin.DataProvider.Core;
using WebAppAdmin.UI.Core;

namespace WebAppAdmin.Areas.Admin.Models
{
    public class EditView
    {
        [Required]
        public string EntityManagerName
        {
            get;
            set;
        }

        [Required]
        public string EntityName
        {
            get;
            set;
        }

        [Required]
        [Display(ResourceType = typeof(WebAppAdmin.Areas.Admin.Resources.EntityView._EditView), Name = "Controller_name")]
        public string ControllerName
        {
            get;
            set;
        }

        [Required]
        [Display(ResourceType = typeof(WebAppAdmin.Areas.Admin.Resources.EntityView._EditView), Name = "Action_name")]
        public string ActionName
        {
            get;
            set;
        }

        [Required]
        [Display(ResourceType = typeof(WebAppAdmin.Areas.Admin.Resources.EntityView._EditView), Name = "View_name")]
        public string Name
        {
            get;
            set;
        }

        public UIElement View
        {
            get;
            set;
        }

        [IgnoreDataMember]
        public EntityType EntityType
        {
            get;
            set;
        }

        [IgnoreDataMember]
        public List<EntityTypeProperty> Properties
        {
            get;
            set;
        }
    }
}