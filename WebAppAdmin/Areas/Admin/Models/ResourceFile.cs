﻿using System;

namespace WebAppAdmin.Areas.Admin.Models
{
    public class ResourceFile
    {
        public string Name { get; set; }
        public string EntityManagerName { get; set; }
        public string EntityName { get; set; }
        public string CultureName { get; set; }
    }
}