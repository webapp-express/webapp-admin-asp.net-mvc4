﻿using System;

namespace WebAppAdmin.Areas.Admin.Models
{
    public enum FileBrowserItemType
    {
        File,
        Image,
        Directory
    }
}