﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using WebAppAdmin.DataProvider.Core;

namespace WebAppAdmin.Areas.Admin.Models
{
    public class ListView : IDataErrorInfo
    {
        [Required]
        public string EntityManagerName
        {
            get;
            set;
        }

        [Required]
        public string EntityName
        {
            get;
            set;
        }

        [Required]
        [Display(ResourceType = typeof(WebAppAdmin.Areas.Admin.Resources.EntityView._ListView), Name = "Controller_name")]
        public string ControllerName
        {
            get;
            set;
        }

        [Required]
        [Display(ResourceType = typeof(WebAppAdmin.Areas.Admin.Resources.EntityView._ListView), Name = "Action_name")]
        public string ActionName
        {
            get;
            set;
        }

        [Required]
        [Display(ResourceType = typeof(WebAppAdmin.Areas.Admin.Resources.EntityView._ListView), Name = "View_name")]
        public string Name
        {
            get;
            set;
        }

        [IgnoreDataMember]
        public string Mode
        {
            get;
            set;
        }

        [IgnoreDataMember]
        public EntityType EntityType
        {
            get;
            set;
        }

        [Display(ResourceType = typeof(WebAppAdmin.Areas.Admin.Resources.EntityView._ListView), Name = "Properties")]
        public List<ListViewProperty> Properties
        {
            get;
            set;
        }

        [IgnoreDataMember]
        public bool ShowFilter
        {
            get;
            set;
        }

        [Display(ResourceType = typeof(WebAppAdmin.Areas.Admin.Resources.EntityView._ListView), Name = "Filter_properties")]
        public List<ListViewProperty> FilterProperties
        {
            get;
            set;
        }

        public List<FilterItem> Filter
        {
            get;
            set;
        }

        [Required]
        public SortItem SortDefault
        {
            get;
            set;
        }

        [IgnoreDataMember]
        public List<SortItem> Sort
        {
            get;
            set;
        }

        [IgnoreDataMember]
        public int PageNumber
        {
            get;
            set;
        }

        [Required]
        [Range(1, Int32.MaxValue)]
        [Display(ResourceType = typeof(WebAppAdmin.Areas.Admin.Resources.EntityView._ListView), Name = "Page_size")]
        public int PageSize
        {
            get;
            set;
        }

        [IgnoreDataMember]
        public string Sender
        {
            get;
            set;
        }

        [IgnoreDataMember]
        public List<ListItem> List
        {
            get;
            set;
        }

        [IgnoreDataMember]
        public int Count
        {
            get;
            set;
        }

        [IgnoreDataMember]
        public ModelErrors ModelErrors
        {
            get;
            set;
        }

        public string Error
        {
            get
            {
                return "";
            }
        }

        public string this[string propertyName]
        {
            get
            {
                return ValidateProperty(propertyName);
            }
        }

        private string ValidateProperty(string propertyName)
        {
            switch (propertyName)
            {
                case "Properties":
                    if (Properties == null
                        || Properties.Where(item => item.IsActive == true).Count() == 0)
                    {
                        return new RequiredAttribute().FormatErrorMessage(propertyName);
                    }
                    break;
                case "FilterProperties":
                    if (FilterProperties == null
                        || FilterProperties.Where(item => item.IsActive == true).Count() == 0)
                    {
                        return new RequiredAttribute().FormatErrorMessage(propertyName);
                    }
                    break;
            }
            return "";
        }
    }
}