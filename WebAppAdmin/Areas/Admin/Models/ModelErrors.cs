﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace WebAppAdmin.Areas.Admin.Models
{
    public class ModelErrors
    {
        public ModelErrors()
        {
            this.Errors = new List<string>();
            this.PropertyErrors = new Dictionary<string, List<string>>();
        }

        public ModelErrors(ModelStateDictionary modelState)
            : this()
        {
            foreach (var item in modelState)
            {
                if (item.Key == "")
                {
                    foreach (var errorItem in item.Value.Errors)
                    {
                        this.Errors.Add(errorItem.ErrorMessage);
                    }
                }
                else
                {
                    List<string> propErrors = new List<string>();
                    foreach (var errorItem in item.Value.Errors)
                    {
                        propErrors.Add(errorItem.ErrorMessage);
                    }
                    if (propErrors.Count > 0)
                    {
                        this.PropertyErrors.Add(item.Key, propErrors);
                    }
                }
            }
        }

        public ModelErrors(Exception exception)
            : this()
        {
            this.Errors.Add(exception.Message);
            while (exception.InnerException != null)
            {
                if (!this.Errors.Contains(exception.InnerException.Message))
                {
                    this.Errors.Add(exception.InnerException.Message); 
                }
                exception = exception.InnerException;
            }
        }

        public List<string> Errors { get; set; }
        public Dictionary<string, List<string>> PropertyErrors { get; set; }

        public bool HasErrors()
        {
            return this.PropertyErrors.Count > 0 || this.Errors.Count > 0;
        }
    }
}