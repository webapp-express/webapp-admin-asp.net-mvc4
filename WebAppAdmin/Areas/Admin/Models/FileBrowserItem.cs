﻿using System;

namespace WebAppAdmin.Areas.Admin.Models
{
    public class FileBrowserItem
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public FileBrowserItemType Type { get; set; }
    }
}
