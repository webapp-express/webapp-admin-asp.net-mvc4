﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebAppAdmin.Areas.Admin.Models
{
    public class Culture
    {
        [Required]
        public string Name { get; set; }
    }
}