﻿using System;

namespace WebAppAdmin.Areas.Admin.Models
{
    public class EntityViewFile
    {
        public string Type { get; set; }
        public string LastWriteDateTime { get; set; }
    }
}