﻿using System;

namespace WebAppAdmin.Areas.Admin.Models
{
    public class ListViewProperty
    {
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}