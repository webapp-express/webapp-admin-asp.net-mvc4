﻿using System;
using System.Collections.Generic;
using WebAppAdmin.DataProvider.Core;
using WebAppAdmin.UI.Core;
using WebAppAdmin.UI.View.Core;
using WebAppAdmin.UI.Elements;

namespace WebAppAdmin.Areas.Admin.Helpers
{
    public class ViewGenerator : IViewGenerator
    {
        private IControlConvention controlConvention;

        public ViewGenerator(IControlConvention controlConvention)
        {
            if (controlConvention == null)
            {
                throw new ArgumentNullException("controlConvention");
            }
            this.controlConvention = controlConvention;
        }

        public virtual UIElement CreateEntityView(EntityType entityType, IEntityTypeManager entityTypeManager)
        {
            UIElementIdGenerator uiElementIdGenerator = new UIElementIdGenerator(0);
            Panel panel = (Panel)UIElementManager.GetUIElement("Panel");
            panel.Id = UIElementIdGenerator.Prefix + uiElementIdGenerator.GetId();
            panel.Class = "form-body";

            List<EntityTypeProperty> properties = EditViewManager.GetProperties(entityType, entityTypeManager, controlConvention);
            foreach (var entityTypeProperty in properties)
            {
                IList<string> controls = controlConvention.MatchControls(entityTypeProperty.Name, entityTypeProperty.Type, entityType, entityTypeManager);
                if (controls != null
                    && controls.Count > 0)
                {
                    Control control = UIElementManager.GetControl(controls[0]);
                    if (control != null)
                    {
                        UIElement uiElement = this.CreateEntityPropertyView(entityType, entityTypeProperty, entityTypeManager, control, controlConvention);
                        if (uiElement != null)
                        {
                            uiElement.Id = UIElementIdGenerator.Prefix + uiElementIdGenerator.GetId();
                            foreach (var child in uiElement.Children)
                            {
                                child.Id = UIElementIdGenerator.Prefix + uiElementIdGenerator.GetId();
                            }
                            panel.Children.Add(uiElement);
                        }
                    }
                }
            }
            return panel;
        }

        public virtual UIElement CreateEntityPropertyView(EntityType entityType, EntityTypeProperty entityTypeProperty, IEntityTypeManager entityTypeManager, Control control, IControlConvention controlConvention)
        {
            return UIElementManager.CreatePropertyContainer(entityType, entityTypeProperty, entityTypeManager, control, controlConvention);
        }
    }
}