﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebAppAdmin.DataProvider.Core;
using WebAppAdmin.UI.Elements;

namespace WebAppAdmin.Areas.Admin.Helpers
{
    internal class EntityViewModelConverter
    {
        internal static EntityViewModel ConvertToEntityItem(object entity, EntityType entityType)
        {
            return new EntityViewModel() { EntityName = entityType.Name, Key = (entity != null ? EntityKeysConverter.ConverKeyToString(entity, entityType) : ""), Text = (entity != null ? entity.ToString() : "") };
        }

        internal static object ConvertToEntity(string id, EntityType entityType)
        {
            if (!String.IsNullOrEmpty(id))
            {
                object key = EntityKeysConverter.ConvertToKeyType(id, entityType);
                List<EntityTypeProperty> keyProperties = entityType.Properties.Where<EntityTypeProperty>(item => item.IsKey).ToList();
                object entity = Activator.CreateInstance(entityType.Type);
                if (keyProperties.Count == 1)
                {
                    entity.GetType().GetProperty(keyProperties[0].Name).SetValue(entity, key, null);
                }
                else
                {
                    for (int i = 0; i < keyProperties.Count; i++)
                    {
                        entity.GetType().GetProperty(keyProperties[i].Name).SetValue(entity, ((object[])key)[i], null);
                    }
                }
                return entity;
            }
            else
            {
                return null;
            }
        }
    }
}