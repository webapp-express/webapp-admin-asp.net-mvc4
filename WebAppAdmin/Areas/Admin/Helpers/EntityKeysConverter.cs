﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebAppAdmin.DataProvider.Core;

namespace WebAppAdmin.Areas.Admin.Helpers
{
    internal class EntityKeysConverter
    {
        internal static object ConvertToKeyType(string id, EntityType entityType)
        {
            List<EntityTypeProperty> keyProperties = entityType.Properties.Where<EntityTypeProperty>(item => item.IsKey).ToList();
            if (keyProperties.Count == 1)
            {
                if (keyProperties[0].Type.GetInterface("IConvertible") != null)
                {
                    return Convert.ChangeType(id, entityType.Type.GetProperty(keyProperties[0].Name).PropertyType);
                }
                else
                {
                    return JsonSerializer.Deserialize(id, entityType.Type.GetProperty(keyProperties[0].Name).PropertyType);
                }
            }
            else
            {
                return JsonSerializer.Deserialize<Dictionary<string, object>>(id);
            }
        }

        internal static string ConverKeyToString(object entity, EntityType entityType)
        {
            List<EntityTypeProperty> keyProperties = entityType.Properties.Where<EntityTypeProperty>(item => item.IsKey).ToList();
            if (keyProperties.Count == 1)
            {
                if (keyProperties[0].Type.GetInterface("IConvertible") != null)
                {
                    return entityType.Type.GetProperty(keyProperties[0].Name).GetValue(entity, null).ToString();
                }
                else
                {
                    return JsonSerializer.Serialize(entityType.Type.GetProperty(keyProperties[0].Name).GetValue(entity, null));
                }
            }
            else
            {
                Dictionary<string, object> keys = new Dictionary<string, object>();
                foreach (var keyProp in keyProperties)
                {
                    keys.Add(keyProp.Name, entity.GetType().GetProperty(keyProp.Name).GetValue(entity, null));
                }
                return JsonSerializer.Serialize(keys);
            }
        }

        internal static string ConverKeyToString(Dictionary<string, object> viewModel, EntityType entityType)
        {
            List<EntityTypeProperty> keyProperties = entityType.Properties.Where<EntityTypeProperty>(item => item.IsKey).ToList();
            if (keyProperties.Count == 1)
            {
                return viewModel[keyProperties[0].Name].ToString();
            }
            else
            {
                Dictionary<string, object> keys = new Dictionary<string, object>();
                foreach (var keyProp in keyProperties)
                {
                    keys.Add(keyProp.Name, viewModel[keyProp.Name]);
                }
                return JsonSerializer.Serialize(keys);
            }
        }
    }
}