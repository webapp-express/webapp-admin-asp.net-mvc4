﻿using System;
using System.Web.Script.Serialization;

namespace WebAppAdmin.Areas.Admin.Helpers
{
    internal static class JsonSerializer
    {
        private static readonly JavaScriptSerializer serializer;

        static JsonSerializer()
        {
            serializer = new JavaScriptSerializer();
        }

        internal static string Serialize(object obj)
        {
            return serializer.Serialize(obj);
        }

        internal static object Deserialize(string input, Type targetType)
        {
            return serializer.Deserialize(input, targetType);
        }

        internal static T Deserialize<T>(string input)
        {
            return serializer.Deserialize<T>(input);
        }

        internal static object DeserializeObject(string input)
        {
            return serializer.DeserializeObject(input);
        }
    }
}