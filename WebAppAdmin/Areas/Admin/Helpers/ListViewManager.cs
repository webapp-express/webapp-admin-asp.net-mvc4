﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using WebAppAdmin.DataProvider.Core;
using WebAppAdmin.Model.Core;
using WebAppAdmin.Areas.Admin.Models;

namespace WebAppAdmin.Areas.Admin.Helpers
{
    public class ListViewManager
    {
        private IEntityManagerFactory entityManagerFactory;
        private string entityManagerName;
        private string entityName;
        private ListView listView;

        public ListViewManager(IEntityManagerFactory entityManagerFactory, string entityManagerName, string entityName)
        {
            if (entityManagerFactory == null)
            {
                throw new ArgumentNullException("entityManagerFactory");
            }
            if (String.IsNullOrEmpty(entityManagerName))
            {
                throw new ArgumentNullException("entityManagerName");
            }
            if (String.IsNullOrEmpty(entityName))
            {
                throw new ArgumentNullException("entityName");
            }
            this.entityManagerFactory = entityManagerFactory;
            this.entityManagerName = entityManagerName;
            this.entityName = entityName;
        }

        public ListView ListView
        {
            get
            {
                if (listView == null)
                {
                    string listViewCacheKey = GetFileName(this.entityManagerName, this.entityName);
                    object listViewCache = HttpContext.Current.Session[listViewCacheKey];
                    if (listViewCache == null)
                    {
                        string path = GetPath(this.entityManagerName, this.entityName);
                        if (File.Exists(path))
                        {
                            DataContractSerializer serializer = new DataContractSerializer(typeof(ListView), UIElementManager.GetUIElementTypeList(), Int32.MaxValue, false, true, null);
                            using (FileStream fs = File.Open(path, FileMode.Open))
                            {
                                listView = (ListView)serializer.ReadObject(fs);
                            }
                        }
                        else
                        {
                            listView = new ListView();
                            listView.EntityManagerName = this.entityManagerName;
                            listView.EntityName = this.entityName;
                            listView.ControllerName = "Entity";
                            listView.ActionName = "List";
                            using (var entityManager = entityManagerFactory.GetInstance(entityManagerName))
                            {
                                listView.EntityType = entityManager.EntityTypeManager.GetEntityTypes().Where(item => item.Name == listView.EntityName).FirstOrDefault();

                                listView.Properties = new List<ListViewProperty>();
                                listView.FilterProperties = new List<ListViewProperty>();
                                listView.Filter = new List<FilterItem>();

                                List<EntityTypeProperty> dependentProperties = DependentPropertiesHelper.GetDependentProperties(listView.EntityType);
                                FilterItem filterItem = null;
                                Type propertyType = null;
                                foreach (var entityTypeProperty in listView.EntityType.Properties)
                                {
                                    propertyType = entityTypeProperty.Type;
                                    propertyType = TypeHelper.ExtractType(propertyType);

                                    if ((propertyType.GetInterface("IEnumerable") == null
                                        || propertyType == typeof(string)
                                        || propertyType.IsEnum)
                                        && !dependentProperties.Contains(entityTypeProperty))
                                    {
                                        listView.Properties.Add(new ListViewProperty() { Name = entityTypeProperty.Name, IsActive = true });
                                        if (listView.SortDefault == null)
                                        {
                                            listView.SortDefault = new SortItem() { PropertyName = entityTypeProperty.Name, SortDirection = SortDirection.Ascending };
                                        }

                                        if (propertyType.GetInterface("IConvertible") != null)
                                        {
                                            listView.FilterProperties.Add(new ListViewProperty() { Name = entityTypeProperty.Name, IsActive = true });
                                            filterItem = new FilterItem() { PropertyName = entityTypeProperty.Name };
                                            this.SetFilterItemValue(filterItem, propertyType);
                                            listView.Filter.Add(filterItem);
                                        }
                                    }
                                }
                            }
                            listView.Name = "_List";
                            listView.PageSize = 20;
                        }
                        if (listView.EntityType == null)
                        {
                            using (var entityManager = entityManagerFactory.GetInstance(entityManagerName))
                            {
                                listView.EntityType = entityManager.EntityTypeManager.GetEntityTypes().Where(item => item.Name == listView.EntityName).FirstOrDefault();
                            }
                            int filterItemCount = listView.Filter.Count;
                            FilterItem filterItem = null;
                            EntityTypeProperty entityTypeProperty = null;
                            for (int i = 0; i < filterItemCount; i++)
                            {
                                filterItem = listView.Filter[i];
                                entityTypeProperty = listView.EntityType.Properties.Where(item => item.Name == filterItem.PropertyName).FirstOrDefault();
                                if (entityTypeProperty != null)
                                {
                                    this.SetFilterItemValue(filterItem, entityTypeProperty.Type);
                                }
                            }
                        }
                        List<string> viewSessionKeys = new List<string>();
                        string key = "";
                        foreach (var item in HttpContext.Current.Session.Keys)
                        {
                            key = item.ToString();
                            if (key.IndexOf(".ListView.config") > 0
                                && key != listViewCacheKey)
                            {
                                viewSessionKeys.Add(key);
                            }
                        }
                        foreach (var item in viewSessionKeys)
                        {
                            HttpContext.Current.Session.Remove(item);
                        }
                        HttpContext.Current.Session[listViewCacheKey] = listView;
                    }
                    else
                    {
                        listView = (ListView)listViewCache;
                    }
                }
                return listView;
            }
        }

        private void SetFilterItemValue(FilterItem filterItem, Type propertyType)
        {
            if (propertyType == typeof(DateTime))
            {
                filterItem.Value = DateTime.Now;
            }
            else
            {
                filterItem.Value = null;
            }
        }

        public void Save(ListView listView)
        {
            string path = GetPath(ListView.EntityManagerName, ListView.EntityName);
            DataContractSerializer serializer = new DataContractSerializer(typeof(ListView), UIElementManager.GetUIElementTypeList(), Int32.MaxValue, false, true, null);
            using (FileStream fs = File.Open(path, FileMode.Create))
            {
                serializer.WriteObject(fs, listView);
            }
            ClearCache(ListView.EntityManagerName, ListView.EntityName);
        }

        public static void Delete(string entityManagerName, string entityName)
        {
            if (String.IsNullOrEmpty(entityManagerName))
            {
                throw new ArgumentNullException("entityManagerName");
            }
            if (String.IsNullOrEmpty(entityName))
            {
                throw new ArgumentNullException("entityName");
            }
            string path = GetPath(entityManagerName, entityName);
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            ClearCache(entityManagerName, entityName);
        }

        public ListView GetEntityListView(IEntityManager entityManager, string mode, bool showFilter, string filter, string sort, int pageNumber, int pageSize, string sender)
        {
            ListView.Mode = mode;
            ListView.ShowFilter = showFilter;
            ListView.PageNumber = pageNumber;
            ListView.PageSize = pageSize;
            ListView.Sender = sender;
            ListView.ModelErrors = new ModelErrors();

            try
            {
                ListView.Sort = new List<SortItem>() { ListView.SortDefault };
                if (!String.IsNullOrEmpty(sort))
                {
                    ListView.Sort = JsonSerializer.Deserialize<List<SortItem>>(HttpUtility.UrlDecode(sort));
                }

                if (!String.IsNullOrEmpty(filter))
                {
                    List<FilterItem> filterChanged = JsonSerializer.Deserialize<List<FilterItem>>(HttpUtility.UrlDecode(filter));
                    FilterItem filterItemChanged = null;
                    FilterItem filterItem = null;
                    Type propertyType = null;

                    for (int i = 0; i < filterChanged.Count; i++)
                    {
                        filterItemChanged = filterChanged[i];
                        if (filterItemChanged.Operator != Operator.NoSet)
                        {
                            filterItem = ListView.Filter.Where<FilterItem>(item => item.PropertyName == filterItemChanged.PropertyName).FirstOrDefault();
                            if (filterItemChanged.Value != null
                                && !String.IsNullOrEmpty(filterItemChanged.Value.ToString()))
                            {
                                filterItem.Value = filterItemChanged.Value;
                                try
                                {
                                    propertyType = ListView.EntityType.Properties.Where<EntityTypeProperty>(item => item.Name == filterItemChanged.PropertyName).FirstOrDefault().Type;
                                    propertyType = TypeHelper.ExtractType(propertyType);
                                    filterItemChanged.Value = Convert.ChangeType(filterItemChanged.Value, propertyType);
                                    filterItem.Value = filterItemChanged.Value;
                                    filterItem.Operator = filterItemChanged.Operator;
                                }
                                catch (Exception ex)
                                {
                                    ListView.ModelErrors.PropertyErrors.Add(filterItem.PropertyName, new List<string>() { ex.Message });
                                    if (!ListView.ModelErrors.Errors.Contains(ex.Message))
                                    {
                                        ListView.ModelErrors.Errors.Add(ex.Message);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (var filterItem in ListView.Filter)
                    {
                        filterItem.Operator = Operator.NoSet;
                    }
                }

                MethodInfo method = entityManager.GetType().GetMethod("GetList");
                MethodInfo genericMethod = method.MakeGenericMethod(ListView.EntityType.Type);
                ListView.List = new List<ListItem>();
                ListItem listItem = null;
                object propVal = null;
                foreach (var entityItem in (IList)genericMethod.Invoke(entityManager, new object[] { ListView.Filter, ListView.Sort, (pageNumber - 1) * pageSize, pageSize }))
                {
                    listItem = new ListItem();
                    listItem.Key = EntityKeysConverter.ConverKeyToString(entityItem, ListView.EntityType);
                    listItem.Properties = new Dictionary<string, object>();
                    foreach (var propItem in ListView.Properties)
                    {
                        if (propItem.IsActive)
                        {
                            propVal = entityItem.GetType().GetProperty(propItem.Name).GetValue(entityItem, null);
                            if (propVal != null)
                            {
                                propVal.ToString();
                            }
                            listItem.Properties.Add(propItem.Name, propVal);
                        }
                    }
                    ListView.List.Add(listItem);
                }

                method = entityManager.GetType().GetMethod("Count");
                genericMethod = method.MakeGenericMethod(ListView.EntityType.Type);
                ListView.Count = (int)genericMethod.Invoke(entityManager, new object[] { ListView.Filter });
            }
            catch (Exception ex)
            {
                ListView.ModelErrors.Errors.Add(ex.Message);
            }
            return ListView;
        }

        public static List<Operator> GetFilterItemOperatorList(ListView listView, FilterItem filterItem)
        {
            List<Operator> operatorList = new List<Operator>() { Operator.NoSet, Operator.Equal, Operator.NotEqual };
            EntityTypeProperty entityTypeProperty = listView.EntityType.Properties.Where<EntityTypeProperty>(item => item.Name == filterItem.PropertyName).FirstOrDefault();
            if (entityTypeProperty.Type == typeof(string))
            {
                operatorList.Add(Operator.Like);
            }
            else if (entityTypeProperty.Type == typeof(int)
                || entityTypeProperty.Type == typeof(decimal)
                || entityTypeProperty.Type == typeof(double)
                || entityTypeProperty.Type == typeof(DateTime))
            {
                operatorList.Add(Operator.GreaterThan);
                operatorList.Add(Operator.GreaterThanOrEqual);
                operatorList.Add(Operator.LessThan);
                operatorList.Add(Operator.LessThanOrEqual);
            }
            return operatorList;
        }

        public static RouteValueDictionary GetPageRouteValues(ListView listView, int pageNumber)
        {
            RouteValueDictionary routeValues = new RouteValueDictionary();
            routeValues.Add("entityManagerName", listView.EntityManagerName);
            routeValues.Add("entityName", listView.EntityName);
            routeValues.Add("mode", listView.Mode);
            routeValues.Add("showFilter", listView.ShowFilter.ToString().ToLower());
            List<FilterItem> filterChanged = new List<FilterItem>(listView.Filter.Where<FilterItem>(item => item.Operator != Operator.NoSet).ToList());
            if (filterChanged.Count > 0)
            {
                routeValues.Add("filter", HttpUtility.UrlDecode(JsonSerializer.Serialize(filterChanged)));
            }
            routeValues.Add("sort", HttpUtility.UrlDecode(JsonSerializer.Serialize(listView.Sort)));
            routeValues.Add("pageNumber", pageNumber);
            routeValues.Add("pageSize", listView.PageSize);
            if (!String.IsNullOrEmpty(listView.Sender))
            {
                routeValues.Add("sender", listView.Sender);
            }

            return routeValues;
        }

        public static string GetFileName(string entityManagerName, string entityName)
        {
            return entityManagerName + "." + entityName + ".ListView.config";
        }

        private static void ClearCache(string entityManagerName, string entityName)
        {
            HttpContext.Current.Session.Remove(GetFileName(entityManagerName, entityName));
        }

        private static string GetPath(string entityManagerName, string entityName)
        {
            return HttpContext.Current.Server.MapPath("~/Areas/Admin/EntityViews/") + GetFileName(entityManagerName, entityName);
        }
    }
}