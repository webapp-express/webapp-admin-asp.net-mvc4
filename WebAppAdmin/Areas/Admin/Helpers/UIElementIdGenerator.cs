﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppAdmin.Areas.Admin.Helpers
{
    internal class UIElementIdGenerator
    {
        private int maxId = 0;

        internal UIElementIdGenerator(int maxId)
        {
            this.maxId = maxId;
        }

        internal int GetId()
        {
            maxId++;
            return maxId;
        }

        internal static string Prefix
        {
            get
            {
                return "el";
            }
        }
    }
}