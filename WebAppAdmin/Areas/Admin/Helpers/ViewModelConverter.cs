﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using WebAppAdmin.DataProvider.Core;
using WebAppAdmin.Areas.Admin.Models;
using WebAppAdmin.UI.Core;
using WebAppAdmin.UI.View.Core;
using WebAppAdmin.UI.Elements;

namespace WebAppAdmin.Areas.Admin.Helpers
{
    internal class ViewModelConverter
    {
        private IValueConverter valueConverter;
        private ModelErrors modelErrors;

        internal ViewModelConverter(IValueConverter valueConverter)
        {
            this.valueConverter = valueConverter;
            modelErrors = new ModelErrors();
        }

        internal Dictionary<string, object> ConvertToViewModel(object entity, IEntityTypeManager entityTypeManager, List<Control> controls)
        {
            Dictionary<string, object> viewModel = new Dictionary<string, object>();
            PropertyInfo propertyInfo = null;
            Type controlValuePropertyType = null;
            object propVal = null;
            foreach (var controlItem in controls)
            {
                propertyInfo = entity.GetType().GetProperty(controlItem.Name);
                propVal = propertyInfo.GetValue(entity, null);
                controlValuePropertyType = controlItem.GetType().GetProperty(controlItem.ValuePropertyName).PropertyType;
                if (controlValuePropertyType.GetInterface("IEnumerable") != null
                    && controlValuePropertyType.IsGenericType)
                {
                    if (propVal != null)
                    {
                        Type genericArgumentType = controlValuePropertyType.GetGenericArguments()[0];
                        var listType = typeof(List<>);
                        IList list = (IList)Activator.CreateInstance(listType.MakeGenericType(genericArgumentType));
                        Type genericArgumentTypeProperty = propertyInfo.PropertyType.GetGenericArguments()[0];
                        EntityType entityTypeItem = entityTypeManager.GetEntityTypes().Where(item => item.Type == genericArgumentTypeProperty).FirstOrDefault();
                        if (controlItem is ItemsControl)
                        {
                            foreach (var listItem in (IEnumerable)propVal)
                            {
                                list.Add(ConvertToViewModel(listItem, entityTypeManager, ((ItemsControl)controlItem).ItemControls));
                            }
                        }
                        else
                        {
                            foreach (var propValItem in (IEnumerable)propVal)
                            {
                                list.Add(valueConverter.ConvertToViewModelPropertyType(propValItem, genericArgumentType, entityTypeItem.Type, entityTypeManager));
                            }
                        }
                        propVal = list;
                    }
                }
                else
                {
                    propVal = valueConverter.ConvertToViewModelPropertyType(propVal, controlValuePropertyType, propertyInfo.PropertyType, entityTypeManager);
                }
                viewModel.Add(controlItem.Name, propVal);
            }
            return viewModel;
        }

        internal object ConvertToEntity(Dictionary<string, object> viewModel, string id, EntityType entityType, IEntityManager entityManager, List<Control> controls)
        {
            object entity = null;
            MethodInfo method = entityManager.GetType().GetMethod("Get");
            MethodInfo generic = method.MakeGenericMethod(entityType.Type);
            if (!String.IsNullOrEmpty(id))
            {
                entity = generic.Invoke(entityManager, new object[] { EntityKeysConverter.ConvertToKeyType(id, entityType) });
            }
            if (entity == null)
            {
                entity = Activator.CreateInstance(entityType.Type);
            }

            PropertyInfo propertyInfo = null;
            object propVal = null;
            EntityTypeProperty entityTypeProperty = null;
            foreach (var control in controls)
            {
                entityTypeProperty = entityType.Properties.Where(item => item.Name == control.Name).FirstOrDefault();
                if (entityTypeProperty != null)
                {
                    propertyInfo = entity.GetType().GetProperty(entityTypeProperty.Name);
                    if (entityTypeProperty.Type.GetInterface("IEnumerable") != null
                        && entityTypeProperty.Type.IsGenericType)
                    {
                        Type[] genericTypes = entityTypeProperty.Type.GetGenericArguments();
                        Type genericArgumentType = genericTypes[0];
                        var listType = typeof(List<>);
                        IList list = (IList)Activator.CreateInstance(listType.MakeGenericType(genericArgumentType));
                        EntityType entityTypeItem = entityManager.EntityTypeManager.GetEntityTypes().Where(item => item.Type == genericArgumentType).FirstOrDefault();
                        List<string> listItemKeyList = new List<string>();
                        if (viewModel.ContainsKey(entityTypeProperty.Name))
                        {
                            propVal = viewModel[entityTypeProperty.Name];
                            EntityTypeProperty entityRefProperty = null;
                            if (entityTypeItem != null)
                            {
                                entityRefProperty = entityType.Properties.Where(item => item.Type == entityTypeItem.Type).FirstOrDefault();
                            }
                            object listItem = null;
                            foreach (var item in (IEnumerable)propVal)
                            {
                                if (entityTypeItem != null)
                                {
                                    listItem = this.ConvertToEntity((Dictionary<string, object>)item, EntityKeysConverter.ConverKeyToString((Dictionary<string, object>)item, entityTypeItem), entityTypeItem, entityManager, ((ItemsControl)control).ItemControls);
                                    if (entityRefProperty != null)
                                    {
                                        listItem.GetType().GetProperty(entityRefProperty.Name).SetValue(listItem, entity, null);
                                    }
                                }
                                else
                                {
                                    listItem = Activator.CreateInstance(genericArgumentType);
                                    PropertyInfo propertyInfoListItem = null;
                                    foreach (var viewModelItem in (Dictionary<string, object>)item)
                                    {
                                        propertyInfoListItem = listItem.GetType().GetProperty(viewModelItem.Key);
                                        propVal = viewModelItem.Value;
                                        propVal = valueConverter.ConvertToEntityPropertyType(propVal, propertyInfoListItem.PropertyType, entityManager.EntityTypeManager);
                                        propertyInfoListItem.SetValue(listItem, propVal, null);
                                    }
                                }
                                list.Add(listItem);
                                if (entityTypeItem != null)
                                {
                                    listItemKeyList.Add(EntityKeysConverter.ConverKeyToString(listItem, entityTypeItem));
                                }
                            }
                        }
                        if (entityTypeItem != null)
                        {
                            object listEntity = propertyInfo.GetValue(entity, null);
                            if (listEntity != null)
                            {
                                List<object> deleteEntityList = new List<object>();
                                int listCount = list.Count;
                                foreach (var item in (IEnumerable)listEntity)
                                {
                                    if (listCount == 0
                                        || !listItemKeyList.Contains(EntityKeysConverter.ConverKeyToString(item, entityTypeItem)))
                                    {
                                        deleteEntityList.Add(item);
                                    }
                                }
                                for (int i = 0; i < deleteEntityList.Count; i++)
                                {
                                    entityManager.Delete(deleteEntityList[i]);
                                }
                            }
                        }
                        propVal = list;
                    }
                    else
                    {
                        propVal = viewModel[entityTypeProperty.Name];
                        Type propertyType = entityTypeProperty.Type;
                        propertyType = TypeHelper.ExtractType(propertyType);
                        propVal = valueConverter.ConvertToEntityPropertyType(propVal, propertyType, entityManager.EntityTypeManager);
                        if (entityManager is IEntityState
                            && propVal != null
                            && entityManager.EntityTypeManager.GetEntityTypes().Where(item => item.Type == entityTypeProperty.Type).FirstOrDefault() != null)
                        {
                            ((IEntityState)entityManager).Attach(propVal);
                        }
                    }
                    propertyInfo.SetValue(entity, propVal, null);
                }
            }
            return entity;
        }

        internal ModelErrors ModelErrors
        {
            get
            {
                return modelErrors;
            }
        }
    }
}