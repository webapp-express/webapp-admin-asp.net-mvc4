﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Web;
using System.Threading;
using WebAppAdmin.DataProvider.Core;
using WebAppAdmin.UI.Core;
using WebAppAdmin.Model.Core;
using WebAppAdmin.Areas.Admin.Models;
using WebAppAdmin.Areas.Admin.Resources;
using WebAppAdmin.Areas.Admin.Resources.EntityResource;

namespace WebAppAdmin.Areas.Admin.Helpers
{
    public class EntityResourceManager : ResourceManager
    {
        private string entityManagerName;
        private string entityName;
        List<EntityResource> resource;

        public EntityResourceManager(string entityManagerName, string entityName)
        {
            if (String.IsNullOrEmpty(entityManagerName))
            {
                throw new ArgumentNullException("entityManagerName");
            }
            this.entityManagerName = entityManagerName;
            this.entityName = entityName;
        }

        public EntityResourceManager(string entityManagerName)
            : this(entityManagerName, null)
        {
        }

        public override string GetString(string name)
        {
            return this.GetString(name, Thread.CurrentThread.CurrentCulture.Name);
        }

        public override string GetString(string name, string culture)
        {
            if (!String.IsNullOrEmpty(name))
            {
                if (resource == null)
                {
                    string resourceName = entityManagerName + (!String.IsNullOrEmpty(entityName) ? "." + entityName : "");
                    resource = GetResource(resourceName + (!String.IsNullOrEmpty(culture) ? "." + culture : ""));
                    if (resource == null
                        && !String.IsNullOrEmpty(culture))
                    {
                        resource = GetResource(resourceName);
                    }
                }
                if (resource != null)
                {
                    EntityResource entityResource = resource.Where<EntityResource>(item => item.Name == name).FirstOrDefault();
                    if (entityResource != null)
                    {
                        return entityResource.Value;
                    }
                }
            }
            return name;
        }

        public static List<ResourceFile> GetResourceFileList()
        {
            List<ResourceFile> resourceFileList = new List<ResourceFile>();
            string path = GetPath();
            List<Culture> cultureList = CultureManager.GetList();
            FileInfo fileInfo = null;
            string name = "";
            string entityName = "";
            string entityNameItem = "";
            string cultureName = "";
            string[] split = null;

            foreach (var fileName in Directory.GetFiles(path, "*.config"))
            {
                fileInfo = new FileInfo(fileName);
                name = fileInfo.Name.Replace(fileInfo.Extension, "");
                split = name.Split(new Char[] { '.' });
                cultureName = _ResourceList.Default.ToLower();
                entityName = "";
                for (int i = 1; i < split.Length; i++)
                {
                    entityNameItem = split[i];
                    if (cultureList.Where<Culture>(item => item.Name == entityNameItem).FirstOrDefault() == null)
                    {
                        entityName = entityName + (entityName != "" ? "." : "") + entityNameItem;
                    }
                    else
                    {
                        cultureName = entityNameItem;
                    }
                }
                resourceFileList.Add(new ResourceFile() { Name = name, EntityManagerName = split[0], EntityName = entityName, CultureName = cultureName });
            }
            return resourceFileList.OrderBy(item => item.EntityManagerName).OrderBy(item => item.CultureName).ToList();
        }

        public static List<EntityResource> GetResource(string name, bool throwException = false)
        {
            if (String.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            string path = GetPath();
            string fileName = path + name + ".config";
            if (!File.Exists(fileName))
            {
                if (throwException)
                {
                    throw new ApplicationException(name + " " + Messages.not_exists);
                }
                else
                {
                    return null;
                }
            }
            List<EntityResource> resourceList = new List<EntityResource>();
            using (FileStream fs = File.Open(fileName, FileMode.Open))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<EntityResource>));
                resourceList = (List<EntityResource>)serializer.Deserialize(fs);
            }
            return resourceList;
        }

        public static void CreateResources(List<string> culrureList, IEntityManagerFactory entityManagerFactory, IEntityItemManager entityItemManager)
        {
            if (culrureList == null)
            {
                throw new ArgumentNullException("culrureList");
            }

            string path = GetPath();
            string fileName = "";
            List<EntityResource> resourceList = null;
            EntityType entityType = null;
            IDictionary<string, IEnumerable<EntityItem>> entityItems = entityItemManager.GetEntityItems();
            foreach (var cultureName in culrureList)
            {
                foreach (var entityManagerItem in entityItems)
                {
                    List<EntityResource> entityGroupResourceList = new List<EntityResource>();
                    entityGroupResourceList.Add(new EntityResource() { Name = entityManagerItem.Key, Value = entityManagerItem.Key });
                    foreach (var entityTypeItem in entityManagerItem.Value)
                    {
                        entityGroupResourceList.Add(new EntityResource() { Name = entityTypeItem.EntityName, Value = entityTypeItem.EntityName });
                        fileName = path + entityTypeItem.EntityManagerName + "." + entityTypeItem.EntityName + (cultureName != "" ? "." + cultureName : "") + ".config";
                        if (!File.Exists(fileName))
                        {
                            resourceList = new List<EntityResource>();
                            resourceList.Add(new EntityResource() { Name = entityTypeItem.EntityName, Value = entityTypeItem.EntityName });
                            using (var entityManager = entityManagerFactory.GetInstance(entityTypeItem.EntityManagerName))
                            {
                                entityType = entityManager.EntityTypeManager.GetEntityTypes().Where(item => item.Name == entityTypeItem.EntityName).FirstOrDefault();
                                if (entityType != null)
                                {
                                    foreach (var propertyItem in entityType.Properties)
                                    {
                                        resourceList.Add(new EntityResource() { Name = propertyItem.Name, Value = propertyItem.Name });
                                        if (propertyItem.Type.GetInterface("IEnumerable") != null && propertyItem.Type.IsGenericType)
                                        {
                                            Type[] genericTypes = propertyItem.Type.GetGenericArguments();
                                            if (genericTypes.Length == 1)
                                            {
                                                EntityType entityTypeGenericArg = entityManager.EntityTypeManager.GetEntityTypes().Where<EntityType>(item => item.Type == genericTypes[0]).FirstOrDefault();
                                                if (entityTypeGenericArg != null)
                                                {
                                                    foreach (var propertyItemTypeGenericArg in entityTypeGenericArg.Properties)
                                                    {
                                                        resourceList.Add(new EntityResource() { Name = propertyItemTypeGenericArg.Name, Value = propertyItemTypeGenericArg.Name });
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            using (FileStream fs = File.Open(fileName, FileMode.Create))
                            {
                                XmlSerializer serializer = new XmlSerializer(typeof(List<EntityResource>));
                                serializer.Serialize(fs, resourceList);
                            }
                        }
                    }
                    fileName = path + entityManagerItem.Key + (cultureName != "" ? "." + cultureName : "") + ".config";
                    if (!File.Exists(fileName))
                    {
                        using (FileStream fs = File.Open(fileName, FileMode.Create))
                        {
                            XmlSerializer serializer = new XmlSerializer(typeof(List<EntityResource>));
                            serializer.Serialize(fs, entityGroupResourceList);
                        }
                    }
                }
            }
        }

        public static void Update(List<EntityResource> resourceList, string name)
        {
            if (resourceList == null)
            {
                throw new ArgumentNullException("resourceList");
            }

            if (String.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            string path = GetPath();
            string fileName = path + name + ".config";
            if (!File.Exists(fileName))
            {
                throw new ApplicationException(name + " " + Messages.not_exists);
            }

            using (FileStream fs = File.Open(fileName, FileMode.Create))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<EntityResource>));
                serializer.Serialize(fs, resourceList);
            }
        }

        public static void Delete(List<string> idList)
        {
            if (idList == null)
            {
                throw new ArgumentNullException("idList");
            }

            string path = GetPath();
            foreach (var name in idList)
            {
                string fileName = path + name + ".config";
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }
            }
        }

        public static ModelErrors ValidateDeleteCulture(List<string> cultureList)
        {
            if (cultureList == null)
            {
                throw new ArgumentNullException("idList");
            }

            string path = GetPath();
            ModelErrors modelErrors = new ModelErrors();
            foreach (var name in cultureList)
            {
                if (Directory.GetFiles(path, "*." + name + ".config").Length > 0)
                {
                    modelErrors.Errors.Add(_ResourceList.Exist_the_resources_with_culture + " " + name + ".");
                }
            }
            return modelErrors;
        }

        private static string GetPath()
        {
            return HttpContext.Current.Server.MapPath("~/Areas/Admin/EntityResources/");
        }
    }
}