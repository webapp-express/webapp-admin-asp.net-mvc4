﻿using System;
using System.Collections.Generic;
using WebAppAdmin.DataProvider.Core;

namespace WebAppAdmin.Areas.Admin.Helpers
{
    internal class DependentPropertiesHelper
    {
        internal static List<EntityTypeProperty> GetDependentProperties(EntityType entityType)
        {
            List<EntityTypeProperty> dependentProperties = new List<EntityTypeProperty>();
            foreach (var entityTypeProperty in entityType.Properties)
            {
                if (entityTypeProperty is IDependentProperties
                    && ((IDependentProperties)entityTypeProperty).DependentProperties != null)
                {
                    foreach (var dependentProperty in ((IDependentProperties)entityTypeProperty).DependentProperties)
                    {
                        dependentProperties.Add(dependentProperty);
                    }
                }
            }
            return dependentProperties;
        }
    }
}