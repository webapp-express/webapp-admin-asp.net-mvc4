﻿using System;
using System.Collections.Generic;
using WebAppAdmin.DataProvider.Core;
using WebAppAdmin.Model.Core;

namespace WebAppAdmin.Areas.Admin.Helpers
{
    public class EntityManagerFactory : Dictionary<string, Func<IEntityManager>>, IEntityManagerFactory
    {
        public IEntityManager GetInstance(string name)
        {
            return this[name]();
        }

        public IEnumerable<string> GetNames()
        {
            return this.Keys;
        }
    }
}
