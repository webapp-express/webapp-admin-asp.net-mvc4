﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Dynamic;
using WebAppAdmin.DataProvider.Core;
using WebAppAdmin.Model.Core;
using WebAppAdmin.Areas.Admin.Models;
using WebAppAdmin.UI.Core;
using WebAppAdmin.UI.Elements;
using WebAppAdmin.UI.View.Core;

namespace WebAppAdmin.Areas.Admin.Helpers
{
    public class EditViewManager
    {
        private string entityManagerName;
        private string entityName;
        private IEntityManagerFactory entityManagerFactory;
        private EditView editView;
        private IViewGenerator viewGenerator;
        private IControlConvention controlConvention;
        private ResourceManager resourceManager;
        private UIElementMode mode;

        public EditViewManager(string entityManagerName, string entityName, IEntityManagerFactory entityManagerFactory, IViewGenerator viewGenerator, IControlConvention controlConvention, ResourceManager resourceManager, UIElementMode mode)
        {
            if (String.IsNullOrEmpty(entityManagerName))
            {
                throw new ArgumentNullException("entityManagerName");
            }
            if (String.IsNullOrEmpty(entityName))
            {
                throw new ArgumentNullException("entityName");
            }
            if (entityManagerFactory == null)
            {
                throw new ArgumentNullException("entityManagerFactory");
            }
            if (viewGenerator == null)
            {
                throw new ArgumentNullException("viewGenerator");
            }
            if (controlConvention == null)
            {
                throw new ArgumentNullException("controlConvention");
            }
            this.entityManagerName = entityManagerName;
            this.entityName = entityName;
            this.entityManagerFactory = entityManagerFactory;
            this.viewGenerator = viewGenerator;
            this.controlConvention = controlConvention;
            this.resourceManager = resourceManager;
            this.mode = mode;
        }

        public EditView EditView
        {
            get
            {
                if (editView == null)
                {
                    string editViewCacheKey = this.GetCacheKey();
                    object editViewCache = HttpContext.Current.Session[editViewCacheKey];
                    if (editViewCache == null)
                    {
                        string path = GetPath(this.entityManagerName, this.entityName);
                        if (File.Exists(path))
                        {
                            DataContractSerializer serializer = new DataContractSerializer(typeof(EditView), UIElementManager.GetUIElementTypeList(), Int32.MaxValue, false, true, null);
                            using (FileStream fs = File.Open(path, FileMode.Open))
                            {
                                editView = (EditView)serializer.ReadObject(fs);
                            }
                        }
                        else
                        {
                            editView = new EditView();
                            editView.EntityManagerName = this.entityManagerName;
                            editView.EntityName = this.entityName;
                            editView.ControllerName = "Entity";
                            editView.ActionName = "Edit";
                            editView.Name = "_Edit";
                            using (var entityManager = entityManagerFactory.GetInstance(editView.EntityManagerName))
                            {
                                this.SetEntityType(entityManager);
                                editView.View = viewGenerator.CreateEntityView(editView.EntityType, entityManager.EntityTypeManager);
                            }
                            editView.View.ResourceManager = this.resourceManager;
                            editView.View.Mode = this.mode;
                        }
                        if (editView.EntityType == null)
                        {
                            using (var entityManager = entityManagerFactory.GetInstance(editView.EntityManagerName))
                            {
                                this.SetEntityType(entityManager);
                            }
                        }
                        List<string> viewSessionKeys = new List<string>();
                        string key = "";
                        foreach (var item in HttpContext.Current.Session.Keys)
                        {
                            key = item.ToString();
                            if (key.IndexOf(".EditView.config") > 0
                                && key != editViewCacheKey)
                            {
                                viewSessionKeys.Add(key);
                            }
                        }
                        foreach (var item in viewSessionKeys)
                        {
                            HttpContext.Current.Session.Remove(item);
                        }
                        HttpContext.Current.Session[editViewCacheKey] = editView;
                    }
                    else
                    {
                        editView = (EditView)editViewCache;
                    }
                }
                return editView;
            }
        }

        private void SetEntityType(IEntityManager entityManager)
        {
            editView.EntityType = entityManager.EntityTypeManager.GetEntityTypes().Where(item => item.Name == editView.EntityName).FirstOrDefault();
            editView.Properties = GetProperties(editView.EntityType, entityManager.EntityTypeManager, controlConvention);
        }

        public static List<EntityTypeProperty> GetProperties(EntityType entityType, IEntityTypeManager entityTypeManager, IControlConvention controlConvention)
        {
            List<EntityTypeProperty> properties = new List<EntityTypeProperty>();
            List<EntityTypeProperty> dependentProperties = DependentPropertiesHelper.GetDependentProperties(entityType);
            IList<string> controls = null;
            foreach (var entityTypeProperty in entityType.Properties)
            {
                if (!entityTypeProperty.IsAutoGenerated
                    && !dependentProperties.Contains(entityTypeProperty))
                {
                    controls = controlConvention.MatchControls(entityTypeProperty.Name, entityTypeProperty.Type, entityType, entityTypeManager);
                    if (controls != null
                        && controls.Count > 0)
                    {
                        properties.Add(entityTypeProperty);
                    }
                }
            }
            return properties;
        }

        public void Save(bool inCache = false)
        {
            if (inCache)
            {
                string editViewCacheKey = this.GetCacheKey();
                HttpContext.Current.Session[editViewCacheKey] = editView;
            }
            else
            {
                string path = GetPath(EditView.EntityManagerName, EditView.EntityName);
                DataContractSerializer serializer = new DataContractSerializer(typeof(EditView), UIElementManager.GetUIElementTypeList(), Int32.MaxValue, false, true, null);
                using (FileStream fs = File.Open(path, FileMode.Create))
                {
                    serializer.WriteObject(fs, EditView);
                }
                ClearCache(EditView.EntityManagerName, EditView.EntityName);
            }
        }

        public static void Delete(string entityManagerName, string entityName)
        {
            if (String.IsNullOrEmpty(entityManagerName))
            {
                throw new ArgumentNullException("entityManagerName");
            }
            if (String.IsNullOrEmpty(entityName))
            {
                throw new ArgumentNullException("entityName");
            }
            string path = GetPath(entityManagerName, entityName);
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            ClearCache(entityManagerName, entityName);
        }

        public static string GetFileName(string entityManagerName, string entityName)
        {
            return entityManagerName + "." + entityName + ".EditView.config";
        }

        private static void ClearCache(string entityManagerName, string entityName)
        {
            string fileName = GetFileName(entityManagerName, entityName);
            int index = HttpContext.Current.Session.Keys.Count - 1;
            for (int i = index; i >= 0; i--)
            {
                if (HttpContext.Current.Session.Keys[i].IndexOf(fileName) >= 0)
                {
                    HttpContext.Current.Session.RemoveAt(i);
                }
            }
        }

        private static string GetPath(string entityManagerName, string entityName)
        {
            return HttpContext.Current.Server.MapPath("~/Areas/Admin/EntityViews/") + GetFileName(entityManagerName, entityName);
        }

        private string GetCacheKey()
        {
            return GetFileName(this.entityManagerName, this.entityName) + ".Mode." + this.mode.ToString();
        }
    }
}