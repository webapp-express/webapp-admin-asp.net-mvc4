﻿using System;
using System.Collections.Generic;
using WebAppAdmin.DataProvider.Core;
using WebAppAdmin.Model.Core;

namespace WebAppAdmin.Areas.Admin.Helpers
{
    public class EntityItemGroupByEntityManagerName : IEntityItemManager
    {
        private IEntityManagerFactory entityManagerFactory;

        public EntityItemGroupByEntityManagerName(IEntityManagerFactory entityManagerFactory)
        {
            if (entityManagerFactory == null)
            {
                throw new ArgumentNullException("entityManagerFactory");
            }
            this.entityManagerFactory = entityManagerFactory;
        }

        public virtual IDictionary<string, IEnumerable<EntityItem>> GetEntityItems()
        {
            IDictionary<string, IEnumerable<EntityItem>> entityItems = new Dictionary<string, IEnumerable<EntityItem>>();
            List<EntityItem> entityItemList = null;
            EntityItem entityItem = null;
            foreach (var entityManagerName in entityManagerFactory.GetNames())
            {
                entityItemList = new List<EntityItem>();
                using (IEntityManager entityManager = entityManagerFactory.GetInstance(entityManagerName))
                {
                    foreach (EntityType entityType in entityManager.EntityTypeManager.GetEntityTypes())
                    {
                        if (!entityType.Type.IsAbstract)
                        {
                            entityItem = new EntityItem();
                            entityItem.EntityManagerName = entityManagerName;
                            entityItem.EntityName = entityType.Name;
                            entityItemList.Add(entityItem); 
                        }
                    }
                }
                entityItems.Add(entityManagerName, entityItemList);
            }
            return entityItems;
        }
    }
}
