﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using WebAppAdmin.Areas.Admin.Models;

namespace WebAppAdmin.Areas.Admin.Helpers
{
    internal class EntityViewsManager
    {
        internal static List<EntityViewFile> GetEntityViewFileList(string entityManagerName, string entityName)
        {
            List<EntityViewFile> entityViewFileList = new List<EntityViewFile>();
            entityViewFileList.Add(new EntityViewFile() { Type = "EditView", LastWriteDateTime = "" });
            entityViewFileList.Add(new EntityViewFile() { Type = "ListView", LastWriteDateTime = "" });
            string path = GetPath();
            List<Culture> cultureList = CultureManager.GetList();
            FileInfo fileInfo = null;
            string editViewFileName = EditViewManager.GetFileName(entityManagerName, entityName);
            string listViewFileName = ListViewManager.GetFileName(entityManagerName, entityName);

            foreach (var fileName in Directory.GetFiles(path, "*.config"))
            {
                fileInfo = new FileInfo(fileName);
                if (fileInfo.Name == editViewFileName)
                {
                    entityViewFileList.Where(item => item.Type == "EditView").First().LastWriteDateTime = fileInfo.LastWriteTime.ToString();
                }
                else if (fileInfo.Name == listViewFileName)
                {
                    entityViewFileList.Where(item => item.Type == "ListView").First().LastWriteDateTime = fileInfo.LastWriteTime.ToString();
                }
            }
            return entityViewFileList;
        }

        public static void Delete(List<string> idList, string entityManagerName, string entityName)
        {
            string path = GetPath();
            foreach (var viewType in idList)
            {
                if (viewType == "EditView")
                {
                    EditViewManager.Delete(entityManagerName, entityName);
                }
                else if (viewType == "ListView")
                {
                   ListViewManager.Delete(entityManagerName, entityName);
                }
            }
        }

        private static string GetPath()
        {
            return HttpContext.Current.Server.MapPath("~/Areas/Admin/EntityViews/");
        }
    }
}