﻿using System;
using System.Collections.Generic;

namespace WebAppAdmin.Areas.Admin.Helpers
{
    internal class TypeHelper
    {
        internal static Type ExtractType(Type type)
        {
            if (type.IsGenericType 
                && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                return type.GetGenericArguments()[0];
            }
            return type;
        }
    }
}
