﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebAppAdmin.DataProvider.Core;
using WebAppAdmin.UI.Elements;
using WebAppAdmin.UI.View.Core;

namespace WebAppAdmin.Areas.Admin.Helpers
{
    public class ValueConverter : IValueConverter
    {
        public virtual object ConvertToViewModelPropertyType(object value, Type viewModelPropertyType, Type entityPropertyType, IEntityTypeManager entityTypeManager)
        {
            if (viewModelPropertyType.GetInterface("IConvertible") != null)
            {
                if (entityPropertyType.IsEnum)
                {
                    return Convert.ChangeType(value, Enum.GetUnderlyingType(entityPropertyType));
                }
                else if (value != null)
                {
                    return Convert.ChangeType(value, viewModelPropertyType);
                }
            }
            else if (viewModelPropertyType == typeof(EntityViewModel))
            {
                return EntityViewModelConverter.ConvertToEntityItem(value, entityTypeManager.GetEntityTypes().Where(item => item.Type == entityPropertyType).FirstOrDefault());
            }

            return value;
        }

        public virtual object ConvertToEntityPropertyType(object value, Type entityPropertyType, IEntityTypeManager entityTypeManager)
        {
            if (value != null)
            {
                if (entityPropertyType.GetInterface("IConvertible") != null)
                {
                    if (value.ToString() == "")
                    {
                        return null;
                    }

                    if (entityPropertyType.IsEnum)
                    {
                        return Convert.ChangeType(value, Enum.GetUnderlyingType(entityPropertyType));
                    }
                    else
                    {
                        return Convert.ChangeType(value, entityPropertyType);
                    }
                }
                else
                {
                    EntityType propertyEntityType = entityTypeManager.GetEntityTypes().Where(item => item.Type == entityPropertyType).FirstOrDefault();
                    if (propertyEntityType != null)
                    {
                        return EntityViewModelConverter.ConvertToEntity(value.ToString(), propertyEntityType);
                    }
                }
            }

            return value;
        }
    }
}