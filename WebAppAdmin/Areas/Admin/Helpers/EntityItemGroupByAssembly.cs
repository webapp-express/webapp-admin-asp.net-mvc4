﻿using System;
using System.Collections.Generic;
using WebAppAdmin.DataProvider.Core;
using WebAppAdmin.Model.Core;

namespace WebAppAdmin.Areas.Admin.Helpers
{
    public class EntityItemGroupByAssembly : IEntityItemManager
    {
        private IEntityManagerFactory entityManagerFactory;

        public EntityItemGroupByAssembly(IEntityManagerFactory entityManagerFactory)
        {
            if (entityManagerFactory == null)
            {
                throw new ArgumentNullException("entityManagerFactory");
            }
            this.entityManagerFactory = entityManagerFactory;
        }

        public virtual IDictionary<string, IEnumerable<EntityItem>> GetEntityItems()
        {
            IDictionary<string, IEnumerable<EntityItem>> entityItems = new Dictionary<string, IEnumerable<EntityItem>>();
            EntityItem entityItem = null;
            string assemblyName = "";
            foreach (var entityManagerName in entityManagerFactory.GetNames())
            {
                using (IEntityManager entityManager = entityManagerFactory.GetInstance(entityManagerName))
                {
                    foreach (EntityType entityType in entityManager.EntityTypeManager.GetEntityTypes())
                    {
                        if (!entityType.Type.IsAbstract)
                        {
                            entityItem = new EntityItem();
                            entityItem.EntityManagerName = entityManagerName;
                            entityItem.EntityName = entityType.Name;

                            assemblyName = entityType.Type.Assembly.GetName().Name;
                            if (entityItems.ContainsKey(assemblyName))
                            {
                                ((List<EntityItem>)entityItems[assemblyName]).Add(entityItem);

                            }
                            else
                            {
                                entityItems.Add(assemblyName, new List<EntityItem>() { entityItem });
                            } 
                        }
                    }
                }
            }
            return entityItems;
        }
    }
}
