﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Web;
using WebAppAdmin.Areas.Admin.Models;
using WebAppAdmin.Areas.Admin.Resources;

namespace WebAppAdmin.Areas.Admin.Helpers
{
    internal class CultureManager
    {
        internal static List<Culture> GetList()
        {
            List<Culture> cultureList = new List<Culture>();
            string path = GetPath();
            if (File.Exists(path))
            {
                using (FileStream fs = File.Open(path, FileMode.Open))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<Culture>));
                    cultureList = (List<Culture>)serializer.Deserialize(fs);
                }
            }
            return cultureList;
        }

        internal static void Save(Culture culture, string id)
        {
            if (culture == null)
            {
                throw new ArgumentNullException("culture");
            }

            string path = GetPath();
            List<Culture> cultureList = GetList();

            if (String.IsNullOrEmpty(id))
            {
                if (cultureList.Where<Culture>(item => item.Name == culture.Name).FirstOrDefault() != null)
                {
                    throw new ApplicationException(culture.Name + " " + Messages.already_exists + ".");
                }
            }
            else
            {
                cultureList.Remove(cultureList.Where<Culture>(item => item.Name == id).FirstOrDefault());
            }
            cultureList.Add(culture);
            using (FileStream fs = File.Open(path, FileMode.Create))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<Culture>));
                serializer.Serialize(fs, cultureList);
            }
        }

        internal static void Delete(List<string> idList)
        {
            if (idList == null)
            {
                throw new ArgumentNullException("idList");
            }

            string path = GetPath();
            List<Culture> cultureList = GetList();
            if (cultureList.Count > 0)
            {
                foreach (var id in idList)
                {
                    cultureList.Remove(cultureList.Where<Culture>(item => item.Name == id).FirstOrDefault());
                }
                using (FileStream fs = File.Open(path, FileMode.Truncate))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<Culture>));
                    serializer.Serialize(fs, cultureList);
                }
            }
        }

        private static string GetPath()
        {
            return HttpContext.Current.Server.MapPath("~/Areas/Admin/EntityResources/Culture/Culture.config");
        }
    }
}