﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebAppAdmin.DataProvider.Core;
using WebAppAdmin.UI.Core;
using WebAppAdmin.UI.View.Core;
using WebAppAdmin.UI.Elements;

namespace WebAppAdmin.Areas.Admin.Helpers
{
    public class ControlConvention : IControlConvention
    {
        public IList<string> MatchControls(string propertyName, Type propertyType, EntityType entityType, IEntityTypeManager entityTypeManager)
        {
            propertyType = TypeHelper.ExtractType(propertyType);
            if (propertyType == typeof(string))
            {
                List<string> contolList = new List<string>() { "TextBox", "HtmlEditor", "TextArea", "ImageBrowser", "FileBrowser", "Password" };
                string propertyNameLower = propertyName.ToLower();
                if (propertyNameLower.IndexOf("content") >= 0)
                {
                    contolList.Remove("HtmlEditor");
                    contolList.Insert(0, "HtmlEditor");
                }
                else if (propertyNameLower.IndexOf("image") >= 0)
                {
                    contolList.Remove("ImageBrowser");
                    contolList.Insert(0, "ImageBrowser");
                }
                else if (propertyNameLower.IndexOf("file") >= 0)
                {
                    contolList.Remove("FileBrowser");
                    contolList.Insert(0, "FileBrowser");
                }
                else if (propertyNameLower.IndexOf("password") >= 0)
                {
                    contolList.Remove("Password");
                    contolList.Insert(0, "Password");
                }
                return contolList;
            }
            else if (propertyType == typeof(bool))
            {
                return new List<string>() { "CheckBox" };
            }
            else if (propertyType.IsEnum)
            {
                return new List<string>() { "DropDownList" };
            }
            else if (propertyType.GetInterface("IConvertible") != null)
            {
                return new List<string>() { "TextBox" };
            }
            else if (propertyType.GetInterface("IEnumerable") == null
                && entityTypeManager.GetEntityTypes().Where<EntityType>(item => item.Type == propertyType).FirstOrDefault() != null)
            {
                return new List<string>() { "EntityBox" }; ;
            }
            else if (propertyType.GetInterface("IEnumerable") != null
                && propertyType.IsGenericType)
            {
                Type[] genericTypes = propertyType.GetGenericArguments();
                if (genericTypes.Length == 1)
                {
                    EntityType entityTypeGenericArg = entityTypeManager.GetEntityTypes().Where<EntityType>(item => item.Type == genericTypes[0]).FirstOrDefault();
                    if (entityTypeGenericArg != null)
                    {
                        if (entityTypeGenericArg.Properties.Where(item => item.Type == entityType.Type).FirstOrDefault() != null)
                        {
                            return new List<string>() { "CollectionEditor" };
                        }
                    }
                    else
                    {
                        return new List<string>() { "CollectionEditor" };
                    }
                }
            }
            return null;
        }
    }
}