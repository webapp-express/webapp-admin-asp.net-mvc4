﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;
using System.IO;
using WebAppAdmin.Areas.Admin.Models;
using BrowserResource = WebAppAdmin.Areas.Admin.Resources.FileBrowser.Browser;

namespace WebAppAdmin.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator, Content manager")]
    public class FileBrowserController : Controller
    {
        private string defaultUploadDir = "/Content/upload/";
        private string imageSubDir = "images";
        private string fileSubDir = "files";
        private string[] imageAllowedExtensions = new string[] { "bmp", "gif", "jpeg", "jpg", "png" };
        private string[] fileAllowedExtensions = new string[] { "7z", "aiff", "asf", "avi", "bmp", "csv", "doc", "fla", "flv", "gif", "gz", "gzip", "jpeg", "jpg", "mid", "mov", "mp3", "mp4", "mpc", "mpeg", "mpg", "ods", "odt", "pdf", "png", "ppt", "pxd", "qt", "ram", "rar", "rm", "rmi", "rmvb", "rtf", "sdc", "sitd", "swf", "sxc", "sxw", "tar", "tgz", "tif", "tiff", "txt", "vsd", "wav", "wma", "wmv", "xls", "xml", "zip" };

        public ActionResult Browser(string type, string CKEditorFuncNum, string CKEditor, string langCode, string sender)
        {
            string uploadDir = GetUploadDir(type);
            string serverPath = Server.MapPath(uploadDir);
            List<FileBrowserItem> fileBrowserList = null;
            if (Directory.Exists(serverPath))
            {
                ViewBag.UploadDir = uploadDir;
                fileBrowserList = this.GetFileBrowserList(uploadDir);
            }
            else
            {
                fileBrowserList = new List<FileBrowserItem>();
                ViewBag.Error = String.Format(BrowserResource.The_upload_directory_was_not_found, uploadDir);
            }
            return View(fileBrowserList);
        }

        [HttpPost]
        public ActionResult BrowserItems(string directory)
        {
            return PartialView("_BrowserItems", this.GetFileBrowserList(directory));
        }

        [HttpPost]
        public string Upload(string type, HttpPostedFileBase upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            try
            {
                string url = this.ValidateAndUploadFile(type, "", upload);
                return "<script>window.parent.CKEDITOR.tools.callFunction(" + CKEditorFuncNum + ", '" + url + "');</script>";
            }
            catch (Exception ex)
            {
                return "<script>window.parent.CKEDITOR.tools.callFunction(" + CKEditorFuncNum + ", '','" + ex.Message + "');</script>";
            }
        }

        [HttpPost]
        public string UploadFile(string type, string directory, HttpPostedFileBase upload)
        {
            try
            {
                return this.ValidateAndUploadFile(type, directory, upload);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                return ex.Message;
            }
        }

        [HttpPost]
        public ActionResult CreateDirectory(string name, string directory)
        {
            try
            {
                if (String.IsNullOrEmpty(name))
                {
                    throw new ArgumentNullException(name);
                }

                string uploadDir = GetUploadDir("");
                if (directory.IndexOf(uploadDir) == 0)
                {
                    string serverPath = Server.MapPath(directory + name);
                    Directory.CreateDirectory(serverPath);
                }
                return BrowserItems(directory);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                ContentResult contentResult = new ContentResult();
                contentResult.Content = ex.Message;
                return contentResult;
            }
        }

        private string ValidateAndUploadFile(string type, string directory, HttpPostedFileBase upload)
        {
            string url = "";
            if (String.IsNullOrEmpty(type))
            {
                type = GetTypeFile(upload.FileName);
            }
            if (FileExtensionAllowed(upload.FileName, type))
            {
                string uploadDir = GetUploadDir(type);
                if (!String.IsNullOrEmpty(directory)
                    && directory.IndexOf(uploadDir) == 0)
                {
                    uploadDir = directory;
                }
                string serverPath = Server.MapPath(uploadDir);
                if (Directory.Exists(serverPath))
                {
                    string fileName = Path.GetFileName(upload.FileName);
                    serverPath += fileName;
                    if (!System.IO.File.Exists(serverPath))
                    {
                        upload.SaveAs(serverPath);
                        url = uploadDir + fileName;
                    }
                    else
                    {
                        throw new ApplicationException(String.Format(BrowserResource.The_file_already_exists, fileName));
                    }
                }
                else
                {
                    throw new ApplicationException(String.Format(BrowserResource.The_upload_directory_was_not_found, uploadDir));
                }
            }
            else
            {
                throw new ApplicationException(String.Format(BrowserResource.The_file_extension_does_not_allow, Path.GetExtension(upload.FileName)));
            }
            return url;
        }

        private string GetTypeFile(string fileName)
        {
            if (imageAllowedExtensions.Contains(this.GetExtension(fileName)))
            {
                return "image";
            }
            return "file";
        }

        private List<FileBrowserItem> GetFileBrowserList(string directory)
        {
            List<FileBrowserItem> fileBrowserList = new List<FileBrowserItem>();
            string serverPath = Server.MapPath(directory);
            string fileName = "";
            foreach (var dirPath in Directory.GetDirectories(serverPath))
            {
                fileName = Path.GetFileName(dirPath);
                fileBrowserList.Add(new FileBrowserItem() { Name = fileName, Url = directory + fileName + "/", Type = FileBrowserItemType.Directory });
            }
            foreach (var filePath in Directory.GetFiles(serverPath))
            {
                fileName = Path.GetFileName(filePath);
                fileBrowserList.Add(new FileBrowserItem() { Name = fileName, Url = directory + fileName, Type = this.GetFileBrowserItemType(fileName) });
            }
            return fileBrowserList;
        }

        private string GetUploadDir(string type)
        {
            string uploadDir = WebConfigurationManager.AppSettings["UploadDir"];
            if (String.IsNullOrEmpty(uploadDir))
            {
                uploadDir = defaultUploadDir;
            }
            if (type == "image")
            {
                uploadDir += imageSubDir + "/";
            }
            else if (type == "file")
            {
                uploadDir += fileSubDir + "/";
            }
            return uploadDir;
        }

        private bool FileExtensionAllowed(string fileName, string type)
        {
            if (type == "image")
            {
                return imageAllowedExtensions.Contains(this.GetExtension(fileName));
            }
            else
            {
                return fileAllowedExtensions.Contains(this.GetExtension(fileName));
            }
        }

        private FileBrowserItemType GetFileBrowserItemType(string fileName)
        {
            if (imageAllowedExtensions.Contains(this.GetExtension(fileName)))
            {
                return FileBrowserItemType.Image;
            }
            return FileBrowserItemType.File;
        }

        private string GetExtension(string fileName)
        {
            return Path.GetExtension(fileName).Replace(".", "").Replace(".", "");
        }
    }
}
