﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;
using WebAppAdmin.DataProvider.Core;
using WebAppAdmin.Model.Core;
using WebAppAdmin.UI.Core;
using WebAppAdmin.UI.View.Core;
using WebAppAdmin.Areas.Admin.Models;
using WebAppAdmin.Areas.Admin.Helpers;
using WebAppAdmin.Areas.Admin.Resources;
using WebAppAdmin.Areas.Admin.Resources.EntityView;

namespace WebAppAdmin.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class EntityViewController : Controller
    {
        private IEntityManagerFactory entityManagerFactory;
        private IEntityItemManager entityItemManager;
        private IViewGenerator viewGenerator;
        private IControlConvention controlConvention;

        public EntityViewController(IEntityManagerFactory entityManagerFactory, IEntityItemManager entityItemManager, IViewGenerator viewGenerator, IControlConvention controlConvention)
        {
            if (entityManagerFactory == null)
            {
                throw new ArgumentNullException("entityManagerFactory");
            }
            if (entityItemManager == null)
            {
                throw new ArgumentNullException("entityItemManager");
            }
            if (viewGenerator == null)
            {
                throw new ArgumentNullException("viewGenerator");
            }
            if (controlConvention == null)
            {
                throw new ArgumentNullException("controlConvention");
            }
            this.entityManagerFactory = entityManagerFactory;
            this.entityItemManager = entityItemManager;
            this.viewGenerator = viewGenerator;
            this.controlConvention = controlConvention;
        }

        public ActionResult Index()
        {
            ViewBag.EntityItems = entityItemManager.GetEntityItems();
            return View();
        }

        [HttpPost]
        public ActionResult List(string entityManagerName, string entityName)
        {
            EntityResourceManager resourceManager = new EntityResourceManager(entityManagerName, entityName);
            ViewBag.EntityName = resourceManager.GetString(entityName);
            return PartialView("_List", EntityViewsManager.GetEntityViewFileList(entityManagerName, entityName));
        }

        [HttpPost]
        public ActionResult ListView(string entityManagerName, string entityName)
        {
            if (String.IsNullOrEmpty(entityManagerName))
            {
                throw new ArgumentNullException("entityManagerName");
            }
            if (String.IsNullOrEmpty(entityName))
            {
                throw new ArgumentNullException("entityName");
            }
            ListViewManager listViewManager = new ListViewManager(entityManagerFactory, entityManagerName, entityName);
            EntityResourceManager resourceManager = new EntityResourceManager(entityManagerName, entityName);
            ViewBag.ResourceManager = resourceManager;
            ViewBag.Heading = _ListView.Editing_list_view + " " + resourceManager.GetString(entityName);
            return PartialView("_ListView", listViewManager.ListView);
        }

        [HttpPost]
        public JsonResult SaveListView(ListView listView)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ListViewManager listViewManager = new ListViewManager(entityManagerFactory, listView.EntityManagerName, listView.EntityName);
                    listView.Filter = new List<FilterItem>();
                    foreach (var item in listView.FilterProperties)
                    {
                        if (item.IsActive)
                        {
                            listView.Filter.Add(new FilterItem() { PropertyName = item.Name, Value = null });
                        }
                    }
                    listViewManager.Save(listView);
                    return Json("");
                }
                catch (Exception ex)
                {
                    Response.StatusCode = 500;
                    ModelErrors modelErrors = new ModelErrors(ex);
                    return Json(new { errors = modelErrors.Errors });
                }
            }
            else
            {
                Response.StatusCode = 500;
                ModelErrors modelErrors = new ModelErrors(ModelState);
                return Json(new { errors = modelErrors.Errors, propertyErrors = modelErrors.PropertyErrors });
            }
        }

        [HttpPost]
        public JsonResult Delete(List<string> idList, string entityManagerName, string entityName)
        {
            if (idList == null)
            {
                throw new ArgumentNullException("idList");
            }
            if (String.IsNullOrEmpty(entityManagerName))
            {
                throw new ArgumentNullException("entityManagerName");
            }
            if (String.IsNullOrEmpty(entityName))
            {
                throw new ArgumentNullException("entityName");
            }
            try
            {
                EntityViewsManager.Delete(idList, entityManagerName, entityName);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                ModelErrors modelErrors = new ModelErrors(ex);
                return Json(new { errors = modelErrors.Errors });
            }
            return Json("");
        }

        [HttpPost]
        public ActionResult EditView(string entityManagerName, string entityName)
        {
            if (String.IsNullOrEmpty(entityManagerName))
            {
                throw new ArgumentNullException("entityManagerName");
            }
            if (String.IsNullOrEmpty(entityName))
            {
                throw new ArgumentNullException("entityName");
            }
            EntityResourceManager resourceManager = new EntityResourceManager(entityManagerName, entityName);
            EditViewManager editViewManager = new EditViewManager(entityManagerName, entityName, entityManagerFactory, viewGenerator, controlConvention, resourceManager, UIElementMode.Edit);
            ViewBag.Heading = _EditView.Editing_edit_view + " " + resourceManager.GetString(entityName);
            return PartialView("_EditView", editViewManager.EditView);
        }

        [HttpPost]
        public JsonResult SaveEditView(EditView editView)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    EditViewManager editViewManager = new EditViewManager(editView.EntityManagerName, editView.EntityName, entityManagerFactory, viewGenerator, controlConvention, null, UIElementMode.Edit);
                    editViewManager.EditView.ControllerName = editView.ControllerName;
                    editViewManager.EditView.ActionName = editView.ActionName;
                    editViewManager.EditView.Name = editView.Name;
                    editViewManager.EditView.View.ResourceManager = null;
                    editViewManager.Save();
                    return Json("");
                }
                catch (Exception ex)
                {
                    Response.StatusCode = 500;
                    ModelErrors modelErrors = new ModelErrors(ex);
                    return Json(new { errors = modelErrors.Errors });
                }
            }
            else
            {
                Response.StatusCode = 500;
                ModelErrors modelErrors = new ModelErrors(ModelState);
                return Json(new { errors = modelErrors.Errors, propertyErrors = modelErrors.PropertyErrors });
            }
        }

        [HttpPost]
        public ActionResult GetUIElementEditor(string entityManagerName, string entityName, string uiElementId)
        {
            EditViewManager editViewManager = new EditViewManager(entityManagerName, entityName, entityManagerFactory, viewGenerator, controlConvention, null, UIElementMode.Edit);
            UIElement uiElement = GetUIElementById(editViewManager.EditView.View, uiElementId);
            if (uiElement != null)
            {
                string uiElementName = uiElement.GetType().Name;
                WebAppAdmin.UI.Core.Attribute attribute = uiElement.Attributes.Where(item => item.Name == "data-control").FirstOrDefault();
                if (attribute != null)
                {
                    uiElementName = attribute.Value;
                }
                string partialViewName = "_" + uiElementName + "Editor";
                ViewEngineResult viewEngineResult = ViewEngines.Engines.FindPartialView(this.ControllerContext, partialViewName);
                if (viewEngineResult.View != null)
                {
                    return PartialView(partialViewName, uiElement);
                }
                else if (uiElement is Control)
                {
                    return PartialView("_ControlEditor", uiElement);
                }
            }
            ContentResult contentResult = new ContentResult();
            contentResult.Content = _EditView.The_selected_element_do_not_have_an_editor;
            return contentResult;
        }

        [HttpPost]
        public JsonResult MoveUIElement(string entityManagerName, string entityName, string uiElementId, string command)
        {
            if (String.IsNullOrEmpty(entityManagerName))
            {
                throw new ArgumentNullException("entityManagerName");
            }
            if (String.IsNullOrEmpty(entityName))
            {
                throw new ArgumentNullException("entityName");
            }
            if (String.IsNullOrEmpty(command))
            {
                throw new ArgumentNullException("command");
            }
            if (command != "add" && String.IsNullOrEmpty(uiElementId))
            {
                throw new ArgumentNullException("uiElementId");
            }

            EditViewManager editViewManager = new EditViewManager(entityManagerName, entityName, entityManagerFactory, viewGenerator, controlConvention, null, UIElementMode.Edit);
            Dictionary<string, object> result = new Dictionary<string, object>();
            UIElement uiElement = null;
            switch (command)
            {
                case "add":
                    List<string> propertyNames = new List<string>();
                    foreach (var prop in editViewManager.EditView.Properties)
                    {
                        if (!prop.IsAutoGenerated)
                        {
                            propertyNames.Add(prop.Name);
                        }
                    }
                    RemoveBindedProperty(editViewManager.EditView.View, propertyNames);
                    if (propertyNames.Count > 0)
                    {
                        List<object> properties = new List<object>();
                        EntityTypeProperty entityTypeProperty = null;
                        using (var entityManager = entityManagerFactory.GetInstance(entityManagerName))
                        {
                            foreach (var propName in propertyNames)
                            {
                                entityTypeProperty = editViewManager.EditView.Properties.First<EntityTypeProperty>(x => x.Name == propName);
                                if (entityTypeProperty != null)
                                {
                                    properties.Add(new { propertyName = propName, controls = controlConvention.MatchControls(entityTypeProperty.Name, entityTypeProperty.Type, editViewManager.EditView.EntityType, entityManager.EntityTypeManager) });
                                }
                            }
                        }
                        result.Add("properties", properties);
                    }
                    else
                    {
                        Response.StatusCode = 500;
                        result.Add("error", _EditView.The_properties_are_not_found);
                    }
                    break;
                case "up":
                    uiElement = GetUIElementById(editViewManager.EditView.View, uiElementId);
                    if (uiElement != null
                        && uiElement.Parent != null)
                    {
                        UIElement parent = uiElement.Parent;
                        int index = parent.Children.IndexOf(uiElement);
                        if (index > 0)
                        {
                            index = index - 1;
                            parent.Children.Remove(uiElement);
                            parent.Children.Insert(index, uiElement);
                            result.Add("index", index);
                        }
                        else
                        {
                            Response.StatusCode = 500;
                        }
                    }
                    else
                    {
                        Response.StatusCode = 500;
                    }
                    break;
                case "down":
                    uiElement = GetUIElementById(editViewManager.EditView.View, uiElementId);
                    if (uiElement != null
                        && uiElement.Parent != null)
                    {
                        UIElement parent = uiElement.Parent;
                        int index = parent.Children.IndexOf(uiElement);
                        if (index < parent.Children.Count - 1)
                        {
                            index = index + 1;
                            parent.Children.Remove(uiElement);
                            parent.Children.Insert(index, uiElement);
                            result.Add("index", index);
                        }
                        else
                        {
                            Response.StatusCode = 500;
                        }
                    }
                    else
                    {
                        Response.StatusCode = 500;
                    }
                    break;
                case "remove":
                    uiElement = GetUIElementById(editViewManager.EditView.View, uiElementId);
                    if (uiElement != null)
                    {
                        uiElement.Parent.Children.Remove(uiElement);
                    }
                    else
                    {
                        Response.StatusCode = 500;
                    }
                    break;
            }
            if (Response.StatusCode != 500)
            {
                editViewManager.Save(true);
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult GetControlList(string entityManagerName, string entityName, string uiElementId, bool getItemControls = false)
        {
            if (String.IsNullOrEmpty(entityManagerName))
            {
                throw new ArgumentNullException("entityManagerName");
            }
            if (String.IsNullOrEmpty(entityName))
            {
                throw new ArgumentNullException("entityName");
            }
            if (String.IsNullOrEmpty(uiElementId))
            {
                throw new ArgumentNullException("uiElementId");
            }
            Dictionary<string, object> result = new Dictionary<string, object>();

            if (!String.IsNullOrEmpty(uiElementId))
            {
                EditViewManager editViewManager = new EditViewManager(entityManagerName, entityName, entityManagerFactory, viewGenerator, controlConvention, null, UIElementMode.Edit);
                UIElement uiElement = GetUIElementById(editViewManager.EditView.View, uiElementId);
                if (uiElement != null
                    && uiElement is Control)
                {
                    EntityTypeProperty entityTypeProperty = editViewManager.EditView.EntityType.Properties.Where(item => item.Name == ((Control)uiElement).Name).FirstOrDefault();
                    if (entityTypeProperty != null)
                    {
                        IList<string> controls = null;
                        using (var entityManager = entityManagerFactory.GetInstance(entityManagerName))
                        {
                            if (uiElement is ItemsControl
                                && getItemControls)
                            {
                                Type[] genericTypes = entityTypeProperty.Type.GetGenericArguments();
                                Type genericArgumentType = genericTypes[0];
                                EntityType entityTypeGenericArg = entityManager.EntityTypeManager.GetEntityTypes().Where(item => item.Type == genericArgumentType).FirstOrDefault();
                                if (entityTypeGenericArg != null)
                                {
                                    foreach (var propItem in entityTypeGenericArg.Properties)
                                    {
                                        controls = controlConvention.MatchControls(propItem.Name, propItem.Type, entityTypeGenericArg, entityManager.EntityTypeManager);
                                        if (controls != null
                                            && controls.Count > 1)
                                        {
                                            result.Add(propItem.Name, controls);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                controls = controlConvention.MatchControls(entityTypeProperty.Name, entityTypeProperty.Type, editViewManager.EditView.EntityType, entityManager.EntityTypeManager);
                                if (controls != null
                                    && controls.Count > 1)
                                {
                                    result.Add("controls", controls);
                                }
                            }
                        }
                    }
                }
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult ChangeUIElementProperty(string entityManagerName, string entityName, string uiElementId, string propertyName, string propertyValue)
        {
            if (String.IsNullOrEmpty(entityManagerName))
            {
                throw new ArgumentNullException("entityManagerName");
            }
            if (String.IsNullOrEmpty(entityName))
            {
                throw new ArgumentNullException("entityName");
            }
            if (String.IsNullOrEmpty(uiElementId))
            {
                throw new ArgumentNullException("uiElementId");
            }
            if (String.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentNullException("propertyName");
            }

            EditViewManager editViewManager = new EditViewManager(entityManagerName, entityName, entityManagerFactory, viewGenerator, controlConvention, null, UIElementMode.Edit);
            UIElement uiElement = GetUIElementById(editViewManager.EditView.View, uiElementId);
            if (uiElement != null)
            {
                this.SetPropertyValue(uiElement, propertyName, propertyValue);
                editViewManager.Save(true);
            }
            return Json("");
        }

        [HttpPost]
        public JsonResult ChangeControl(string entityManagerName, string entityName, string uiElementId, string control, string propertyName = null)
        {
            if (String.IsNullOrEmpty(entityManagerName))
            {
                throw new ArgumentNullException("entityManagerName");
            }
            if (String.IsNullOrEmpty(entityName))
            {
                throw new ArgumentNullException("entityName");
            }
            if (String.IsNullOrEmpty(uiElementId))
            {
                throw new ArgumentNullException("uiElementId");
            }
            if (String.IsNullOrEmpty(control))
            {
                throw new ArgumentNullException("control");
            }

            EditViewManager editViewManager = new EditViewManager(entityManagerName, entityName, entityManagerFactory, viewGenerator, controlConvention, null, UIElementMode.Edit);
            Dictionary<string, object> result = new Dictionary<string, object>();
            UIElement uiElement = GetUIElementById(editViewManager.EditView.View, uiElementId);
            Control newControl = null;
            if (uiElement != null
                && uiElement is Control
                && uiElement.Parent != null)
            {
                EntityTypeProperty entityTypeProperty = editViewManager.EditView.Properties.Where(item => item.Name == ((Control)uiElement).Name).FirstOrDefault();
                if (entityTypeProperty != null)
                {
                    if (String.IsNullOrEmpty(propertyName))
                    {
                        newControl = UIElementManager.GetControl(control);
                        newControl.Id = uiElement.Id;
                        UIElementManager.BindPropertyControl(newControl, entityTypeProperty.Name);
                        UIElement parent = uiElement.Parent;
                        int index = parent.Children.IndexOf(uiElement);
                        parent.Children.Remove(uiElement);
                        parent.Children.Insert(index, newControl);
                    }
                    else
                    {
                        if (uiElement is ItemsControl)
                        {
                            object obj = null;
                            PropertyInfo propertyInfo = null;
                            this.GetProperty(uiElement, propertyName, out  obj, out  propertyInfo);
                            if (obj != null
                                && propertyInfo != null)
                            {
                                Type[] genericTypes = entityTypeProperty.Type.GetGenericArguments();
                                Type genericArgumentType = genericTypes[0];
                                using (var entityManager = entityManagerFactory.GetInstance(entityManagerName))
                                {
                                    EntityType entityTypeGenericArg = entityManager.EntityTypeManager.GetEntityTypes().Where(item => item.Type == genericArgumentType).FirstOrDefault();
                                    if (entityTypeGenericArg != null)
                                    {
                                        Control itemControl = (Control)propertyInfo.GetValue(obj, null);
                                        entityTypeProperty = entityTypeGenericArg.Properties.Where(item => item.Name == itemControl.Name).FirstOrDefault();
                                        if (entityTypeProperty != null)
                                        {
                                            Control newItemControl = UIElementManager.GetControl(control);
                                            UIElementManager.BindPropertyControl(newItemControl, entityTypeProperty.Name);
                                            propertyInfo.SetValue(obj, newItemControl, null);

                                            newControl = (Control)newItemControl.Clone();
                                            newControl.Id = uiElement.Id + "-" + newControl.Name + "-i0";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (newControl != null)
                {
                    editViewManager.Save(true);
                    WebAppAdmin.UI.Core.ViewContext viewContext = new WebAppAdmin.UI.Core.ViewContext();
                    newControl.Render(viewContext);

                    result.Add("resources", viewContext.Resources);
                    result.Add("content", viewContext.Content.ToString());
                }
            }
            return Json(result);
        }

        [HttpPost]
        public JsonResult ChangeItemControlIndex(string entityManagerName, string entityName, string uiElementId, string itemControlsRelatedPropertyName, int index, int newIndex)
        {
            if (String.IsNullOrEmpty(entityManagerName))
            {
                throw new ArgumentNullException("entityManagerName");
            }
            if (String.IsNullOrEmpty(entityName))
            {
                throw new ArgumentNullException("entityName");
            }
            if (String.IsNullOrEmpty(uiElementId))
            {
                throw new ArgumentNullException("uiElementId");
            }

            EditViewManager editViewManager = new EditViewManager(entityManagerName, entityName, entityManagerFactory, viewGenerator, controlConvention, null, UIElementMode.Edit);
            UIElement uiElement = GetUIElementById(editViewManager.EditView.View, uiElementId);
            if (uiElement != null
                && uiElement is ItemsControl)
            {
                object obj = uiElement.GetType().GetProperty(itemControlsRelatedPropertyName).GetValue(uiElement, null);
                if (obj != null
                    && obj is IList)
                {
                    IList list = (IList)obj;
                    int listCount = list.Count;
                    if (index >= 0
                        && index <= listCount - 1
                        && newIndex >= 0
                        && newIndex < listCount - 1)
                    {
                        object item = list[index];
                        list.Remove(item);
                        list.Insert(newIndex, item);
                        editViewManager.Save(true);
                        return Json("");
                    }
                }
            }
            Response.StatusCode = 500;
            return Json("");
        }

        [HttpPost]
        public JsonResult AddProperties(string entityManagerName, string entityName, string param)
        {
            if (String.IsNullOrEmpty(entityManagerName))
            {
                throw new ArgumentNullException("entityManagerName");
            }
            if (String.IsNullOrEmpty(entityName))
            {
                throw new ArgumentNullException("entityName");
            }
            if (String.IsNullOrEmpty(param))
            {
                throw new ArgumentNullException("param");
            }
            EditViewManager editViewManager = new EditViewManager(entityManagerName, entityName, entityManagerFactory, viewGenerator, controlConvention, null, UIElementMode.Edit);
            Dictionary<string, object> result = new Dictionary<string, object>();
            List<Dictionary<string, string>> paramList = (List<Dictionary<string, string>>)JsonSerializer.Deserialize(param, typeof(List<Dictionary<string, string>>));
            EntityType entityType = null;
            using (var entityManager = entityManagerFactory.GetInstance(entityManagerName))
            {
                entityType = entityManager.EntityTypeManager.GetEntityTypes().Where(item => item.Name == entityName).FirstOrDefault();
                WebAppAdmin.UI.Core.ViewContext viewContext = null;
                string uiElementsContent = "";
                List<WebAppAdmin.UI.Core.Resource> uiElementResourceList = new List<WebAppAdmin.UI.Core.Resource>();
                List<string> uiElementIdList = new List<string>();
                UIElement uiElement = null;
                int maxId = 0;
                GetMaxId(editViewManager.EditView.View, ref maxId);
                UIElementIdGenerator uiElementIdGenerator = new UIElementIdGenerator(maxId);
                foreach (var paramItem in paramList)
                {
                    uiElement = viewGenerator.CreateEntityPropertyView(entityType, entityType.Properties.Where(item => item.Name == paramItem["propName"]).FirstOrDefault(), entityManager.EntityTypeManager, UIElementManager.GetControl(paramItem["control"]), controlConvention);
                    uiElement.Id = UIElementIdGenerator.Prefix + uiElementIdGenerator.GetId();
                    foreach (var child in uiElement.Children)
                    {
                        child.Id = UIElementIdGenerator.Prefix + uiElementIdGenerator.GetId();
                        uiElementIdList.Add(child.Id);
                    }

                    editViewManager.EditView.View.Children.Add(uiElement);
                    viewContext = new WebAppAdmin.UI.Core.ViewContext();
                    uiElement.Render(viewContext);
                    uiElementsContent = uiElementsContent + viewContext.Content.ToString();
                    uiElementResourceList.AddRange(viewContext.Resources);
                }
                editViewManager.Save(true);
                result.Add("resources", uiElementResourceList);
                result.Add("content", uiElementsContent);
                result.Add("idList", uiElementIdList);
            }
            return Json(result);
        }

        private void SetPropertyValue(UIElement uiElement, string propertyName, object propertyValue)
        {
            object obj = null;
            PropertyInfo propertyInfo = null;
            this.GetProperty(uiElement, propertyName, out  obj, out  propertyInfo);
            if (obj != null
                && propertyInfo != null)
            {
                propertyValue = Convert.ChangeType(propertyValue, propertyInfo.PropertyType);
                propertyInfo.SetValue(obj, propertyValue, null);
            }
        }

        private void GetProperty(UIElement uiElement, string propertyName, out object obj, out PropertyInfo propertyInfo)
        {
            obj = uiElement;
            propertyInfo = null;
            string[] propNames = propertyName.Split(new Char[] { '.' });
            int propNamesLength = propNames.Length;
            if (propNamesLength == 1)
            {
                propertyInfo = obj.GetType().GetProperty(propertyName);
            }
            else
            {
                string propName = "";
                int index = -1;
                int indexStartArray = -1;
                int indexEndArray = -1;
                for (int i = 0; i < propNamesLength - 1; i++)
                {
                    index = -1;
                    propName = propNames[i];
                    if (propName.IndexOf("[") >= 0)
                    {
                        indexStartArray = propName.IndexOf("[");
                        indexEndArray = propName.IndexOf("]");
                        index = Convert.ToInt32(propName.Substring(indexStartArray + 1, indexEndArray - indexStartArray - 1));
                        propName = propName.Substring(0, indexStartArray);
                    }
                    propertyInfo = obj.GetType().GetProperty(propName);
                    obj = propertyInfo.GetValue(obj, null);
                    if (obj == null)
                    {
                        return;
                    }
                    if (index >= 0)
                    {
                        int itemIndex = 0;
                        foreach (var item in (IEnumerable)obj)
                        {
                            if (itemIndex == index)
                            {
                                obj = item;
                                break;
                            }
                            itemIndex++;
                        }
                    }
                }
                propName = propNames[propNamesLength - 1];
                propertyInfo = obj.GetType().GetProperty(propName);
            }
        }

        private void RemoveBindedProperty(UIElement uiElement, List<string> propertyNames)
        {
            foreach (var child in uiElement.Children)
            {
                if (child is Control)
                {
                    propertyNames.Remove(((Control)child).Name);
                }
                else
                {
                    RemoveBindedProperty(child, propertyNames);
                }
            }
        }

        private UIElement GetUIElementById(UIElement uiElement, string id)
        {
            if (uiElement.Id == id)
            {
                return uiElement;
            }
            else
            {
                foreach (var child in uiElement.Children)
                {
                    if (child.Id == id)
                    {
                        return child;
                    }
                    else
                    {
                        UIElement viewEl = GetUIElementById(child, id);
                        if (viewEl != null)
                        {
                            return viewEl;
                        }
                    }
                }

            }
            return null;
        }

        private void GetMaxId(UIElement uiElement, ref int maxId)
        {
            int currentId = 0;
            foreach (var child in uiElement.Children)
            {
                currentId = Convert.ToInt32(child.Id.Substring(UIElementIdGenerator.Prefix.Length));
                if (currentId > maxId)
                {
                    maxId = currentId;
                }
                GetMaxId(child, ref maxId);
            }
        }
    }
}
