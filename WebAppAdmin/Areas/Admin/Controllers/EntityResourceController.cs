﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using WebAppAdmin.Model.Core;
using WebAppAdmin.Areas.Admin.Models;
using WebAppAdmin.Areas.Admin.Helpers;
using WebAppAdmin.Areas.Admin.Resources.EntityResource;

namespace WebAppAdmin.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class EntityResourceController : Controller
    {
        private IEntityManagerFactory entityManagerFactory;
        private IEntityItemManager entityItemManager;

        public EntityResourceController(IEntityManagerFactory entityManagerFactory, IEntityItemManager entityItemManager)
        {
            if (entityManagerFactory == null)
            {
                throw new ArgumentNullException("entityManagerFactory");
            }
            if (entityItemManager == null)
            {
                throw new ArgumentNullException("entityItemManager");
            }
            this.entityManagerFactory = entityManagerFactory;
            this.entityItemManager = entityItemManager;
        }

        public ActionResult Index(string entityName, string mode)
        {
            if (String.IsNullOrEmpty(entityName))
            {
                return RedirectToAction("Index", new { entityName = "Resource", mode = "list" });
            }
            return View();
        }

        [HttpPost]
        public ActionResult List(string entityName)
        {
            if (entityName == "Resource")
            {
                return PartialView("_ResourceList", EntityResourceManager.GetResourceFileList());
            }
            else if (entityName == "Culture")
            {
                return PartialView("_CultureList", CultureManager.GetList());
            }
            else
            {
                return new ContentResult();
            }
        }

        [HttpPost]
        public ActionResult Edit(string entityName, string id)
        {
            if (entityName == "Resource")
            {
                if (!String.IsNullOrEmpty(id))
                {
                    ViewBag.Heading = _ResourceEdit.Editing_resource + " " + id;
                    return PartialView("_ResourceEdit", EntityResourceManager.GetResource(id, true));
                }
                else
                {
                    return List(entityName);
                }
            }
            else if (entityName == "Culture")
            {
                Culture culture = null;
                if (String.IsNullOrEmpty(id))
                {
                    culture = new Culture();
                }
                else
                {
                    culture = CultureManager.GetList().Where<Culture>(item => item.Name == id).FirstOrDefault();
                }
                ViewBag.Heading = (String.IsNullOrEmpty(id) ? _CultureEdit.Creating_culture : _CultureEdit.Editing_culture) + " " + id;
                return PartialView("_CultureEdit", culture);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public JsonResult CreateResources(List<string> culrureList)
        {
            try
            {
                EntityResourceManager.CreateResources(culrureList, entityManagerFactory, entityItemManager);
                return Json("");
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                return Json(new { error = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult UpdateResource(List<EntityResource> resource, string id)
        {
            try
            {
                if (this.TryValidateModel(resource))
                {
                    EntityResourceManager.Update(resource, id);
                }
                else
                {
                    Response.StatusCode = 500;
                    ModelErrors modelErrors = new ModelErrors(ModelState);
                    return Json(new { errors = modelErrors.Errors, propertyErrors = modelErrors.PropertyErrors });
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                ModelErrors modelErrors = new ModelErrors(ex);
                return Json(new { errors = modelErrors.Errors });
            }
            return Json("");
        }

        [HttpPost]
        public JsonResult DeleteResource(List<string> idList)
        {
            try
            {
                EntityResourceManager.Delete(idList);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                ModelErrors modelErrors = new ModelErrors(ex);
                return Json(new { errors = modelErrors.Errors });
            }
            return Json("");
        }

        [HttpPost]
        public JsonResult GetCultureList()
        {
            try
            {
                return Json(CultureManager.GetList());
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                ModelErrors modelErrors = new ModelErrors(ex);
                return Json(new { errors = modelErrors.Errors });
            }
        }

        [HttpPost]
        public JsonResult SaveCulture(Culture culture, string id = "")
        {
            try
            {
                if (this.TryValidateModel(culture))
                {
                    CultureManager.Save(culture, id);
                }
                else
                {
                    Response.StatusCode = 500;
                    ModelErrors modelErrors = new ModelErrors(ModelState);
                    return Json(new { errors = modelErrors.Errors, propertyErrors = modelErrors.PropertyErrors });
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                ModelErrors modelErrors = new ModelErrors(ex);
                return Json(new { errors = modelErrors.Errors });
            }
            return Json("");
        }

        [HttpPost]
        public JsonResult DeleteCulture(List<string> idList)
        {
            try
            {
                ModelErrors modelErrors = EntityResourceManager.ValidateDeleteCulture(idList);
                if (modelErrors.Errors.Count > 0)
                {
                    Response.StatusCode = 500;
                    return Json(new { errors = modelErrors.Errors });
                }
                else
                {
                    CultureManager.Delete(idList);
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                ModelErrors modelErrors = new ModelErrors(ex);
                return Json(new { errors = modelErrors.Errors });
            }
            return Json("");
        }
    }
}
