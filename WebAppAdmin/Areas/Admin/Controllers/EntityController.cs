﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using WebAppAdmin.DataProvider.Core;
using WebAppAdmin.Model.Core;
using WebAppAdmin.Areas.Admin.Models;
using WebAppAdmin.Areas.Admin.Helpers;
using WebAppAdmin.UI.Core;
using WebAppAdmin.UI.View.Core;
using WebAppAdmin.UI.Elements;
using WebAppAdmin.Areas.Admin.Resources.Entity;

namespace WebAppAdmin.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator, Content manager")]
    public class EntityController : Controller
    {
        private IEntityManagerFactory entityManagerFactory;
        private IEntityItemManager entityItemManager;
        private IValueConverter valueConverter;
        private IViewGenerator viewGenerator;
        private IControlConvention controlConvention;

        public EntityController(IEntityManagerFactory entityManagerFactory, IEntityItemManager entityItemManager, IValueConverter valueConverter, IViewGenerator viewGenerator, IControlConvention controlConvention)
        {
            if (entityManagerFactory == null)
            {
                throw new ArgumentNullException("entityManagerFactory");
            }
            if (entityItemManager == null)
            {
                throw new ArgumentNullException("entityItemManager");
            }
            if (valueConverter == null)
            {
                throw new ArgumentNullException("valueConverter");
            }
            if (viewGenerator == null)
            {
                throw new ArgumentNullException("viewGenerator");
            }
            if (controlConvention == null)
            {
                throw new ArgumentNullException("controlConvention");
            }
            this.entityManagerFactory = entityManagerFactory;
            this.entityItemManager = entityItemManager;
            this.valueConverter = valueConverter;
            this.viewGenerator = viewGenerator;
            this.controlConvention = controlConvention;
        }

        public ActionResult Index()
        {
            ViewBag.EntityItems = entityItemManager.GetEntityItems();
            return View();
        }

        public ActionResult List(string entityManagerName, string entityName, string mode, string sender = "")
        {
            if (String.IsNullOrEmpty(entityManagerName))
            {
                throw new ArgumentNullException("entityManagerName");
            }
            if (String.IsNullOrEmpty(entityName))
            {
                throw new ArgumentNullException("entityName");
            }
            return View();
        }

        [HttpPost]
        public ActionResult List(string entityManagerName, string entityName, string mode, bool showFilter = false, string filter = "", string sort = "", int pageNumber = 1, int pageSize = 20, string sender = "")
        {
            if (String.IsNullOrEmpty(entityManagerName))
            {
                throw new ArgumentNullException("entityManagerName");
            }
            if (String.IsNullOrEmpty(entityName))
            {
                throw new ArgumentNullException("entityName");
            }
            ListViewManager listViewManager = new ListViewManager(entityManagerFactory, entityManagerName, entityName);
            using (var entityManager = entityManagerFactory.GetInstance(entityManagerName))
            {
                ListView listView = listViewManager.GetEntityListView(entityManager, mode, showFilter, filter, sort, pageNumber, pageSize, sender);
                if (listViewManager.ListView.ControllerName != "Entity"
                    || listViewManager.ListView.ActionName != "List")
                {
                    Type controllerType = Assembly.GetExecutingAssembly().GetTypes().Where(type => type.IsSubclassOf(typeof(Controller)) && type.Name == listViewManager.ListView.ControllerName + "Controller").FirstOrDefault();
                    Controller сontroller = (Controller)DependencyResolver.Current.GetService(controllerType);
                    MethodInfo method = controllerType.GetMethod(listViewManager.ListView.ActionName);
                    return (ActionResult)method.Invoke(сontroller, new object[] { listView });

                }
                else
                {
                    ResourceManager resourceManager = new EntityResourceManager(entityManagerName, entityName);
                    ViewBag.ResourceManager = resourceManager;
                    ViewBag.Heading = resourceManager.GetString(entityName);
                    return PartialView(listView.Name, listView);
                }
            }
        }

        [HttpPost]
        public ActionResult Edit(string entityManagerName, string entityName, string id = "")
        {
            if (String.IsNullOrEmpty(entityManagerName))
            {
                throw new ArgumentNullException("entityManagerName");
            }
            if (String.IsNullOrEmpty(entityName))
            {
                throw new ArgumentNullException("entityName");
            }
            ResourceManager resourceManager = new EntityResourceManager(entityManagerName, entityName);
            EditViewManager editViewManager = new EditViewManager(entityManagerName, entityName, entityManagerFactory, viewGenerator, controlConvention, resourceManager, UIElementMode.View);
            using (var entityManager = entityManagerFactory.GetInstance(editViewManager.EditView.EntityManagerName))
            {
                EntityType entityType = entityManager.EntityTypeManager.GetEntityTypes().Where(item => item.Name == entityName).FirstOrDefault();
                object entity = null;
                if (String.IsNullOrEmpty(id))
                {
                    entity = Activator.CreateInstance(entityType.Type);
                }
                else
                {
                    MethodInfo method = entityManager.GetType().GetMethod("Get");
                    MethodInfo genericMethod = method.MakeGenericMethod(entityType.Type);
                    entity = genericMethod.Invoke(entityManager, new object[] { EntityKeysConverter.ConvertToKeyType(HttpUtility.UrlDecode(id), entityType) });
                }

                if (editViewManager.EditView.ControllerName != "Entity"
                    || editViewManager.EditView.ActionName != "Edit")
                {
                    editViewManager.EditView.View.ViewModel = entity;
                    Type controllerType = Assembly.GetExecutingAssembly().GetTypes().Where(type => type.IsSubclassOf(typeof(Controller)) && type.Name == editViewManager.EditView.ControllerName + "Controller").FirstOrDefault();
                    Controller сontroller = (Controller)DependencyResolver.Current.GetService(controllerType);
                    MethodInfo method = controllerType.GetMethod(editViewManager.EditView.ActionName);
                    return (ActionResult)method.Invoke(сontroller, new object[] { editViewManager.EditView });
                }
                else
                {
                    ViewModelConverter viewModelConverter = new ViewModelConverter(valueConverter);
                    List<Control> controls = new List<Control>();
                    this.GetControls(editViewManager.EditView.View, controls);
                    editViewManager.EditView.View.ViewModel = viewModelConverter.ConvertToViewModel(entity, entityManager.EntityTypeManager, controls);
                    if (viewModelConverter.ModelErrors.HasErrors())
                    {
                        Response.StatusCode = 500;
                        return Json(new { errors = viewModelConverter.ModelErrors.Errors, propertyErrors = viewModelConverter.ModelErrors.PropertyErrors });
                    }
                    else
                    {
                        ViewBag.Heading = (String.IsNullOrEmpty(id) ? _Edit.Creating : _Edit.Editing) + " " + resourceManager.GetString(entityName);
                        return PartialView(editViewManager.EditView.Name, editViewManager.EditView);
                    }
                }
            }
        }

        private void GetControls(UIElement uiElement, List<Control> controls)
        {
            if (uiElement is Control)
            {
                controls.Add((Control)uiElement);
            }
            if (!(uiElement is ItemsControl))
            {
                foreach (var child in uiElement.Children)
                {
                    GetControls(child, controls);
                }
            }
        }

        [HttpPost]
        public JsonResult GetEntityItem(string entityManagerName, string entityName, string id = "")
        {
            if (String.IsNullOrEmpty(entityManagerName))
            {
                throw new ArgumentNullException("entityManagerName");
            }
            if (String.IsNullOrEmpty(entityName))
            {
                throw new ArgumentNullException("entityName");
            }
            try
            {
                using (var entityManager = entityManagerFactory.GetInstance(entityManagerName))
                {
                    EntityType entityType = entityManager.EntityTypeManager.GetEntityTypes().Where(item => item.Name == entityName).FirstOrDefault();
                    MethodInfo method = entityManager.GetType().GetMethod("Get");
                    MethodInfo generic = method.MakeGenericMethod(entityType.Type);
                    object entity = generic.Invoke(entityManager, new object[] { EntityKeysConverter.ConvertToKeyType(id, entityType) });
                    return Json(EntityViewModelConverter.ConvertToEntityItem(entity, entityType));
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                ModelErrors modelErrors = new ModelErrors(ex);
                return Json(new { errors = modelErrors.Errors });
            }
        }

        [HttpPost]
        public JsonResult Save(string entityManagerName, string entityName, string viewModel, string id = "")
        {
            if (String.IsNullOrEmpty(entityManagerName))
            {
                throw new ArgumentNullException("entityManagerName");
            }
            if (String.IsNullOrEmpty(entityName))
            {
                throw new ArgumentNullException("entityName");
            }
            if (viewModel == null)
            {
                throw new ArgumentNullException("viewModel");
            }

            EditViewManager editViewManager = new EditViewManager(entityManagerName, entityName, entityManagerFactory, viewGenerator, controlConvention, null, UIElementMode.View);
            List<Control> controls = new List<Control>();
            this.GetControls(editViewManager.EditView.View, controls);

            try
            {
                using (var entityManager = entityManagerFactory.GetInstance(entityManagerName))
                {
                    EntityType entityType = entityManager.EntityTypeManager.GetEntityTypes().Where(item => item.Name == entityName).FirstOrDefault();
                    ViewModelConverter viewModelConverter = new ViewModelConverter(valueConverter);
                    object entityChanged = viewModelConverter.ConvertToEntity(JsonSerializer.Deserialize<Dictionary<string, object>>(viewModel), id, entityType, entityManager, controls);
                    if (viewModelConverter.ModelErrors.HasErrors())
                    {
                        Response.StatusCode = 500;
                        ModelErrors modelErrors = new ModelErrors(ModelState);
                        return Json(new { errors = modelErrors.Errors, propertyErrors = modelErrors.PropertyErrors });
                    }

                    if (this.TryValidateModel(entityChanged))
                    {
                        if (String.IsNullOrEmpty(id))
                        {
                            entityManager.Create(entityChanged);
                        }
                        else
                        {
                            entityManager.Update(entityChanged);
                        }
                        entityManager.SaveChanges();
                    }
                    else
                    {
                        Response.StatusCode = 500;
                        ModelErrors modelErrors = new ModelErrors(ModelState);
                        return Json(new { errors = modelErrors.Errors, propertyErrors = modelErrors.PropertyErrors });
                    }
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                ModelErrors modelErrors = new ModelErrors(ex);
                return Json(new { errors = modelErrors.Errors });
            }
            return Json("");
        }

        [HttpPost]
        public JsonResult Delete(string entityManagerName, string entityName, List<string> idList)
        {
            if (String.IsNullOrEmpty(entityManagerName))
            {
                throw new ArgumentNullException("entityManagerName");
            }
            if (String.IsNullOrEmpty(entityName))
            {
                throw new ArgumentNullException("entityName");
            }
            if (idList == null)
            {
                throw new ArgumentNullException("idList");
            }
            try
            {
                using (var entityManager = entityManagerFactory.GetInstance(entityManagerName))
                {
                    EntityType entityType = entityManager.EntityTypeManager.GetEntityTypes().Where(item => item.Name == entityName).FirstOrDefault();
                    foreach (string id in idList)
                    {
                        MethodInfo method = entityManager.GetType().GetMethod("Get");
                        MethodInfo genericMethod = method.MakeGenericMethod(entityType.Type);
                        entityManager.Delete(genericMethod.Invoke(entityManager, new object[] { EntityKeysConverter.ConvertToKeyType(id, entityType) }));
                    }
                    entityManager.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                ModelErrors modelErrors = new ModelErrors(ex);
                return Json(new { errors = modelErrors.Errors });
            }
            return Json("");
        }
    }
}
