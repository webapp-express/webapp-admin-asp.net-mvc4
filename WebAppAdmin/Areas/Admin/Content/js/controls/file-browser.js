(function ($) {
    $.fn.fileBrowser = function (options) {
        if (options === "set") {
            var container = this;
            var el = container.find("input[type='text']")
            el.val(arguments[1]);
            setStateClearButton(el, container.find(".btn-clear"));
            return this;
        }
        else {
            var el = this;
            var id = el.attr("id");
            el.removeAttr("id");
            var dataJqwd = el.attr("data-jqwd");
            el.removeAttr("data-jqwd");
            el.wrap('<div id="' + id + '" data-jqwd="' + dataJqwd + '"><div class="input-group"></div></div>');

            var container = el.parents("#" + id);
            container.find(".input-group").append('<div class="input-group-btn"><button class="btn btn-default btn-clear hidden" type="button"><i class="fa fa-remove"></i></button><button class="btn btn-default btn-select" type="button"><i class="fa fa-search"></i></button></div>');

            var img = container.find("img");
            el.on("change", function () {
                img.attr("src", $(this).val());
                return false;
            });

            el.on("keydown", function (event) {
                if (event.keyCode == 13) {
                    selectFile(id);
                }
                else if (event.keyCode == 46) {
                    clearVal(el);
                }
                return false;
            });

            var btnSelect = container.find(".btn-select");
            btnSelect.on("click", function () {
                selectFile(id);
                return false;
            });

            var btnClear = container.find(".btn-clear");
            btnClear.on("click", function () {
                clearVal(el);
                return false;
            });

            setStateClearButton(el, btnClear);

            return this;
        }

        function selectFile(id) {
            window.open('/Admin/FileBrowser/Browser?type=file&sender=' + id, '_blank', 'location=no, menubar=no, status=no, titlebar=no, toolbar=no, scrollbars=yes, resizable=yes');
        }

        function clearVal(el) {
            el.val("");
            setStateClearButton(el, btnClear);
        }

        function setStateClearButton(el, btnClear) {
            if (el.val()) {
                btnClear.removeClass("hidden");
            }
            else {
                btnClear.addClass("hidden");
            }
        }
    };
}(jQuery));