(function ($) {
    $.fn.entityBox = function (options) {
        var el = this;
        if (options === "set") {
            var entity = arguments[1];
            el.children(".Key").val(entity.Key);
            el.children("input[type='text']").val(entity.Text);
            setStateClearButton(el, el.find(".btn-clear"));
            return this;
        }
        else {
            var textEl = el.children("input[type='text']");
            textEl.addClass("form-control");
            el.append('<div class="input-group-btn"><button class="btn btn-default btn-clear hidden" type="button"><i class="fa fa-remove"></i></button><button class="btn btn-default btn-select" type="button"><i class="fa fa-search"></i></button></div>');
            var queryStringItems = WebAppAdmin.getQueryStringItems();
            var entityName = el.children(".EntityName").val();
            var id = el.attr("id");

            var btnSelect = el.find(".btn-select");
            btnSelect.on("click", function () {
                selectEntity(queryStringItems.entityManagerName, entityName, id);
                return false;
            });

            var btnClear = el.find(".btn-clear");
            btnClear.on("click", function () {
                clearVal();
                return false;
            });

            setStateClearButton(el, btnClear);

            textEl.on("keydown", function (event) {
                if (event.keyCode == 13) {
                    selectEntity(queryStringItems.entityManagerName, entityName, id);
                }
                else if (event.keyCode == 46) {
                    clearVal();
                }
                return false;
            });

            return this;
        }

        function selectEntity(entityManagerName, entityName, id) {
            window.open('/Admin/Entity/List?entityManagerName=' + entityManagerName + '&entityName=' + entityName + '&mode=select&sender=' + id, '_blank', 'location=no, menubar=no, status=no, titlebar=no, toolbar=no, scrollbars=yes, resizable=yes');
        }

        function clearVal() {
            el.children(".Key").val("");
            el.children("input[type='text']").val("");
            setStateClearButton(el, btnClear);
        }

        function setStateClearButton(el, btnClear) {
            if (el.children(".Key").val()) {
                btnClear.removeClass("hidden");
            }
            else {
                btnClear.addClass("hidden");
            }
        }
    };
}(jQuery));
