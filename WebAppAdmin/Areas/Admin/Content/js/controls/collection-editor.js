(function ($) {
    $.fn.collectionEditor = function (options) {
        var el = this;
        var table = el.find("table");
        el.find("table").addClass("table table-bordered");
        el.append('<button class="btn btn-default btn-add" type="button"><i class="fa fa-plus"></i></button>');
        table.find(".item-btn-container").each(function (index) {
            addItemBtn($(this));
        });

        var queryStringItems = WebAppAdmin.getQueryStringItems();
        var propertyname = el.children(".PropertyName").val();
        var control = el.attr("data-control");
        var entityName = el.children(".ItemEntityName").val();
        var indexEl = el.children(".Index");
        var index = parseInt(indexEl.val()) + 1;
        var btnAdd = el.find(".btn-add");

        btnAdd.on("click", function () {
            var content = tmpl(el.children(".ItemTemplate").html(), { index: index });
            var tbody = el.find("tbody");
            tbody.append(content);
            var tr = tbody.find("tr:last");
            tr.find("[data-jqwd]").initWidget();
            addItemBtn(tr.find(".item-btn-container"));
            indexEl.val(index);
            return false;
        });

        el.on("click", ".btn-remove", function () {
            $(this).parents("tr:first").remove();
            return false;
        });

        if (WebAppAdmin.Mode == "Edit") {
            btnAdd.click();
        }

        return this;

        function addItemBtn(itemBtnContainer) {
            itemBtnContainer.append('<i class="fa fa-remove btn-remove"></i>');
        }
    };
}(jQuery));
