(function ($) {
    $.fn.htmlEditor = function (options) {
        var el = this;
        var id = el.attr("id");
        el.removeAttr("id");
        el.wrap('<div id="' + id + '"></div>');
        var editor = CKEDITOR.replace(el[0], options);
        editor.on("change", function (event) {
            editor.updateElement();
        });

        editor.on("instanceReady", function (event) {
            $(window).trigger("resize");
            editor.updateElement();//htmlEncodeOutput
        });
        return this;
    };
}(jQuery));
