﻿$(function () {
    var panelEditor = $("#panel-editor");

    panelEditor.find("[data-command]").on("click", function (event) {
        var command = $(this).attr("data-command");
        var uiElementId = $("#edit-view-panel").find(".ui-el-selector.active").attr("data-ui-el-id");
        moveUIElement(uiElementId, command, $(this));
    });

    function moveUIElement(uiElementId, command, btn) {
        btn.progressButton();
        var queryStringItems = WebAppAdmin.getQueryStringItems();
        $.ajax({
            url: "/Admin/EntityView/MoveUIElement",
            type: "POST",
            data: { entityManagerName: queryStringItems.entityManagerName, entityName: queryStringItems.entityName, uiElementId: uiElementId, command: command }
        }).done(function (data) {
            switch (command) {
                case "add":
                    addUiElements(data);
                    break;
                case "remove":
                    removeUIElement(uiElementId);
                    break;
                case "up":
                    upUIElement(uiElementId);
                    break;
                case "down":
                    downUIElement(uiElementId);
                    break;
            }
            btn.progressButton("end");
        }).fail(function (jqXHR, textStatus) {
            var data = JSON.parse(jqXHR.responseText);
            if (data.error) {
                btn.popover({ content: data.error, placement: "left", trigger: "focus" });
                btn.popover("show");
                setInterval(function () { btn.popover("destroy"); }, 3000);
            }
            btn.progressButton("end");
        });
    }

    function addUiElements(data) {
        if (data.properties) {
            var panelModal = $("#panel-modal");
            var content = tmpl("property_item_tmpl", data);
            panelModal.find(".modal-body-content").html(content);
            panelModal.modal();
        }
    }

    $("#add-props-btn").on("click", function (event) {
        var btn = $(this);
        var panelModal = $("#panel-modal");
        var alert = panelModal.find(".alert");
        alert.addClass("hidden");
        var props = [];
        panelModal.find(".add-props-selector:checked").each(function (index) {
            props.push({ propName: $(this).parent().children(".add-props-val").val(), control: $(this).parents(".form-group:first").find(".add-props-uielement-selector").val() });
        });
        if (props.length > 0) {
            btn.progressButton();
            var queryStringItems = WebAppAdmin.getQueryStringItems();
            $.ajax({
                url: "/Admin/EntityView/AddProperties",
                type: "POST",
                data: { entityManagerName: queryStringItems.entityManagerName, entityName: queryStringItems.entityName, param: JSON.stringify(props) }
            }).done(function (data) {
                addResources(data.resources);
                $("#edit-view-panel").find(".form-body").append(data.content);
                for (var i = 0, length = data.idList.length; i < length; i++) {
                    $("#" + data.idList[i]).initWidget();
                }
                WebAppAdmin.EditView.createUIElSelectors();
                btn.progressButton("end");
                panelModal.modal("hide");
            }).fail(function (jqXHR, textStatus) {
                var data = JSON.parse(jqXHR.responseText);
                if (data.error) {
                    btn.popover({ content: data.error, placement: "top", trigger: "focus" });
                    btn.popover("show");
                    setInterval(function () { btn.popover("destroy"); }, 3000);
                }
                btn.progressButton("end");
            });
        }
        else {
            alert.removeClass("hidden");
            return false;
        }
    });

    function removeUIElement(uiElementId) {
        var editViewPanel = $("#edit-view-panel");
        editViewPanel.find("[data-ui-el-id='" + uiElementId + "']").remove();
        $("#" + uiElementId).find("[id]").each(function (index) {
            editViewPanel.find("[data-ui-el-id='" + $(this).attr("id") + "']").remove();
        });
        $("#" + uiElementId).remove();
        WebAppAdmin.EditView.resize();
    }

    function upUIElement(uiElementId) {
        var uiElement = $("#" + uiElementId);
        var index = uiElement.index();
        if (index > 0) {
            var children = uiElement.parent().children();
            children.eq(index - 1).before(uiElement);
            WebAppAdmin.EditView.resize();
        }
    }

    function downUIElement(uiElementId) {
        var uiElement = $("#" + uiElementId);
        var children = uiElement.parent().children();
        var index = uiElement.index();
        if (index < children.length - 1) {
            children.eq(index + 1).after(uiElement);
            WebAppAdmin.EditView.resize();
        }
    }
});