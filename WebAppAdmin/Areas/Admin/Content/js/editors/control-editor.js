﻿$(function () {
    var controlEditor = $("#control-editor"),
        select = controlEditor.find("select"),
        controlProps = controlEditor.find("[name]"),
        uiElementId = controlEditor.find(".uiElementId").val(),
        queryStringItems = WebAppAdmin.getQueryStringItems();
    $.ajax({
        url: "/Admin/EntityView/GetControlList",
        type: "POST",
        dataType: "json",
        data: { entityManagerName: queryStringItems.entityManagerName, entityName: queryStringItems.entityName, uiElementId: uiElementId }
    }).done(function (data) {
        if (data.controls) {
            var currentControl = select.val();
            var control = null;
            var content = "";
            for (var i = 0, length = data.controls.length; i < length; i++) {
                control = data.controls[i];
                if (control != currentControl) {
                    content = content + "<option value='" + control + "'>" + control + "</option>";
                }
            }
            select.append(content);
            select.prop("disabled", false);
        }
    });

    controlProps.on("change", function () {
        var el = $(this);
        WebAppAdmin.EditView.changeUIElementProperty(queryStringItems.entityManagerName, queryStringItems.entityName, uiElementId, el.attr("name"), el.val());
    });

    select.on("change", function () {
        select.prop("disabled", true);
        $.ajax({
            url: "/Admin/EntityView/ChangeControl",
            type: "POST",
            dataType: "json",
            data: { entityManagerName: queryStringItems.entityManagerName, entityName: queryStringItems.entityName, uiElementId: uiElementId, control: select.val() }
        }).done(function (data) {
            if (data.content) {
                var uiElement = $("#" + uiElementId);
                var parent = uiElement.parent();
                uiElement.remove();
                addResources(data.resources);
                parent.append(data.content);
                uiElement = $("#" + uiElementId);
                uiElement.initWidget();
                WebAppAdmin.EditView.resize();
            }
            select.prop("disabled", false);
        });
    });
});