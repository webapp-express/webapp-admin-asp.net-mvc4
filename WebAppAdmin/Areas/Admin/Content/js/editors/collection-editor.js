﻿$(function () {
    var collectionEditor = $("#collection-editor"),
        uiElementId = collectionEditor.find(".uiElementId").val(),
        itemControlsRelatedPropertyName = "Columns",
        queryStringItems = WebAppAdmin.getQueryStringItems();

    $.ajax({
        url: "/Admin/EntityView/GetControlList",
        type: "POST",
        dataType: "json",
        data: { entityManagerName: queryStringItems.entityManagerName, entityName: queryStringItems.entityName, uiElementId: uiElementId, getItemControls: true }
    }).done(function (data) {
        var select = null, controls = null, currentControl = null, control = null, content = "";
        for (var propName in data) {
            select = collectionEditor.find("input[value='" + propName + "']").parent().children("select");
            if (select.length > 0) {
                controls = data[propName];
                currentControl = select.val();
                content = "";
                for (var i = 0, length = controls.length; i < length; i++) {
                    control = controls[i];
                    if (control != currentControl) {
                        content = content + "<option value='" + control + "'>" + control + "</option>";
                    }
                }
                select.append(content);
                select.prop("disabled", false);
            }
        }
    });

    collectionEditor.find("select").on("change", function () {
        var select = $(this);
        var propertyName = select.attr("name");
        select.prop("disabled", true);
        $.ajax({
            url: "/Admin/EntityView/ChangeControl",
            type: "POST",
            dataType: "json",
            data: { entityManagerName: queryStringItems.entityManagerName, entityName: queryStringItems.entityName, uiElementId: uiElementId, control: select.val(), propertyName: propertyName }
        }).done(function (data) {
            if (data.content) {
                var tdItems = $("#" + uiElementId).find("table").find("tr:last td");
                var index = select.parents("tr:first").index();
                var parent = $(tdItems[index]);
                parent.children().remove();
                addResources(data.resources);
                parent.append(data.content);
                parent.children().initWidget();
                WebAppAdmin.EditView.resize();
            }
            select.prop("disabled", false);
        });
    });

    collectionEditor.find(".Visible").on("change", function () {
        var checkbox = $(this);
        WebAppAdmin.EditView.changeUIElementProperty(queryStringItems.entityManagerName, queryStringItems.entityName, uiElementId, checkbox.attr("name"), checkbox.prop("checked"));
    });

    collectionEditor.find(".ControlClass").on("change", function () {
        var el = $(this);
        WebAppAdmin.EditView.changeUIElementProperty(queryStringItems.entityManagerName, queryStringItems.entityName, uiElementId, el.attr("name"), el.val());
    });

    collectionEditor.find(".btn-prop-order").on("click", function () {
        var btnPropOrder = $(this);
        btnPropOrder.progressButton();
        var propItem = btnPropOrder.parents("tr:first");
        var children = propItem.parent().children();
        var index = propItem.index();
        var newIndex = index;
        var count = children.length;
        if (btnPropOrder.hasClass("btn-prop-order-up")
            && index > 0) {
            newIndex = index - 1;
        }
        else if (btnPropOrder.hasClass("btn-prop-order-down")
            && index < count - 1) {
            newIndex = index + 1;
        }
        if (newIndex != index) {
            changeListItemIndex(index, newIndex, { children: children, count: count, propItem: propItem, btnPropOrder: btnPropOrder });
        }
    });

    function changeListItemIndex(index, newIndex, options) {
        $.ajax({
            url: "/Admin/EntityView/ChangeItemControlIndex",
            type: "POST",
            data: { entityManagerName: queryStringItems.entityManagerName, entityName: queryStringItems.entityName, uiElementId: uiElementId, itemControlsRelatedPropertyName: itemControlsRelatedPropertyName, index: index, newIndex: newIndex }
        }).done(function (data) {
            var table = $("#" + uiElementId).find("table");
            table.find("tr").each(function (rowIndex) {
                var tdItems = $(this).children(rowIndex == 0 ? "th" : "td");
                if (newIndex < index) {
                    $(tdItems[newIndex]).before($(tdItems[index]));
                    options.children.eq(newIndex).before(options.propItem);
                }
                else if (newIndex > index) {
                    $(tdItems[newIndex]).after($(tdItems[index]));
                    options.children.eq(newIndex).after(options.propItem);
                }
            });

            options.propItem.parent().children().each(function (index) {
                var propItem = $(this);
                propItem.find(".Visible").attr("name", itemControlsRelatedPropertyName + "[" + index + "].Visible")
                propItem.find("select").attr("name", itemControlsRelatedPropertyName + "[" + index + "].Control")

                if (index == 0) {
                    propItem.find(".btn-prop-order-up").addClass("invisible");
                }
                else if (index == options.count - 1) {
                    propItem.find(".btn-prop-order-down").addClass("invisible");
                }
                else {
                    propItem.find(".btn-prop-order").removeClass("invisible");
                }
            });

            options.btnPropOrder.progressButton("end");
        }).fail(function (jqXHR, textStatus) {
            options.btnPropOrder.progressButton("end");
        });
    }
});