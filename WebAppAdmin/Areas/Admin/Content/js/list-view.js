﻿$(function () {
    $(".btn-prop-order").on("click", function () {
        var btnPropOrder = $(this);
        var propItem = btnPropOrder.parents("tr:first");
        var children = propItem.parent().children();
        var index = propItem.index();
        var count = children.length;
        if (btnPropOrder.hasClass("btn-prop-order-up")
            && index > 0) {
            children.eq(index - 1).before(propItem);
        }
        else if (btnPropOrder.hasClass("btn-prop-order-down")
            && index < count - 1) {
            children.eq(index + 1).after(propItem);
        }

        var propertyName = btnPropOrder.parents(".table-form-control:first").attr("data-property-name");
        propItem.parent().children().each(function (rowIndex) {
            propItem = $(this);
            propItem.find("." + propertyName + "Name").attr("name", propertyName + "[" + rowIndex + "].Name")
            propItem.find("." + propertyName + "IsActive").attr("name", propertyName + "[" + rowIndex + "].IsActive")
            if (rowIndex == 0) {
                propItem.find(".btn-prop-order-up").addClass("invisible");
            }
            else if (rowIndex == count - 1) {
                propItem.find(".btn-prop-order-down").addClass("invisible");
            }
            else {
                propItem.find(".btn-prop-order").removeClass("invisible");
            }
        });
    });

    $("#save-btn").on("click", function () {
        save();
    });

    $("#close-btn").on("click", function () {
        close();
    });

    function close() {
        WebAppAdmin.router.back();
    }

    function save() {
        hideErrors();
        $.ajax({
            url: "/Admin/EntityView/SaveListView",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ listView: WebAppAdmin.EntityBind.bind({}, $(".form-body")) })
        }).done(function (data) {
            close();
        }).fail(function (jqXHR, textStatus) {
            var data = JSON.parse(jqXHR.responseText);
            if (data.errors) {
                var propErrors = null, propError = null;
                for (var propName in data.propertyErrors) {
                    propErrors = data.propertyErrors[propName];
                    for (var i = 0, length = propErrors.length; i < length; i++) {
                        propError = propErrors[i];
                        if ($.inArray(propError, data.errors) < 0) {
                            data.errors.push(propError);
                        }
                    }
                    setPropertyError(propName);
                }
                if (data.errors.length > 0) {
                    WebAppAdmin.showErrors(data);
                }
            }
        });
    }

    function hideErrors() {
        WebAppAdmin.hideErrors();
        $(".has-error").removeClass("has-error");
    }

    function setPropertyError(propName) {
        $("[name='" + propName + "']").parent().addClass("has-error");
    }
});