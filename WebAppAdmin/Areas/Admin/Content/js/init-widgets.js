﻿function initWidgets() {
    var resources = $("#view-context-resources").val();
    if (resources) {
        addResources(JSON.parse(resources));
    }
    $("[data-jqwd]").initWidget();
}

function addResources(resources) {
    if (resources) {
        var head = $("head");
        var scriptSrcArray = [], linkHrefArray = [];
        var el = null, tagName = null, src = null, href = null;
        head.children().each(function (index) {
            el = $(this);
            tagName = el.prop("tagName").toLowerCase();
            if (tagName == "script") {
                src = el.attr("src");
                if (src) {
                    scriptSrcArray.push(src);
                }
            }
            else if (tagName == "link") {
                href = el.attr("href");
                if (href) {
                    linkHrefArray.push(href);
                }
            }
        });
        var content = "";
        var resource = null;
        for (var i = 0, length = resources.length; i < length; i++) {
            resource = resources[i];
            if (resource.Type == 0
                && $.inArray(resource.Value, scriptSrcArray) < 0) {
                content = content + "<script src='" + resource.Value + "'></script>";
                scriptSrcArray.push(resource.Value);
            }
            else if (resource.Type == 1
                && $.inArray(resource.Value, linkHrefArray) < 0) {
                content = content + "<link href='" + resource.Value + "' rel='stylesheet' />";
                linkHrefArray.push(resource.Value);
            }
        }
        if (content) {
            head.append(content);
        }
    }
}