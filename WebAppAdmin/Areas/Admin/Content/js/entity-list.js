﻿$(function () {
    $("#create-entity").on("click", function () {
        $(this).progressButton();
        var queryStringItems = WebAppAdmin.getQueryStringItems();
        queryStringItems.mode = "edit";
        queryStringItems.id = "";
        WebAppAdmin.navigate(queryStringItems);
    });

    $(".entity-list-item td").on("click", function (event) {
        var entityListItem = $(this);
        if (!entityListItem.hasClass("entity-list-item-selector")) {
            var entityId = entityListItem.parent().find(".entity-id").val();
            var queryStringItems = WebAppAdmin.getQueryStringItems();

            if (queryStringItems.mode == "select") {
                var queryStringItems = WebAppAdmin.getQueryStringItems();
                $.ajax({
                    url: "/Admin/Entity/GetEntityItem",
                    type: "POST",
                    data: { entityManagerName: queryStringItems.entityManagerName, entityName: queryStringItems.entityName, id: entityId }
                }).done(function (data) {
                    var sender = opener.$("#" + queryStringItems.sender);
                    sender[sender.attr("data-jqwd")]("set", data);
                    window.close();
                }).fail(function (jqXHR, textStatus) {
                    var data = JSON.parse(jqXHR.responseText);
                    if (data.errors
                        && data.errors.length > 0) {
                        WebAppAdmin.showErrors(data);
                    }
                });
            }
            else {
                startProgress(entityListItem, event);
                queryStringItems.mode = "edit";
                queryStringItems.id = entityId;
                WebAppAdmin.navigate(queryStringItems);
            }
        }
    });

    $("#delete-entity").on("click", function () {
        var btn = $(this);
        btn.progressButton();
        WebAppAdmin.hideErrors();
        var idList = [];
        $("#entity-list-table").find(".entity-selector:checked").each(function (index) {
            idList.push($(this).parent().find(".entity-id").val());
        });
        if (idList.length > 0) {
            var queryStringItems = WebAppAdmin.getQueryStringItems();
            $.ajax({
                url: "/Admin/Entity/Delete",
                type: "POST",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ entityManagerName: queryStringItems.entityManagerName, entityName: queryStringItems.entityName, idList: idList })
            }).done(function (data) {
                location.reload(true);
            }).fail(function (jqXHR, textStatus) {
                var data = JSON.parse(jqXHR.responseText);
                if (data.errors) {
                    WebAppAdmin.showErrors(data);
                }
                btn.progressButton("end");
            });
        }
    });

    $("#select-entity-list").on("change", function () {
        var checked = $(this).prop("checked");
        $("#entity-list-table").find(".entity-selector").prop("checked", checked);
    });

    $("#filter-entity-list").on("click", function () {
        var queryStringItems = WebAppAdmin.getQueryStringItems();
        queryStringItems["showFilter"] = !(queryStringItems["showFilter"] === "true");
        WebAppAdmin.navigate(queryStringItems);
    });

    $("#apply-filter-btn").on("click", function () {
        var filter = [];
        var elTr = null;
        var PropertyName = null, Operator = null, Value = null;
        $("#entity-list-filter-table").find("tr").each(function (index) {
            elTr = $(this);
            Operator = elTr.find("[name='Operator']").val();
            if (Operator && Operator != "NoSet") {
                PropertyName = elTr.find("[name='PropertyName']").val();
                Value = elTr.find("[name='Value']").val();
                filter.push({ PropertyName: PropertyName, Operator: Operator, Value: Value });
            }
        });
        var queryStringItems = WebAppAdmin.getQueryStringItems();
        if (filter.length > 0) {
            queryStringItems["filter"] = encodeURIComponent(JSON.stringify(filter));
        }
        else {
            queryStringItems["filter"] = "";
        }
        WebAppAdmin.navigate(queryStringItems);
    });

    $("#clear-filter-btn").on("click", function () {
        $("#entity-list-filter-table [name='Operator']").val("NoSet");
        $("#apply-filter-btn").trigger("click");
    });

    $("#close-filter-btn").on("click", function () {
        $("#filter-entity-list").trigger("click");
    });

    $(".entity-list-table-column").on("click", function () {
        var elTh = $(this);
        var sort = [];
        var PropertyName = elTh.find("[name='PropertyName']").val();
        var SortDirection = "Ascending";
        if (elTh.find(".asc").length > 0) {
            SortDirection = "Descending";
        }
        sort.push({ PropertyName: PropertyName, SortDirection: SortDirection });
        var queryStringItems = WebAppAdmin.getQueryStringItems();
        queryStringItems["sort"] = encodeURIComponent(JSON.stringify(sort));
        WebAppAdmin.navigate(queryStringItems);
    });

    function startProgress(el, event) {
        var entityListProgress = $("#entity-list-progress");
        var position = el.position();
        var offset = el.parents("table:first").offset();
        entityListProgress.css("left", event.pageX - offset.left + "px");
        entityListProgress.css("top", position.top + "px");
        entityListProgress.removeClass("hidden");
    }

    function endProgress() {
        $("#entity-list-progress").addClass("hidden");
    }
});