﻿$(function () {
    $("#create-entity").on("click", function () {
        var queryStringItems = WebAppAdmin.getQueryStringItems();
        queryStringItems.mode = "edit";
        queryStringItems.id = "";
        WebAppAdmin.navigate(queryStringItems);
    });

    $(".entity-list-item td").on("click", function () {
        var entityListItem = $(this);
        if (!entityListItem.hasClass("entity-list-item-selector")) {
            var entityId = entityListItem.parent().find(".entity-id").val();
            var queryStringItems = WebAppAdmin.getQueryStringItems();
            queryStringItems.mode = "edit";
            queryStringItems.id = entityId;
            WebAppAdmin.navigate(queryStringItems);
        }
    });

    $("#delete-entity").on("click", function () {
        WebAppAdmin.hideErrors();
        var idList = [];
        $("#entity-list-table").find(".entity-selector:checked").each(function (index) {
            idList.push($(this).parent().find(".entity-id").val());
        });
        if (idList.length > 0) {
            var queryStringItems = WebAppAdmin.getQueryStringItems();
            $.ajax({
                url: "/Admin/EntityResource/DeleteCulture",
                type: "POST",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ idList: idList })
            }).done(function (data) {
                location.reload(true);
            }).fail(function (jqXHR, textStatus) {
                var data = JSON.parse(jqXHR.responseText);
                if (data.errors) {
                    WebAppAdmin.showErrors(data);
                }
            });
        }
    });

    $("#select-entity-list").on("change", function () {
        var checked = $(this).prop("checked");
        $("#entity-list-table").find(".entity-selector").prop("checked", checked);
    });
});