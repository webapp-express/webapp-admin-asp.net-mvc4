﻿$(function () {
    $("#save-btn").on("click", function () {
        save();
    });

    $("#close-btn").on("click", function () {
        close();
    });

    function save() {
        hideErrors();
        var queryStringItems = WebAppAdmin.getQueryStringItems();
        $.ajax({
            url: "/Admin/EntityResource/SaveCulture",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ culture: WebAppAdmin.EntityBind.bind({}, $(".form-body")), id: queryStringItems["id"] })
        }).done(function (data) {
            close();
        }).fail(function (jqXHR, textStatus) {
            var data = JSON.parse(jqXHR.responseText);
            if (data.errors) {
                var propErrors = null, propError = null;
                for (var propName in data.propertyErrors) {
                    propErrors = data.propertyErrors[propName];
                    for (var i = 0, length = propErrors.length; i < length; i++) {
                        propError = propErrors[i];
                        if ($.inArray(propError, data.errors) < 0) {
                            data.errors.push(propError);
                        }
                    }
                    setPropertyError(propName);
                }
                if (data.errors.length > 0) {
                    WebAppAdmin.showErrors(data);
                }
            }
        });
    }

    function close() {
        WebAppAdmin.router.back();
    }

    function hideErrors() {
        WebAppAdmin.hideErrors();
        $(".has-error").removeClass("has-error");
    }

    function setPropertyError(propName) {
        $("[name='" + propName + "']").parent().addClass("has-error");
    }
});