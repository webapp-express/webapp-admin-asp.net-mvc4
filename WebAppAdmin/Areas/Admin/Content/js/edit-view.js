﻿WebAppAdmin.Mode = "Edit";
WebAppAdmin.EditView = WebAppAdmin.EditView || {};

$(function () {
    var editViewPanel = $("#edit-view-panel");

    WebAppAdmin.EditView.resize = function () {
        var panelProperties = $("#panel-properties");
        panelProperties.width(panelProperties.parent().width());

        $("#edit-view-panel").find(".ui-el-selector").each(function (index) {
            var uiElSelector = $(this);
            var uiEl = $("#" + uiElSelector.attr("data-ui-el-id"));
            if (uiEl.length > 0) {
                uiElSelector.css("width", uiEl.width() + parseInt(uiEl.css("padding-left")) + parseInt(uiEl.css("padding-right")) + parseInt(uiEl.css("border-left-width")) + parseInt(uiEl.css("border-right-width")) + "px");
                uiElSelector.css("height", uiEl.height() + parseInt(uiEl.css("padding-top")) + parseInt(uiEl.css("padding-bottom")) + parseInt(uiEl.css("border-top-width")) + parseInt(uiEl.css("border-bottom-width")) + "px");
                var position = uiEl.position();
                uiElSelector.css("left", position.left + "px");
                uiElSelector.css("top", position.top + "px");
            }
        });
    }

    WebAppAdmin.EditView.createUIElSelectors = function () {
        var uiEl = null, uiElId = null, editViewPanel = $("#edit-view-panel");
        editViewPanel.find("[id]").each(function (index) {
            uiEl = $(this);
            uiElId = uiEl.attr("id");
            if (uiElId
                && editViewPanel.find("[data-ui-el-id='" + uiElId + "']").length == 0) {
                var uiElSelector = document.createElement("div");
                uiElSelector.className = "ui-el-selector";
                uiElSelector.setAttribute("data-ui-el-id", uiElId);
                uiElSelector.style.width = uiEl.width() + parseInt(uiEl.css("padding-left")) + parseInt(uiEl.css("padding-right")) + parseInt(uiEl.css("border-left-width")) + parseInt(uiEl.css("border-right-width")) + "px";
                uiElSelector.style.height = uiEl.height() + parseInt(uiEl.css("padding-top")) + parseInt(uiEl.css("padding-bottom")) + parseInt(uiEl.css("border-top-width")) + parseInt(uiEl.css("border-bottom-width")) + "px";
                var position = uiEl.position();
                uiElSelector.style.left = position.left + "px";
                uiElSelector.style.top = position.top + "px";
                editViewPanel[0].appendChild(uiElSelector);
            }
        });
    }

    WebAppAdmin.EditView.changeUIElementProperty = function (entityManagerName, entityName, uiElementId, propertyName, propertyValue, options) {
        $.ajax({
            url: "/Admin/EntityView/ChangeUIElementProperty",
            type: "POST",
            dataType: "json",
            data: { entityManagerName: entityManagerName, entityName: entityName, uiElementId: uiElementId, propertyName: propertyName, propertyValue: propertyValue }
        }).done(function (data) {
            if (options
                && options.success) {
                options.success(data);
            }
        }).fail(function (jqXHR, textStatus) {
            if (options
                && options.error) {
                options.error(jqXHR.responseText);
            }
        });
    }

    initWidgets();
    WebAppAdmin.EditView.createUIElSelectors();
    WebAppAdmin.EditView.resize();

    editViewPanel.on("click", ".ui-el-selector", function (event) {
        clearSelectedUIElements();
        $(this).addClass("active");
        getUIElementEditor($(this));
    });

    function getUIElementEditor(el) {
        var queryStringItems = WebAppAdmin.getQueryStringItems();
        var uiElementId = el.attr("data-ui-el-id");
        $.ajax({
            url: "/Admin/EntityView/GetUIElementEditor",
            type: "POST",
            data: { entityManagerName: queryStringItems.entityManagerName, entityName: queryStringItems.entityName, uiElementId: uiElementId }
        }).done(function (html) {
            $("#panel-properties .panel-body").html(html);
        }).fail(function (jqXHR, textStatus) {
            var data = JSON.parse(jqXHR.responseText);
            if (data.error) {
                alert(data.error);
            }
        });
    }

    function clearSelectedUIElements() {
        editViewPanel.find(".ui-el-selector").removeClass("active");
    }

    $("#property-editor").on("change", function (event) {
        var uiElementId = editViewPanel.find(".ui-el-selector.active").attr("data-ui-el-id");
        if (uiElementId) {
            var command = "change";
            onSelectCommand(command, { uiElementId: uiElementId, control: $(this).val() }, $(this));
        }
    });

    $("#panel-commands [data-command]").on("click", function (event) {
        var command = $(this).attr("data-command");
        if (command == "add") {
            onSelectCommand(command, {}, $(this));
        }
        else {
            var uiElementId = editViewPanel.find(".ui-el-selector.active").attr("data-ui-el-id");
            if (uiElementId) {
                onSelectCommand(command, { uiElementId: uiElementId }, $(this));
            }
        }
    });

    $("#save-btn").on("click", function () {
        save($(this));
    });

    $("#close-btn").on("click", function () {
        close();
    });

    function close() {
        WebAppAdmin.router.back();
    }

    function save(btn) {
        btn.progressButton();
        WebAppAdmin.hideErrors();
        var queryStringItems = WebAppAdmin.getQueryStringItems();
        $.ajax({
            url: "/Admin/EntityView/SaveEditView",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ editView: WebAppAdmin.EntityBind.bind({}, $("#controller-panel")) })
        }).done(function (data) {
            close();
        }).fail(function (jqXHR, textStatus) {
            var data = JSON.parse(jqXHR.responseText);
            if (data.errors
                && data.errors.length > 0) {
                WebAppAdmin.showErrors(data);
            }
            btn.progressButton("end");
        });
    }

    $(window).on("resize", function () {
        WebAppAdmin.EditView.resize();
    });
});