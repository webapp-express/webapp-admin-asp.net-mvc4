﻿WebAppAdmin.EntityBind = WebAppAdmin.EntityBind || {};
WebAppAdmin.EntityBind.bind = function (entity, container) {
    var el = null, propName = null, propVal = null, attrType = null;
    container.find("[name]").each(function (index) {
        el = $(this);
        propName = el.attr("name");
        if (propName) {
            propVal = el.val();
            attrType = el.attr("type");
            if (attrType == "checkbox") {
                if (propVal == "on") {
                    propVal = el.prop("checked");
                }
                else if (!el.prop("checked")) {
                    return;
                }
            }
            setEntityPropVal(entity, propName, propVal);
        }
    });

    return entity;

    function setEntityPropVal(entity, propName, propVal) {
        var newVal = propVal;
        var props = propName.split('.');
        var propsLength = props.length;
        var indexStartArray = -1;
        if (propsLength == 1) {
            var indexStartArray = propName.indexOf('[');
            if (indexStartArray < 0) {
                entity[propName] = propVal;
            }
            else {
                var currObj = entity;
                var currArrayName = propName.substring(0, indexStartArray);
                var currPropVal = currObj[currArrayName];
                if (!currPropVal) {
                    currObj[currArrayName] = [];
                    currPropVal = currObj[currArrayName];
                }
                currObj = currPropVal;
                var indexEndArray = propName.indexOf(']');
                var indexItemArray = parseInt(propName.substring(indexStartArray + 1, indexEndArray));
                currPropVal = currObj[indexItemArray];
                if (!currPropVal) {
                    currObj.splice(indexItemArray, 0, propVal);
                }
                else {
                    currPropVal = propVal;
                }
            }
        }
        else {
            var currObj = entity;
            var currPropVal = null;
            var currPropName = '';
            var currArrayName = '';
            var indexStartArray = -1;
            var indexEndArray = -1;
            var indexItemArray = -1;;
            for (var i = 0, itemsLength = propsLength - 1; i < itemsLength; i++) {
                currPropName = props[i];
                indexStartArray = currPropName.indexOf('[');
                if (indexStartArray < 0) {
                    currPropVal = currObj[currPropName];
                    if (!currPropVal) {
                        currObj[currPropName] = {};
                        currPropVal = currObj[currPropName];
                    }
                    currObj = currPropVal;
                }
                else {
                    if (indexStartArray > 0) {
                        currArrayName = currPropName.substring(0, indexStartArray);
                        currPropVal = currObj[currArrayName];
                        if (!currPropVal) {
                            currObj[currArrayName] = [];
                            currPropVal = currObj[currArrayName];
                        }
                        currObj = currPropVal;
                    }
                    indexEndArray = currPropName.indexOf(']');
                    indexItemArray = parseInt(currPropName.substring(indexStartArray + 1, indexEndArray));
                    currPropVal = currObj[indexItemArray];
                    if (!currPropVal) {
                        currObj.splice(indexItemArray, 0, {});
                        currPropVal = currObj[indexItemArray];
                    }
                    currObj = currPropVal;
                }
            }
            currPropName = props[propsLength - 1];
            currObj[currPropName] = propVal;
        }
    }
}