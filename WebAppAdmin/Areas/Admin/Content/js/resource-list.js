﻿$(function () {
    $("#create-entity").on("click", function () {
        var queryStringItems = WebAppAdmin.getQueryStringItems();
        $.ajax({
            url: "/Admin/EntityResource/GetCultureList",
            type: "POST",
            dataType: "json"
        }).done(function (data) {
            if (data) {
                var panelModal = $("#panel-modal");
                var content = tmpl("culture_item_tmpl", { cultureList: data });
                panelModal.find(".modal-body-content").html(content);
                panelModal.modal();
            }
        }).fail(function (jqXHR, textStatus) {
            var data = JSON.parse(jqXHR.responseText);
            if (data.errors) {
                WebAppAdmin.showErrors(data);
            }
        });
    });

    $("#create-recources-btn").on("click", function (event) {
        var btn = $(this);
        var panelModal = $("#panel-modal");
        var alert = panelModal.find(".alert");
        alert.addClass("hidden");
        var culrureList = [];
        panelModal.find("input[name='Name']:checked").each(function (index) {
            culrureList.push($(this).val());
        });
        if (culrureList.length > 0) {
            btn.progressButton();
            $.ajax({
                url: "/Admin/EntityResource/CreateResources",
                type: "POST",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ culrureList: culrureList })
            }).done(function (data) {
                panelModal.modal("hide");
                location.reload(true);
            }).fail(function (jqXHR, textStatus) {
                var data = JSON.parse(jqXHR.responseText);
                if (data.error) {
                    btn.popover({ content: data.error, placement: "top", trigger: "focus" });
                    btn.popover("show");
                    setInterval(function () { btn.popover("destroy"); }, 3000);
                    btn.progressButton("end");
                }
            });
        }
        else {
            alert.removeClass("hidden");
            return false;
        }
    });

    $(".entity-list-item td").on("click", function () {
        var entityListItem = $(this);
        if (!entityListItem.hasClass("entity-list-item-selector")) {
            var entityId = entityListItem.parent().find(".entity-id").val();
            var queryStringItems = WebAppAdmin.getQueryStringItems();
            queryStringItems.mode = "edit";
            queryStringItems.id = entityId;
            WebAppAdmin.navigate(queryStringItems);
        }
    });

    $("#delete-entity").on("click", function () {
        WebAppAdmin.hideErrors();
        var idList = [];
        $("#entity-list-table").find(".entity-selector:checked").each(function (index) {
            idList.push($(this).parent().find(".entity-id").val());
        });
        if (idList.length > 0) {
            var queryStringItems = WebAppAdmin.getQueryStringItems();
            $.ajax({
                url: "/Admin/EntityResource/DeleteResource",
                type: "POST",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ idList: idList })
            }).done(function (data) {
                location.reload(true);
            }).fail(function (jqXHR, textStatus) {
                var data = JSON.parse(jqXHR.responseText);
                if (data.errors) {
                    WebAppAdmin.showErrors(data);
                }
            });
        }
    });

    $("#select-entity-list").on("change", function () {
        var checked = $(this).prop("checked");
        $("#entity-list-table").find(".entity-selector").prop("checked", checked);
    });
});