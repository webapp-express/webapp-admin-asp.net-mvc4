﻿var WebAppAdmin = WebAppAdmin || {};
WebAppAdmin = (function myfunction() {
    var router = new Router();

    router.route("/Admin\\?entityManagerName=:entityManagerName&entityName=:entityName&mode=:mode*path", entityControllerHandler);
    router.route("/Admin/Entity\\?entityManagerName=:entityManagerName&entityName=:entityName&mode=:mode*path", entityControllerHandler);
    router.route("/Admin/Entity/Index\\?entityManagerName=:entityManagerName&entityName=:entityName&mode=:mode*path", entityControllerHandler);

    router.route("/Admin/EntityView\\?entityManagerName=:entityManagerName&entityName=:entityName&mode=:mode*path", entityViewControllerHandler);
    router.route("/Admin/EntityView/Index\\?entityManagerName=:entityManagerName&entityName=:entityName&mode=:mode*path", entityViewControllerHandler);

    router.route("/Admin/EntityResource\\?entityName=:entityName&mode=:mode*path", entityResourceControllerHandler);
    router.route("/Admin/EntityResource/Index\\?entityName=:entityName&mode=:mode*path", entityResourceControllerHandler);

    function entityControllerHandler(entityManagerName, entityName, mode) {
        switch (mode) {
            case "list":
                showEntityList(entityManagerName, entityName);
                break;
            case "select":
                showEntityList(entityManagerName, entityName);
                break;
            case "edit":
                var queryStringItems = getQueryStringItems();
                showEntityEdit(entityManagerName, entityName, queryStringItems.id ? queryStringItems.id : "");
                break;
        }
    }

    function showEntityList(entityManagerName, entityName) {
        var queryStringItems = getQueryStringItems();
        $.ajax({
            url: "/Admin/Entity/List",
            type: "POST",
            data: queryStringItems
        }).done(function (html) {
            $("#content").html(html);
            var metamodelItemsPanel = $("#metamodel-items-panel");
            metamodelItemsPanel.find("li.active").removeClass("active");
            metamodelItemsPanel.find("a[href='" + location.pathname + "?entityManagerName=" + entityManagerName + "&entityName=" + entityName + "&mode=list']").parent().addClass("active");
        });
    }

    function showEntityEdit(entityManagerName, entityName, id) {
        hideErrors();
        $.ajax({
            url: "/Admin/Entity/Edit",
            type: "POST",
            data: { entityManagerName: entityManagerName, entityName: entityName, id: id }
        }).done(function (html) {
            $("#content").html(html);
        }).fail(function (jqXHR, textStatus) {
            var data = JSON.parse(jqXHR.responseText);
            if (data.errors) {
                showErrors(data);
            }
        });
    }

    function entityViewControllerHandler(entityManagerName, entityName, mode) {
        switch (mode) {
            case "list":
                showEntityViewList(entityManagerName, entityName);
                break;
            case "edit":
                var queryStringItems = getQueryStringItems();
                showEntityViewEdit(entityManagerName, entityName, queryStringItems.id);
                break;
        }
    }

    function showEntityViewList(entityManagerName, entityName) {
        $.ajax({
            url: "/Admin/EntityView/List",
            type: "POST",
            data: { entityManagerName: entityManagerName, entityName: entityName }
        }).done(function (html) {
            $("#content").html(html);
            var metamodelItemsPanel = $("#metamodel-items-panel");
            metamodelItemsPanel.find("li.active").removeClass("active");
            metamodelItemsPanel.find("a[href='" + location.pathname + "?entityManagerName=" + entityManagerName + "&entityName=" + entityName + "&mode=list']").parent().addClass("active");
        });
    }

    function showEntityViewEdit(entityManagerName, entityName, id) {
        var url = "";
        if (id == "EditView") {
            url = "/Admin/EntityView/EditView";
        }
        else if (id == "ListView") {
            url = "/Admin/EntityView/ListView";
        }
        if (url != "") {
            $.ajax({
                url: url,
                type: "POST",
                data: { entityManagerName: entityManagerName, entityName: entityName }
            }).done(function (html) {
                $("#content").html(html);
            });
        }
    }

    function entityResourceControllerHandler(entityName, mode) {
        switch (mode) {
            case "list":
                showEntityResourceList(entityName);
                break;
            case "edit":
                var queryStringItems = getQueryStringItems();
                showEntityResourceEdit(entityName, queryStringItems.id ? queryStringItems.id : "");
                break;
        }
    }

    function showEntityResourceList(entityName) {
        $.ajax({
            url: "/Admin/EntityResource/List",
            type: "POST",
            data: { entityName: entityName }
        }).done(function (html) {
            $("#content").html(html);
            var metamodelItemsPanel = $("#metamodel-items-panel");
            metamodelItemsPanel.find("li.active").removeClass("active");
            metamodelItemsPanel.find("a[href='" + location.pathname + "?entityName=" + entityName + "&mode=list']").parent().addClass("active");
        });
    }

    function showEntityResourceEdit(entityName, id) {
        $.ajax({
            url: "/Admin/EntityResource/Edit",
            type: "POST",
            data: { entityName: entityName, id: id }
        }).done(function (html) {
            $("#content").html(html);
        });
    }

    var navigate = function (queryStringItems) {
        var queryString = "";
        for (var propName in queryStringItems) {
            queryString = queryString + (queryString != "" ? "&" : "") + propName + "=" + queryStringItems[propName];
        }
        var url = window.location.pathname + "?" + queryString;
        router.navigate(url);
    },
    getQueryStringItems = function () {
        var queryStringItems = {}, nameValue = null;
        var nameValueItems = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < nameValueItems.length; i++) {
            nameValue = nameValueItems[i].split('=');
            queryStringItems[nameValue[0]] = nameValue[1];
        }
        return queryStringItems;
    },
    hideErrors = function () {
        $("#errors-panel").alert("close");
    },
    showErrors = function (data) {
        var content = tmpl("errors_tmpl", data);
        $("h1").after(content);
        $(window).scrollTop(0);
    };

    return {
        router: router,
        Mode: "View",
        navigate: navigate,
        getQueryStringItems: getQueryStringItems,
        hideErrors: hideErrors,
        showErrors: showErrors
    };
}());

$(function () {
    WebAppAdmin.router.start();
    $("body").on("click", ".router-link", function () {
        WebAppAdmin.router.navigate($(this).attr("href"));
        return false;
    });
});