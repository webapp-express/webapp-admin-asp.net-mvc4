﻿$(function () {
    initWidgets();

    $("#save-btn").on("click", function () {
        save($(this));
    });

    $("#close-btn").on("click", function () {
        close();
    });

    function save(btn) {
        btn.progressButton();
        hideErrors();
        var queryStringItems = WebAppAdmin.getQueryStringItems();
        $.ajax({
            url: "/Admin/Entity/Save",
            type: "POST",
            dataType: "json",
            data: { entityManagerName: queryStringItems.entityManagerName, entityName: queryStringItems.entityName, viewModel: JSON.stringify(WebAppAdmin.EntityBind.bind({}, $(".form-body"))), id: queryStringItems.id ? queryStringItems.id : "" }
        }).done(function (data) {
            close();
        }).fail(function (jqXHR, textStatus) {
            var data = JSON.parse(jqXHR.responseText);
            if (data.errors) {
                var propErrors = null, propError = null;
                for (var propName in data.propertyErrors) {
                    propErrors = data.propertyErrors[propName];
                    for (var i = 0, length = propErrors.length; i < length; i++) {
                        propError = propErrors[i];
                        if ($.inArray(propError, data.errors) < 0) {
                            data.errors.push(propError);
                        }
                    }
                    setPropertyError(propName);
                }
                if (data.errors.length > 0) {
                    WebAppAdmin.showErrors(data);
                }
            }
            btn.progressButton("end");
        });
    }

    function close() {
        var getQueryStringItems = WebAppAdmin.getQueryStringItems();
        getQueryStringItems["mode"] = "list";
        WebAppAdmin.navigate(getQueryStringItems);
    }

    function hideErrors() {
        WebAppAdmin.hideErrors();
        $(".has-error").removeClass("has-error");
    }

    function setPropertyError(propName) {
        $("[name='" + propName + "']").parent().addClass("has-error");
    }
});