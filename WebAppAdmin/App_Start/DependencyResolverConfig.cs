﻿using System;
using System.Web.Mvc;
using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;
using System.Reflection;
using WebAppAdmin.Model.Core;
using WebAppAdmin.UI.View.Core;
using WebAppAdmin.Areas.Admin.Helpers;

namespace WebAppAdmin
{
    public static class DependencyResolverConfig
    {
        public static void Register()
        {
            var container = new Container();

            // Register your types, for instance:
            //The configuration of the EntityFramework data provider
            //container.RegisterSingle<IEntityManagerFactory>(new EntityManagerFactory { 
            //{ "IdentityDbContext", () => new WebAppAdmin.DataProvider.EntityFramework.EntityManager(new Microsoft.AspNet.Identity.EntityFramework.IdentityDbContext()) }, 
            //{ "SiteModelsContext", () => new WebAppAdmin.DataProvider.EntityFramework.EntityManager(new Models.SiteModelsContext()) } });

            //The configuration of the NHibernate data provider
            //container.RegisterSingle<IEntityManagerFactory>(new EntityManagerFactory {  
            //{ "SiteModelsContext", () => new WebAppAdmin.DataProvider.NHibernate.EntityManager(NHibernateHelper.SessionFactory) } });

            container.RegisterSingle<IValueConverter, ValueConverter>();
            container.RegisterSingle<IViewGenerator, ViewGenerator>();
            container.RegisterSingle<IControlConvention, ControlConvention>();
            container.RegisterSingle<IEntityItemManager, EntityItemGroupByEntityManagerName>();
            //Optional IEntityItemManager
            //container.RegisterSingle<IEntityItemManager, EntityItemGroupByAssembly>();
          
            // This is an extension method from the integration package.
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            // This is an extension method from the integration package as well.
            container.RegisterMvcIntegratedFilterProvider();

            //container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
    }
}