﻿using System;
using WebAppAdmin.DataProvider.Core;

namespace WebAppAdmin.UI.View.Core
{
    public interface IValueConverter
    {
        object ConvertToViewModelPropertyType(object value, Type viewModelPropertyType, Type entityPropertyType, IEntityTypeManager entityTypeManager);
        object ConvertToEntityPropertyType(object value, Type entityPropertyType, IEntityTypeManager entityTypeManager);
    }
}
