﻿using System;
using WebAppAdmin.DataProvider.Core;
using WebAppAdmin.UI.Core;

namespace WebAppAdmin.UI.View.Core
{
    public interface IViewGenerator
    {
        UIElement CreateEntityView(EntityType entityType, IEntityTypeManager entityTypeManager);
        UIElement CreateEntityPropertyView(EntityType entityType, EntityTypeProperty entityTypeProperty, IEntityTypeManager entityTypeManager, Control control, IControlConvention controlConvention);
    }
}
