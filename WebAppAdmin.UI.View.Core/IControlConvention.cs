﻿using System;
using System.Collections.Generic;
using WebAppAdmin.DataProvider.Core;
using WebAppAdmin.UI.Core;

namespace WebAppAdmin.UI.View.Core
{
    /// <summary>
    /// Return an <see cref="IList"/> of control names.
    /// </summary>
    public interface IControlConvention
    {
        IList<string> MatchControls(string propertyName, Type propertyType, EntityType entityType, IEntityTypeManager entityTypeManager);
    }
}
